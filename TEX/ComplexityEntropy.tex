% !TEX root = Main.tex
\section{Entropy as a measure of xtUML component model complexity}
\label{sec:EntropyComplexity}

The main idea behind entropy as a measure of model complexity is that a model is observed as an information source. In that case, the entropy of a model can be calculated using classical Shannon's formula for entropy\cite{shannon2001}. This approach has been used by several authors for calculating complexity of software on source code level\cite{Harrison1992} \cite{kim1995}. The key issue when calculating the entropy of an information source is symbol selection. Harisson \cite{Harrison1992} used source code \emph{operators} as symbols. He noted that \emph{program complexity is inversely proportional to the average information content of its operators}. This implies that, a larger number of source code symbols leads to higher software complexity. Kim et. al.\cite{kim1995} had a somewhat different approach which could be applied only to object-oriented source code. They constructed intra- and inter- class dependency graphs with (data and function) class members as nodes and read/write relations as arcs between nodes. The symbols used in entropy calculations are nodes in the graph.

In this thesis, we will use a similar idea of a model as an information source and will use entropy to calculate model complexity. Similarly as Harrison \cite{Harrison1992}, we will use model elements as our symbols and frequency of their appearance in the model to calculate their probability and entropy. However, a precise definition of what will be considered as a \emph{model element} is not trivial and needs elaboration. 

\subsection{Model elements}
Each model can be observed as a populated meta-model. Similarly to the \emph{Abstract Syntax Tree} (AST) in programming languages, a meta-model defines the structure and rules for creating valid models. Meta-models are typically specified using class models. Simply put, we are using a class model to describe a class model itself (as well as the other xtUML sub-models). The classes that describe the xtUML language itself are called \emph{meta-classes}. In our calculus, each meta-class will represent a model \emph{element type} and each meta-class \emph{instance} will be a model element\footnote{In remaining of this chapter, phrases \emph{meta-class} and \emph{model element type} will be used interchangeably. The same applies for phrases \emph{meta-class instance} and \emph{model element}.}.

In order to calculate probability of each model element type, we will use the number of instances of the element type in a given model. The probability of an element type is then used in order to calculate its entropy.

\begin{figure*}
  \centering
    \includegraphics[width=0.9\textwidth]{fig/MM}
    \caption{A part of Bridgepoint xtUML meta-model describing interfaces.}
    \label{fig:MM}
\end{figure*}

For example, when we create an interface in our model, an instance of meta-class \emph{Interface} is created behind the scenes. Similarly, by adding a new interface message to that interface, we are actually creating a new instance of an \emph{Executable Property} meta-class and associate it to the corresponding instance of \emph{Interface} class (see figure \ref{fig:MM}). An \emph{Executable Property} can be either an \emph{Interface Signal} or an \emph{Interface Operation} and may have many \emph{Property Parameters}. It is important to stress that the populated xtUML meta-model \textbf{completely} describes all structural and behavioural details of an xtUML model. Complete xtUML meta-model itself is therefore a very large class model consisting of 435 (meta)classes and 847 (meta)relations distributed across a number of packages\footnote{For more details please refer to BridgePoint tool meta-model \cite{BridgePoint} which can be accessed from the welcome screen in the tool. Since the BridgePoint is currently the only xtUML tool, BridgePoint meta-model is actually xtUML meta-model.}. 

From perspective of a BridgePoint user, an xtUML application model is a set of interlinked text snippets and figures. However, behind the scenes, each xtUML application model is actually a tree of xtUML meta-model instances or \emph{populated meta-model}. If more information about meta-models is required please refer to \cite{genova2009}.


When calculating entropy of an xtUML model, it is required to know the probability of all model element types (meta-classes). The probability of each model element type (meta-class) is calculated as a ratio between the number of elements of given type (the number of instances of given meta-class) and the number of all elements in the model (the number of instances of all meta-classes). Complete model entropy is then given with:  

\begin{equation}\label{eq:CE}
CE = \sum_{i=1}^{M} p_{i} log_{2} \frac{1}{p_{i}} = \sum_{i=1}^{M} \frac{N_{i}}{N_{T}} log_{2} \frac{N_{T}}{N_{i}}
\end{equation}

where $N_{i}$ represents the number of elements of $i$-th type, $M$ represents the number of element types in the meta-model and $N_{T}$ represents the total number of elements in the model.




\subsection{Vertical distribution of entropy}

As an input to calculation of vertical distribution of entropy across model layers, we will use distribution of model elements across those layers. First, we will categorize all element types (meta-classes) by the layer they belong to. For example, class attributes and operations are part of class model so we consider all attributes and operations (together with other elements) as part of \emph{CLASS} layer. Notice that not all model element types can be assigned to a model layer because there are elements that can appear in all layers or that do not belong to any of the layers. All such elements are categorized to category \emph{OTHER}. Since we do not introduce any new symbols, the total model entropy of vertically distributed model ($CE_{vd}$), remains the same as the total model entropy ($CE$, given with eq.~\ref{eq:CE}):

\begin{equation}\label{eq:CEvd}
CE = CE_{vd} = \sum_{j=1}^{L} CE_{j} = \sum_{j=1}^{L} \sum_{i=1}^{M_{j}} \frac{N_{i}}{N_{T}} log_{2} \frac{N_{T}}{N_{i}}
\end{equation}

where $CE_{j}$ represents the sum of entropies of all elements from layer $j$, $M_{j}$ represents the number of meta-classes associated to $j$-th layer, $N_{i}$ represents the number of instances of $i$-th meta-class in $j$-th layer and $N_{T}$ the total number of elements in the model. In this equation, an layer entropy is calculated as a sum of entropies of all element types in that layer. 

Notice the difference between this approach and the case in which we calculate the entropy of a layer by counting the elements in each layer and using it in order to calculate probability that an element belongs to a layer:

\begin{equation} \label{eq:CEvdTilda}
CE_{vd} \neq CE_{vd}' = \sum_{j=1}^{L} \frac{N_{j}}{N_{T}} log_{2} \frac{N_{T}}{N_{j}}
\end{equation}

where $N_{j}$ represents the number of elements in $j$-th layer anf $L$ represents the number of layers. In that case we have only 5 symbols corresponding to 5 different layers of the xtUML model. As we decided to use meta-classes as symbols, we will ignore this approach and only use eq.~\ref{eq:CEvd} when calculating the entropy of a layer.

\subsection{Horizontal distribution of entropy across classes}
\label{EntropyHorizontalDistributionByClasses}

In order to distribute model elements horizontally, across classes, we need to associate each element with the (application) class it belongs to. This is done in addition to base distribution by the element type (meta-class). Notice that only a subset of model element types in the meta-model can be associated to a class. For example, an attribute, an operation, a state, a transition, a statement, an expression or a variable may belong to a class, but a component, a port, a package or an interface cannot. Although such distribution does not include all model elements, it is still interesting because classes are considered as the most important xtUML elements and contain the majority of model elements\footnote{When it comes to distribution of complexity and application logic, xtUML design rules favor classes over other elements.} (see figure \ref{fig:DistributionOfElements}).


Similarly as we did when calculating entropy of an xtUML layer, when calculating the entropy of a class, we will sum up the entropies of all the element belonging to that class:

\begin{equation}\label{eq:CEhdc}
CE_{hdc} = \sum_{k=1}^{C} CE_{k} = \sum_{k=1}^{C} \sum_{i=1}^{M_{c}} \frac{N_{i}}{N_{T}} log_{2} \frac{N_{T}}{N_{i}}
\end{equation}

where $CE_{k}$ represents the sum of entropies of all elements within $k$-th application class, $C$ represents the number of application classes, $N_{i}$ represents the number of instances of $i$-th meta-class, and $M_{c}$ represents the number of meta-classes that can be associated with a class. Notice that $N_{T}$ represents the total number of instances of all meta-classes, not only those that can be associated to classes.

Unlike vertical distribution, horizontal distribution introduces new symbols. The reason for this is the fact that, in order to calculate horizontal entropy distribution, we need to split the population of model elements of the same type according to the (application) class they belong. Although vertical distribution includes a larger number of elements (actually all model elements), the number of symbols is larger when we observe only the elements that can be associated with a class (distributed across classes). For example, when calculating vertical distribution, we count all \emph{create} statements in the model and consider \emph{all} of them to represent the same symbol. However, when calculating horizontal distribution, we are interested how those statements are distributed across classes. We consider \emph{create} statements in the \emph{Calculator} class as a separate symbol from those in the \emph{Number} class. In that way, the number of symbols increases by factor of $C$ which represents the number of application classes. Since the total entropy depends on the number of symbols, the total amount of entropy of elements within classes will therefore be larger then the the total amount of vertically distributed entropy of all elements within a model. 

\subsection{Horizontal distribution of entropy across bodies}
Similar approach is used to calculate entropy distribution across bodies. A body is a model element that contains processing statements. Each processing model statement must be part of exactly one body. In that sense, a body is very similar to a subroutine (procedure or a method). A body, with all its statements, can be associated to other model elements, practically holding the part of processing model associated with that element. For example, class operations and states are not directly associated with their processing statements but have their own instance of body which holds them. 

Instead of splitting a population of elements of a certain type across application classes, we will split it according to the bodies they belong to (in addition to the basic distribution according to the model element type). Notice that only a relatively small subset of element types within meta-model may be associated with a body. For example, a statement, an expression or a variable can belong to a body, but a class, a relation, a port or a component cannot. Although a small subset of element \emph{types} is included in this distribution, it is still interesting because the majority of model \emph{elements} are located in the bodies (see figure \ref{fig:DistributionOfElements}).


\begin{figure*}
  \centering
    \includegraphics[width=0.9\textwidth]{fig/DistributionOfElements}
      \caption{Distribution of elements in xtUML models.}
   \label{fig:DistributionOfElements}
\end{figure*}

Body entropy is then calculated as a sum of entropies of all the element types in that body:

\begin{equation}\label{eq:CEhdb}
CE_{hdb} = \sum_{n=1}^{B} CE_{n} = \sum_{n=1}^{B} \sum_{i=1}^{M_{b}} \frac{N_{i}}{N_{T}} log_{2} \frac{N_{T}}{N_{i}}
\end{equation}

where $CE_{n}$ represents the sum of entropies of all elements within $n$-th body, $B$ represents the number of bodies within the model, $N_{i}$ represents the number of instances of $i$-th meta-class, and $M_{b}$ represents the number of meta-classes that can be associated to a body. Notice that $N_{T}$ represents the total number of instances in the model, and not only those that can be associated to bodies.


\subsection{Entropy as a complexity metric: conclusion}
Using the model element types (meta-classes) defined in the meta-model as symbols and their cardinality (number of instances) for the calculation of application model entropy is a novel approach presented in this thesis. Entropy as a complexity metric represents a logarithmic-scale size metric and as such has limited practical value. However, similarly as the LOC metric in the source-code level, the number of model elements (required for entropy calculus) may be well suited as the \emph{referent size metric} for the models. This is especially important because LOC metric is poor choice as a metric in model-based software-development. 

When comparing the size of two models (conforming to the same meta-model), the number of meta-class instances gives an intuitive measure of their relative complexity since it specifies the number of elements required to completely describe the application model. Similarly as comparing the LOC metric for applications written in different languages, comparing the number of elements for models conforming to different meta-models (i.e. models of different type), does not have much sense. 

The only drawback of using number of meta-model instances as a referent size metric of a model is that it cannot be explicitly counted as the lines of code can. This limitation is, however, easily avoided since instance counting represents a relatively simple additional feature in the tooling that operates on the populated meta-model of the application.
