% !TEX root = Main.tex
\section{Data complexity of xtUML models}
\label{sec:DataComplexity}
Data complexity is a vague term that may imply several different complexity metrics. In this section, we will cover two data complexity metrics that can be applied to xtUML models. Before analysing these metrics, we will explain the data types and data modelling in xtUML. 

\subsection{Introduction to data types in xtUML}
As in most object-oriented languages, at design time, users in xtUML can specify their own data types. Most frequently, but not exclusively, this is done in form of classes or data structures. In xtUML, classes and data structures have almost complementary usages. Classes are used to describe the concepts in a domain; they can declare relations between the instances and those relations may expose very specific meaning. It is not strange to have more than one relation between same pair of classes, expressing similar, but slightly different, semantics. At design time, classes are specific for, and visible only within, the component they are defined in. As a consequence, classes cannot be used as parameter types on interface messages. This means that, at runtime, class instances cannot be passed to some other component. This limitation in xtUML is intentional as components are intended to act as wrappers for subject matter domains which should be clearly separated. Furthermore, this limitation simplifies the language because components are independent of each other and can truly be considered as \textit{black boxes}.

Data structures are used in a different way: typically, they are defined globally and allow data exchange between the components or with outside, non-modelled, parts of the application\footnote{For details about communication with non-modelled parts of the application, please refer to BridgePoint tool help \cite{BridgePoint}}. Unlike classes, data structures cannot be related, except using implicit containment relations when a member of the data structure is typed by some other data structure. In addition, classes may also have attributes typed by a data structure. 

Except the data structures and classes, xtUML allows definition of enumerations and user-defined primitive types. Enumerations in xtUML have well-known semantics as in many other languages. User-defined primitive types are similar to \textit{typedef} construct in the C language: they are based, and take values of, some other primitive type. There are, however, only 4 basic primitive types in xtUML: boolean, real, integer and string. Except adding another name to a primitive type, a user-defined type is often a subject of a separate translation rule. For example, by default an \textit{integer} type in the model is translated into a \textit{long} type in C++ language. However, a user defined type \textit{byte} which uses integer type as its base, may be translated as a \textit{char} type in C++. By allowing user-defined primitives, xtUML allows easier mapping to richer type systems required by target implementation languages. Memory optimization of that kind are not a concern in xtUML modelling, so, except the name, there is no other differences between the user-defined type and it's core primitive type. 



\subsection{Data type complexity}
\label{sec:DataTypeComplexity}

Complexity of a data type measures the \textit{complexity of a data type definition} as \textit{the number of its members and relations}. In xtUML, data modelling is usually done using a class model and our focus will be on complexity of a class definition. Although a definition of a structured data type formally does not belong to a class model, its data type complexity will be added to data complexity of a class when there is a class attribute typed by the data structure. A class data type complexity depends on the number of its primitive attributes, the number of relations class is involved in, as well as the number of primitive members within all its structured members. Mathematically, this is described with following recursive equation:

\begin{equation}\label{eq:CDt}
CD_{t}(c) = N_{p} + N_{r}+\sum_{i=1}^{N_{s}} CD_{t}(s_{i})
\end{equation}


where $CD_{t}(c)$ represents the data type complexity of a class $c$, $N_{p}$ represents the number of primitive members in a class, $N_{r}$ represents the number of relations a class is involved in, $N_{s}$ represents the number of structured members in a class and $CD_{t}(s_{i})$ represents the data type complexity of $i$-th structured member in a class, also calculated using the equation \ref{eq:CDt}. Of course, when applying the recursive formula to structured member, $N_{r}$ will always be zero. Notice that equation does not handle multiplicity of attributes or relations which implies that an array of integers contributes to the data type complexity equally as a plain integer member. 


According to the xtUML specification, classes cannot have other classes as types of their attributes, since that implies hidden relations; relations not exposed visually in the class model. Unlike the common practice in object-oriented languages, a relation in xtUML class model is not represented with a class attribute (therefore the $N_{r}$ in eq.~\ref{eq:CDt}). This also implies that a relation contributes to data type complexity of each class involved in the relation.




\subsection{Data flow complexity}
\label{sec:DataFlowComplexity}
Data flow complexity measures the complexity of the data processing performed at runtime. This typically includes the number of times data items are assigned and/or used during processing. Data flow complexity is inspired by compiler optimization techniques and is first described by Oviedo \cite{oviedo1980}. He measured the data flow complexity as the number of \textit{def-use} (definition-usage) pairs in the code. However, an xtUML model consists of several layers and Oviedo's metric is applicable only to the processing model. In order to be able to investigate vertical data flow complexity distribution, we need to be able to apply the metric to other layers as well. We will use \emph{the number of variable (re)definitions} as the data flow complexity metric because it can be applied to other xtUML models as well. As it ignores variable usages, our metric can be considered as simplification of Oviedo's original metric.

At runtime, from perspective of data flow complexity, an xtUML model can be observed as a set of bodies communicating synchronously or asynchronously with each other. Each body, including those of states and transitions, may have some input and output data flows which implies that entire data flow complexity of an xtUML model may be measured on a body level. In other words, data flow complexity primarily affects the processing model which specifies details of data manipulation done at runtime. Other xtUML layers may also expose data flow complexity, but data flow complexity expressed there is taken into account on body level. This abstraction of an xtUML model is somewhat simpler from the one used when calculating cyclomatic complexity of a complete xtUML model. In order to create a control flow graph (CFG) out of a complete xtUML model, we could not eliminate the state machine layer since it describes dynamic (runtime) behaviour and introduces additional cyclomatic complexity. Unlike in cyclomatic complexity, state machine layer does not introduce any real data flow complexity that is not already expressed in the body layer. Regardless to that, it would be beneficial to see how much of the data flow complexity is visualized and how it is distributed vertically across component, class and state-machine models.

In a component model, data flow complexity will be expressed as the number of parameters on \textbf{incoming} port messages. Data flow complexity of outgoing port messages, is not ignored however; it is taken into account in the remote component in which those messages are considered as incoming. Each incoming port message (signal or operation) has associated a non-empty body that specifies processing instructions to be executed upon message reception. However, on component level, we know only the number of body parameters. Each parameter represents a \textit{variable definition} on the entry to the body and contributes to the data flow complexity on the processing model level accordingly. A parameter in a component model only visualizes the actual parameter in the body, so it is does not contribute to the absolute data flow complexity. All parameters on all incoming messages of all ports on the component need to be taken into account\footnote{Remember that even outgoing ports may have incoming messages as the port direction does not imply the messages direction}:

\begin{equation}\label{eq:CDf_comp}
CD_{f}(comp) =\sum_{i=1}^{N_{p}} \sum_{j=1}^{N_{im}} N_{prm}(j)
\end{equation}

where $N_{p}$ represents the number of ports on a component, $N_{im}$ the number of incoming messages on a given port and $N_{prm}(j)$ the number of parameters on $j$-th message. 

Similarly to incoming port operations, class operations have non empty bodies so their parameters represent a \textit{variable definitions} in the body. Class model contribution to the data flow complexity will also be ignored when calculating the total data flow complexity of the models as it is taken into account in the data flow complexity of the processing model. We will consider that each parameter of each operation in each class \emph{visualizes} one single variable definition from the body layer. For a class $c$ we than have: 

\begin{equation}\label{eq:CDf_class}
CD_{f}(c) =\sum_{j=1}^{N_{op}} N_{prm}(j)
\end{equation}

where $N_{op}$ is the number of operations in a class and $N_{prm}(j)$ is the number of parameters in $j$-th operation of the class. When calculating vertical distribution of data flow complexity all classes within a component should be taken into account.


On state machine level, events are main carriers of data so data flow complexity is defined with the number of the \textit{event data items}. They represent parameters of the event that triggered a transition or state entry and have the same semantics as parameters of class operations or port messages. Therefore we can say that each event data item within a \textbf{non-empty} state or transition body, counts as a single variable definition. However, it is perfectly normal that an event does not have any data items, in which case, the processing of an event does not imply any variable definition on the body level. Similarly as in class operation and incoming port messages, each event data item on each transition or state with non-empty body will not count as separate variable definition but will rather be considered to visualize the same variable definition from the body layer.

State machines in xtUML allow both, transitions and states, to define processing instructions (have non-empty bodies), but they are often empty. Non-empty transitions and states are, on state machine model, clearly indicated (see figure \ref{fig:StateWithMultipleIncommingTransitions}). This means that, unlike in class operations and incoming port messages which almost exclusively have non-empty bodies, we cannot assume the existence of a body for transitions or states. When counting variable definitions (event data items) visualized by the state machine model, we should include only those transitions and states that actually have non-empty bodies. Although they share many similarities regarding the data flow complexity, counting the number of event data items in transition and state bodies is not identical. In xtUML state machines, events are assigned to transitions so, within a transition body, we always know exactly which event triggered the transition and which event data items (if any) we have on our disposal. However, this is not always true for state bodies. Each state may have more than one incoming transition, which may be triggered by different events. As a consequence, within a state body, we cannot be sure which event actually led to state body execution. This implies that, for states having multiple incoming transitions triggered by different events, only a subset of event data items common to all incoming events will be available in the state body. If events do not have common data items, there will be no data items reachable in a state body. For example, on figure \ref{fig:StateWithMultipleIncommingTransitions}, only \textit{amount} parameter is reachable within \textit{Producing} state because it is the only event data item common to both events. This should be taken into account when calculating data complexity of a state (body). For a state machine $sm$ we then have:

\begin{equation}\label{eq:CDf_sm}
CD_{f}(sm) =\sum_{i=1}^{N_{Tneb}} N_{edi}(i) + \sum_{j=1}^{N_{Sneb}} N_{cedi}(j)
\end{equation}

where $N_{Tneb}$ represents the number of transitions with non-empty bodies, $N_{edi}(i)$ represents the number of event data items available in the body of $i$-th transition, $N_{Sneb}$ represents the number of non-empty states within a state machine and $N_{cedi}(j)$ is the number of event data items \emph{common} to all transitions incoming to the $j$-th state. When calculating the vertical distribution of data flow complexity, all state-machines within all classes in a component should be taken into account.


\begin{figure*}
  \centering
    \includegraphics[width=1.0\textwidth]{fig/StateWithMultipleIncommingTransitions}
    \caption{An example of a state with multiple incoming transitions triggered by different events.}
     \label{fig:StateWithMultipleIncommingTransitions}
\end{figure*} 



As already mentioned, data flow complexity of a body layer in an xtUML model represents the complete data flow complexity of the model. In order to measure it we will count the number of (re)definitions  in each body. In OAL, the language used to textually specify the processing model, there are four ways to (re)define a value within a body of OAL code: 

\begin{itemize}
\item \textbf{Instance creation statement} creates a new instance of a given class with all its primitive members initialized to default values. Typically, a reference to the instance is stored in a variable.
\item \textbf{Assignment statement} assigns a value to a variable or to an instance member. It may be interpreted as a declaration with an initialization (if the variable of same name has not be used yet) or as a simple redefinition (if there is a variable of the same name already defined)\footnote{OAL language used to textually specify processing model does not allow explicit declaration and supports only implicit variable typing.}. Such definitions may have any type, primitive, data structure or class, depending on the type of the right-hand side expressions.
\item \textbf{Selection statement} is used to select an instance or an instance set from the population of some class or across a chain of relations ending with that class. The result of a selection is stored in a variable of corresponding type (class instance reference or a set of class instance references).
\item \textbf{Parameter definitions} in a body are implicitly defined (no formal definition of a parameter exists in a body). Parameter values are accessed using keyword \textit{param} before the name of the parameter or event data item.
\end{itemize}


\begin{figure*}
  \centering
    \includegraphics[width=0.6\textwidth]{fig/OAL_Definitions}
    \caption{Statements in OAL language that affect the number of definitions.}
     \label{fig:OALDefinitions}
\end{figure*} 

Therefore, for a non-empty body $b$, data flow complexity can be calculated using the following equation:

\begin{equation}\label{eq:CDf_body}
CD_{f}(b) = N_{ics}(b) + N_{as}(b) + N_{ss}(b) + N_{prm}(b)
\end{equation}

where $N_{ics}(b)$ represents the number of instance creation statements, $N_{as}(b)$ represents the number of assignment statements, $N_{ss}(b)$ represents the number of selection statements and $N_{prm}(b)$ is the number of parameters (or event data items) available in body $b$. When calculating the total data flow complexity of  processing model (also total data flow complexity) of an xtUML model, all non-empty bodies should be taken into account.