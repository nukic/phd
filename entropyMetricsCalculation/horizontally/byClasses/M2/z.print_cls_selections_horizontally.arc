.invoke pl = GET_ENV_VAR("PROJECT_LOC")
.if(pl.success)
	.print "======================================================================================"
	.assign project_location = pl.result	
	.print "Using project location ${project_location}."
	.print "======================================================================================"
	.//--------------------------------------------------------------------------------
	.//INCLUSIONS
	.//include "${project_location}/utils/q.general.utils.arc"
	.//include "${project_location}/utils/q.cc.horizontal.arc"
	.//include "${project_location}/utils/q.cc.vertical.arc"
	.//include "${project_location}/utils/q.loc.horizontal.arc"
	.//include "${project_location}/utils/q.printing.arc"
	.print "Starting..."
	.select many pkg_set from instances of EP_PKG
	.assign indent = "   "
	.include "${project_location}/utils/t.looping_horizontally.arc"
	.for each pkg in pkg_set
		.print "Handling package: ${pkg.Name}"
		.select many directly_contained_cls_set related by pkg->PE_PE[R8000]->O_OBJ[R8001]
		.if(not_empty directly_contained_cls_set)
			.include "${project_location}/utils/t.pkg_name_printout_horizontally.arc"
			.for each mmClass in directly_contained_cls_set
				.print "Handling metaclass: ${mmClass.Name}"
				.include "${project_location}/utils/t.class_selection_horizontally.arc"
			.end for
		.end if
	.end for
	.include "${project_location}/utils/t.emitting_horizontally.arc"
	.emit to file "C:/Temp/PrintMetaclassCardinalityHorizontally.arc"
	.print "DONE!"
.else
	.print "FAILED TO READ ENV VAR: PROJECT_LOC"
.end if
.//