.function get_all_class_bodies
.param inst_ref cls_inst
	.select many attr_result from instances of ACT_ACT where false
	.//Class operation bodies
	.select many op_body_set related by cls_inst->O_TFR[R115]->ACT_OPB[R696]->ACT_ACT[R698]
	.assign attr_result = attr_result|op_body_set
	.//Derr attr bodies
	.select many derattr_body_set related by cls_inst->O_ATTR[R102]->O_BATTR[R106]->O_DBATTR[R107]->ACT_DAB[R693]->ACT_ACT[R698]
	.assign attr_result = attr_result|derattr_body_set
	.//State bodies
	.select many state_body_set related by cls_inst->SM_ISM[R518]->SM_SM[R517]->SM_ACT[R515]->ACT_SAB[R691]->ACT_ACT[R698]
	.assign attr_result = attr_result|state_body_set
	.//Transition bodies
	.select many txn_body_set related by cls_inst->SM_ISM[R518]->SM_SM[R517]->SM_ACT[R515]->ACT_TAB[R688]->ACT_ACT[R698]
	.assign attr_result = attr_result|txn_body_set
	.print "Class ${cls_inst.Name} has total bodies: ${attr_result}"
.end function
.//
.function find_remaining_links
.param inst_ref curr_lnk_inst
.param inst_ref_set all_prev_lnk_set
	.select many attr_result from instances of ACT_LNK where false
	.select one next_lnk related by curr_lnk_inst->ACT_LNK[R604.'succeeds']
	.if(empty next_lnk)
		.assign attr_result = all_prev_lnk_set
	.else
		.//select one r_rel_inst related by next_lnk->R_REL[R681]
		.//print "Next chain is over relation number ${r_rel_inst.Numb}"
		.assign all_prev_lnk_set = all_prev_lnk_set|next_lnk
		.invoke frl = find_remaining_links(next_lnk, all_prev_lnk_set)
		.assign attr_result = frl.result
	.end if
.end function
.//
.//---------------------------------------------------------
   .select many class_set from instances of O_OBJ
   .for each class in class_set
   .print "Looking for metaclass instances related to class ${class.Name}"
      .//------------------------------------------------------------------------
      .//Association
      .//select many R_REL_set related by class->...->R_REL[Rxxx]
      .//assign R_REL_set_cardinality = cardinality R_REL_set
      .//print "R_REL ${R_REL_set_cardinality} ${class.Name}"
   .//R_REL ${R_REL_set_cardinality} ${class.Name}
      .//select many R_AONE_set related by class->...->R_AONE[Rxxx]
      .//assign R_AONE_set_cardinality = cardinality R_AONE_set
      .//print "R_AONE ${R_AONE_set_cardinality} ${class.Name}"
   .//R_AONE ${R_AONE_set_cardinality} ${class.Name}
      .//select many R_AOTH_set related by class->...->R_AOTH[Rxxx]
      .//assign R_AOTH_set_cardinality = cardinality R_AOTH_set
      .//print "R_AOTH ${R_AOTH_set_cardinality} ${class.Name}"
   .//R_AOTH ${R_AOTH_set_cardinality} ${class.Name}
      .//select many R_CONE_set related by class->...->R_CONE[Rxxx]
      .//assign R_CONE_set_cardinality = cardinality R_CONE_set
      .//print "R_CONE ${R_CONE_set_cardinality} ${class.Name}"
   .//R_CONE ${R_CONE_set_cardinality} ${class.Name}
      .//select many R_COTH_set related by class->...->R_COTH[Rxxx]
      .//assign R_COTH_set_cardinality = cardinality R_COTH_set
      .//print "R_COTH ${R_COTH_set_cardinality} ${class.Name}"
   .//R_COTH ${R_COTH_set_cardinality} ${class.Name}
      .//select many R_ASSR_set related by class->...->R_ASSR[Rxxx]
      .//assign R_ASSR_set_cardinality = cardinality R_ASSR_set
      .//print "R_ASSR ${R_ASSR_set_cardinality} ${class.Name}"
   .//R_ASSR ${R_ASSR_set_cardinality} ${class.Name}
      .//select many R_FORM_set related by class->...->R_FORM[Rxxx]
      .//assign R_FORM_set_cardinality = cardinality R_FORM_set
      .//print "R_FORM ${R_FORM_set_cardinality} ${class.Name}"
   .//R_FORM ${R_FORM_set_cardinality} ${class.Name}
      .//select many R_PART_set related by class->...->R_PART[Rxxx]
      .//assign R_PART_set_cardinality = cardinality R_PART_set
      .//print "R_PART ${R_PART_set_cardinality} ${class.Name}"
   .//R_PART ${R_PART_set_cardinality} ${class.Name}
      .//select many R_SUB_set related by class->...->R_SUB[Rxxx]
      .//assign R_SUB_set_cardinality = cardinality R_SUB_set
      .//print "R_SUB ${R_SUB_set_cardinality} ${class.Name}"
   .//R_SUB ${R_SUB_set_cardinality} ${class.Name}
      .//select many R_SUPER_set related by class->...->R_SUPER[Rxxx]
      .//assign R_SUPER_set_cardinality = cardinality R_SUPER_set
      .//print "R_SUPER ${R_SUPER_set_cardinality} ${class.Name}"
   .//R_SUPER ${R_SUPER_set_cardinality} ${class.Name}
      .//select many R_OIR_set related by class->...->R_OIR[Rxxx]
      .//assign R_OIR_set_cardinality = cardinality R_OIR_set
      .//print "R_OIR ${R_OIR_set_cardinality} ${class.Name}"
   .//R_OIR ${R_OIR_set_cardinality} ${class.Name}
      .//select many R_COMP_set related by class->...->R_COMP[Rxxx]
      .//assign R_COMP_set_cardinality = cardinality R_COMP_set
      .//print "R_COMP ${R_COMP_set_cardinality} ${class.Name}"
   .//R_COMP ${R_COMP_set_cardinality} ${class.Name}
      .//select many R_ASSOC_set related by class->...->R_ASSOC[Rxxx]
      .//assign R_ASSOC_set_cardinality = cardinality R_ASSOC_set
      .//print "R_ASSOC ${R_ASSOC_set_cardinality} ${class.Name}"
   .//R_ASSOC ${R_ASSOC_set_cardinality} ${class.Name}
      .//select many R_RTO_set related by class->...->R_RTO[Rxxx]
      .//assign R_RTO_set_cardinality = cardinality R_RTO_set
      .//print "R_RTO ${R_RTO_set_cardinality} ${class.Name}"
   .//R_RTO ${R_RTO_set_cardinality} ${class.Name}
      .//select many R_RGO_set related by class->...->R_RGO[Rxxx]
      .//assign R_RGO_set_cardinality = cardinality R_RGO_set
      .//print "R_RGO ${R_RGO_set_cardinality} ${class.Name}"
   .//R_RGO ${R_RGO_set_cardinality} ${class.Name}
      .//select many R_SIMP_set related by class->...->R_SIMP[Rxxx]
      .//assign R_SIMP_set_cardinality = cardinality R_SIMP_set
      .//print "R_SIMP ${R_SIMP_set_cardinality} ${class.Name}"
   .//R_SIMP ${R_SIMP_set_cardinality} ${class.Name}
      .//select many R_SUBSUP_set related by class->...->R_SUBSUP[Rxxx]
      .//assign R_SUBSUP_set_cardinality = cardinality R_SUBSUP_set
      .//print "R_SUBSUP ${R_SUBSUP_set_cardinality} ${class.Name}"
   .//R_SUBSUP ${R_SUBSUP_set_cardinality} ${class.Name}
      .//------------------------------------------------------------------------
      .//Body
      .invoke alcb = get_all_class_bodies(class)
      .assign ACT_ACT_set = alcb.result
      .//
      .assign ACT_ACT_set_cardinality = cardinality ACT_ACT_set
      .print "ACT_ACT ${ACT_ACT_set_cardinality} ${class.Name}"
ACT_ACT ${ACT_ACT_set_cardinality} ${class.Name}
      .select many ACT_BLK_set related by ACT_ACT_set->ACT_BLK[R601]
      .assign ACT_BLK_set_cardinality = cardinality ACT_BLK_set
      .print "ACT_BLK ${ACT_BLK_set_cardinality} ${class.Name}"
ACT_BLK ${ACT_BLK_set_cardinality} ${class.Name} 
      .select many ACT_SMT_set related by ACT_ACT_set->ACT_BLK[R601]->ACT_SMT[R602]
      .assign ACT_SMT_set_cardinality = cardinality ACT_SMT_set
      .print "ACT_SMT ${ACT_SMT_set_cardinality} ${class.Name}"
ACT_SMT ${ACT_SMT_set_cardinality} ${class.Name}
      .select many ACT_BRK_set related by ACT_ACT_set->ACT_BLK[R601]->ACT_SMT[R602]->ACT_BRK[R603]
      .assign ACT_BRK_set_cardinality = cardinality ACT_BRK_set
      .print "ACT_BRK ${ACT_BRK_set_cardinality} ${class.Name}"
ACT_BRK ${ACT_BRK_set_cardinality} ${class.Name}
      .//select many ACT_BRB_set related by class->...->ACT_BRB[Rxxx]
      .//assign ACT_BRB_set_cardinality = cardinality ACT_BRB_set
      .//print "ACT_BRB ${ACT_BRB_set_cardinality} ${class.Name}"
   .//ACT_BRB ${ACT_BRB_set_cardinality} ${class.Name}
      .//select many ACT_CON_set related by ACT_ACT_set->ACT_BLK[R601]->ACT_SMT[R602]->ACT_CON[R603]
      .//assign ACT_CON_set_cardinality = cardinality ACT_CON_set
      .//print "ACT_CON ${ACT_CON_set_cardinality} ${class.Name}"
   .//ACT_CON ${ACT_CON_set_cardinality} ${class.Name}
      .select many ACT_CTL_set related by ACT_ACT_set->ACT_BLK[R601]->ACT_SMT[R602]->ACT_CTL[R603]
      .assign ACT_CTL_set_cardinality = cardinality ACT_CTL_set
      .print "ACT_CTL ${ACT_CTL_set_cardinality} ${class.Name}"
ACT_CTL ${ACT_CTL_set_cardinality} ${class.Name}
      .select many ACT_DAB_set related by ACT_ACT_set->ACT_DAB[R698]
      .assign ACT_DAB_set_cardinality = cardinality ACT_DAB_set
      .print "ACT_DAB ${ACT_DAB_set_cardinality} ${class.Name}"
ACT_DAB ${ACT_DAB_set_cardinality} ${class.Name}
      .select many ACT_E_set related by ACT_ACT_set->ACT_BLK[R601]->ACT_SMT[R602]->ACT_E[R603]
      .assign ACT_E_set_cardinality = cardinality ACT_E_set
      .print "ACT_E ${ACT_E_set_cardinality} ${class.Name}"
ACT_E ${ACT_E_set_cardinality} ${class.Name}
      .select many ACT_EL_set related by ACT_ACT_set->ACT_BLK[R601]->ACT_SMT[R602]->ACT_EL[R603]
      .assign ACT_EL_set_cardinality = cardinality ACT_EL_set
      .print "ACT_EL ${ACT_EL_set_cardinality} ${class.Name}"
ACT_EL ${ACT_EL_set_cardinality} ${class.Name}
      .select many ACT_FOR_set related by ACT_ACT_set->ACT_BLK[R601]->ACT_SMT[R602]->ACT_FOR[R603]
      .assign ACT_FOR_set_cardinality = cardinality ACT_FOR_set
      .print "ACT_FOR ${ACT_FOR_set_cardinality} ${class.Name}"
ACT_FOR ${ACT_FOR_set_cardinality} ${class.Name}
      .//select many ACT_FNB_set related by class->...->ACT_FNB[Rxxx]
      .//assign ACT_FNB_set_cardinality = cardinality ACT_FNB_set
      .//print "ACT_FNB ${ACT_FNB_set_cardinality} ${class.Name}"
   .//ACT_FNB ${ACT_FNB_set_cardinality} ${class.Name}
      .select many ACT_IF_set related by ACT_ACT_set->ACT_BLK[R601]->ACT_SMT[R602]->ACT_IF[R603]
      .assign ACT_IF_set_cardinality = cardinality ACT_IF_set
      .print "ACT_IF ${ACT_IF_set_cardinality} ${class.Name}"
ACT_IF ${ACT_IF_set_cardinality} ${class.Name}
      .select many ACT_OPB_set related by ACT_ACT_set->ACT_OPB[R698]
      .assign ACT_OPB_set_cardinality = cardinality ACT_OPB_set
      .print "ACT_OPB ${ACT_OPB_set_cardinality} ${class.Name}"
ACT_OPB ${ACT_OPB_set_cardinality} ${class.Name}
      .//select many ACT_POB_set related by class->...->ACT_POB[Rxxx]
      .//assign ACT_POB_set_cardinality = cardinality ACT_POB_set
      .//print "ACT_POB ${ACT_POB_set_cardinality} ${class.Name}"
   .//ACT_POB ${ACT_POB_set_cardinality} ${class.Name}
      .//select many ACT_PSB_set related by class->...->ACT_PSB[Rxxx]
      .//assign ACT_PSB_set_cardinality = cardinality ACT_PSB_set
      .//print "ACT_PSB ${ACT_PSB_set_cardinality} ${class.Name}"
   .//ACT_PSB ${ACT_PSB_set_cardinality} ${class.Name}
      .//select many ACT_ROB_set related by class->...->ACT_ROB[Rxxx]
      .//assign ACT_ROB_set_cardinality = cardinality ACT_ROB_set
      .//print "ACT_ROB ${ACT_ROB_set_cardinality} ${class.Name}"
   .//ACT_ROB ${ACT_ROB_set_cardinality} ${class.Name}
      .//select many ACT_RSB_set related by class->...->ACT_RSB[Rxxx]
      .//assign ACT_RSB_set_cardinality = cardinality ACT_RSB_set
      .//print "ACT_RSB ${ACT_RSB_set_cardinality} ${class.Name}"
   .//ACT_RSB ${ACT_RSB_set_cardinality} ${class.Name}
      .select many ACT_SAB_set related by ACT_ACT_set->ACT_SAB[R698]
      .assign ACT_SAB_set_cardinality = cardinality ACT_SAB_set
      .print "ACT_SAB ${ACT_SAB_set_cardinality} ${class.Name}"
ACT_SAB ${ACT_SAB_set_cardinality} ${class.Name}
      .select many ACT_TAB_set related by ACT_ACT_set->ACT_TAB[R698]
      .assign ACT_TAB_set_cardinality = cardinality ACT_TAB_set
      .print "ACT_TAB ${ACT_TAB_set_cardinality} ${class.Name}"
ACT_TAB ${ACT_TAB_set_cardinality} ${class.Name}
      .select many ACT_WHL_set related by ACT_ACT_set->ACT_BLK[R601]->ACT_SMT[R602]->ACT_WHL[R603]
      .assign ACT_WHL_set_cardinality = cardinality ACT_WHL_set
      .print "ACT_WHL ${ACT_WHL_set_cardinality} ${class.Name}"
ACT_WHL ${ACT_WHL_set_cardinality} ${class.Name}
      .//------------------------------------------------------------------------
      .//Component
      .//select many C_C_set related by class->...->C_C[Rxxx]
      .//assign C_C_set_cardinality = cardinality C_C_set
      .//print "C_C ${C_C_set_cardinality} ${class.Name}"
   .//C_C ${C_C_set_cardinality} ${class.Name}
      .//select many C_DG_set related by class->...->C_DG[Rxxx]
      .//assign C_DG_set_cardinality = cardinality C_DG_set
      .//print "C_DG ${C_DG_set_cardinality} ${class.Name}"
   .//C_DG ${C_DG_set_cardinality} ${class.Name}
      .//select many C_EP_set related by class->...->C_EP[Rxxx]
      .//assign C_EP_set_cardinality = cardinality C_EP_set
      .//print "C_EP ${C_EP_set_cardinality} ${class.Name}"
   .//C_EP ${C_EP_set_cardinality} ${class.Name}
      .//select many C_IO_set related by class->...->C_IO[Rxxx]
      .//assign C_IO_set_cardinality = cardinality C_IO_set
      .//print "C_IO ${C_IO_set_cardinality} ${class.Name}"
   .//C_IO ${C_IO_set_cardinality} ${class.Name}
      .//select many C_RID_set related by class->...->C_RID[Rxxx]
      .//assign C_RID_set_cardinality = cardinality C_RID_set
      .//print "C_RID ${C_RID_set_cardinality} ${class.Name}"
   .//C_RID ${C_RID_set_cardinality} ${class.Name}
      .//select many C_IR_set related by class->...->C_IR[Rxxx]
      .//assign C_IR_set_cardinality = cardinality C_IR_set
      .//print "C_IR ${C_IR_set_cardinality} ${class.Name}"
   .//C_IR ${C_IR_set_cardinality} ${class.Name}
      .//select many C_AS_set related by class->...->C_AS[Rxxx]
      .//assign C_AS_set_cardinality = cardinality C_AS_set
      .//print "C_AS ${C_AS_set_cardinality} ${class.Name}"
   .//C_AS ${C_AS_set_cardinality} ${class.Name}
      .//select many C_I_set related by class->...->C_I[Rxxx]
      .//assign C_I_set_cardinality = cardinality C_I_set
      .//print "C_I ${C_I_set_cardinality} ${class.Name}"
   .//C_I ${C_I_set_cardinality} ${class.Name}
      .//select many C_PO_set related by class->...->C_PO[Rxxx]
      .//assign C_PO_set_cardinality = cardinality C_PO_set
      .//print "C_PO ${C_PO_set_cardinality} ${class.Name}"
   .//C_PO ${C_PO_set_cardinality} ${class.Name}
      .//select many C_PP_set related by class->...->C_PP[Rxxx]
      .//assign C_PP_set_cardinality = cardinality C_PP_set
      .//print "C_PP ${C_PP_set_cardinality} ${class.Name}"
   .//C_PP ${C_PP_set_cardinality} ${class.Name}
      .//select many C_P_set related by class->...->C_P[Rxxx]
      .//assign C_P_set_cardinality = cardinality C_P_set
      .//print "C_P ${C_P_set_cardinality} ${class.Name}"
   .//C_P ${C_P_set_cardinality} ${class.Name}
      .//select many C_R_set related by class->...->C_R[Rxxx]
      .//assign C_R_set_cardinality = cardinality C_R_set
      .//print "C_R ${C_R_set_cardinality} ${class.Name}"
   .//C_R ${C_R_set_cardinality} ${class.Name}
      .//select many C_SF_set related by class->...->C_SF[Rxxx]
      .//assign C_SF_set_cardinality = cardinality C_SF_set
      .//print "C_SF ${C_SF_set_cardinality} ${class.Name}"
   .//C_SF ${C_SF_set_cardinality} ${class.Name}
      .//------------------------------------------------------------------------
      .//Component Library
      .//select many CL_IC_set related by class->...->CL_IC[Rxxx]
      .//assign CL_IC_set_cardinality = cardinality CL_IC_set
      .//print "CL_IC ${CL_IC_set_cardinality} ${class.Name}"
   .//CL_IC ${CL_IC_set_cardinality} ${class.Name}
      .//select many CL_IPINS_set related by class->...->CL_IPINS[Rxxx]
      .//assign CL_IPINS_set_cardinality = cardinality CL_IPINS_set
      .//print "CL_IPINS ${CL_IPINS_set_cardinality} ${class.Name}"
   .//CL_IPINS ${CL_IPINS_set_cardinality} ${class.Name}
      .//select many CL_IP_set related by class->...->CL_IP[Rxxx]
      .//assign CL_IP_set_cardinality = cardinality CL_IP_set
      .//print "CL_IP ${CL_IP_set_cardinality} ${class.Name}"
   .//CL_IP ${CL_IP_set_cardinality} ${class.Name}
      .//select many CL_IIR_set related by class->...->CL_IIR[Rxxx]
      .//assign CL_IIR_set_cardinality = cardinality CL_IIR_set
      .//print "CL_IIR ${CL_IIR_set_cardinality} ${class.Name}"
   .//CL_IIR ${CL_IIR_set_cardinality} ${class.Name}
      .//select many CL_IR_set related by class->...->CL_IR[Rxxx]
      .//assign CL_IR_set_cardinality = cardinality CL_IR_set
      .//print "CL_IR ${CL_IR_set_cardinality} ${class.Name}"
   .//CL_IR ${CL_IR_set_cardinality} ${class.Name}
      .//------------------------------------------------------------------------
      .//Component Nesting
      .//select many CN_CIC_set related by class->...->CN_CIC[Rxxx]
      .//assign CN_CIC_set_cardinality = cardinality CN_CIC_set
      .//print "CN_CIC ${CN_CIC_set_cardinality} ${class.Name}"
   .//CN_CIC ${CN_CIC_set_cardinality} ${class.Name}
      .//select many CN_DC_set related by class->...->CN_DC[Rxxx]
      .//assign CN_DC_set_cardinality = cardinality CN_DC_set
      .//print "CN_DC ${CN_DC_set_cardinality} ${class.Name}"
   .//CN_DC ${CN_DC_set_cardinality} ${class.Name}
      .//------------------------------------------------------------------------
      .//Component Packaging
      .//select many CP_CPINP_set related by class->...->CP_CPINP[Rxxx]
      .//assign CP_CPINP_set_cardinality = cardinality CP_CPINP_set
      .//print "CP_CPINP ${CP_CPINP_set_cardinality} ${class.Name}"
   .//CP_CPINP ${CP_CPINP_set_cardinality} ${class.Name}
      .//select many CP_CP_set related by class->...->CP_CP[Rxxx]
      .//assign CP_CP_set_cardinality = cardinality CP_CP_set
      .//print "CP_CP ${CP_CP_set_cardinality} ${class.Name}"
   .//CP_CP ${CP_CP_set_cardinality} ${class.Name}
      .//------------------------------------------------------------------------
      .//Signal Provisions and Requirements
      .//select many SPR_PEP_set related by class->...->SPR_PEP[Rxxx]
      .//assign SPR_PEP_set_cardinality = cardinality SPR_PEP_set
      .//print "SPR_PEP ${SPR_PEP_set_cardinality} ${class.Name}"
   .//SPR_PEP ${SPR_PEP_set_cardinality} ${class.Name}
      .//select many SPR_PO_set related by class->...->SPR_PO[Rxxx]
      .//assign SPR_PO_set_cardinality = cardinality SPR_PO_set
      .//print "SPR_PO ${SPR_PO_set_cardinality} ${class.Name}"
   .//SPR_PO ${SPR_PO_set_cardinality} ${class.Name}
      .//select many SPR_PS_set related by class->...->SPR_PS[Rxxx]
      .//assign SPR_PS_set_cardinality = cardinality SPR_PS_set
      .//print "SPR_PS ${SPR_PS_set_cardinality} ${class.Name}"
   .//SPR_PS ${SPR_PS_set_cardinality} ${class.Name}
      .//select many SPR_REP_set related by class->...->SPR_REP[Rxxx]
      .//assign SPR_REP_set_cardinality = cardinality SPR_REP_set
      .//print "SPR_REP ${SPR_REP_set_cardinality} ${class.Name}"
   .//SPR_REP ${SPR_REP_set_cardinality} ${class.Name}
      .//select many SPR_RO_set related by class->...->SPR_RO[Rxxx]
      .//assign SPR_RO_set_cardinality = cardinality SPR_RO_set
      .//print "SPR_RO ${SPR_RO_set_cardinality} ${class.Name}"
   .//SPR_RO ${SPR_RO_set_cardinality} ${class.Name}
      .//select many SPR_RS_set related by class->...->SPR_RS[Rxxx]
      .//assign SPR_RS_set_cardinality = cardinality SPR_RS_set
      .//print "SPR_RS ${SPR_RS_set_cardinality} ${class.Name}"
   .//SPR_RS ${SPR_RS_set_cardinality} ${class.Name}
      .//------------------------------------------------------------------------
      .//Constants
      .//select many CNST_CSP_set related by class->...->CNST_CSP[Rxxx]
      .//assign CNST_CSP_set_cardinality = cardinality CNST_CSP_set
      .//print "CNST_CSP ${CNST_CSP_set_cardinality} ${class.Name}"
   .//CNST_CSP ${CNST_CSP_set_cardinality} ${class.Name}
      .//select many CNST_CIP_set related by class->...->CNST_CIP[Rxxx]
      .//assign CNST_CIP_set_cardinality = cardinality CNST_CIP_set
      .//print "CNST_CIP ${CNST_CIP_set_cardinality} ${class.Name}"
   .//CNST_CIP ${CNST_CIP_set_cardinality} ${class.Name}
      .//select many CNST_LFSC_set related by class->...->CNST_LFSC[Rxxx]
      .//assign CNST_LFSC_set_cardinality = cardinality CNST_LFSC_set
      .//print "CNST_LFSC ${CNST_LFSC_set_cardinality} ${class.Name}"
   .//CNST_LFSC ${CNST_LFSC_set_cardinality} ${class.Name}
      .//select many CNST_LSC_set related by class->...->CNST_LSC[Rxxx]
      .//assign CNST_LSC_set_cardinality = cardinality CNST_LSC_set
      .//print "CNST_LSC ${CNST_LSC_set_cardinality} ${class.Name}"
   .//CNST_LSC ${CNST_LSC_set_cardinality} ${class.Name}
      .//select many CNST_SYC_set related by class->...->CNST_SYC[Rxxx]
      .//assign CNST_SYC_set_cardinality = cardinality CNST_SYC_set
      .//print "CNST_SYC ${CNST_SYC_set_cardinality} ${class.Name}"
   .//CNST_SYC ${CNST_SYC_set_cardinality} ${class.Name}
      .//------------------------------------------------------------------------
      .//Domain
      .//select many S_BPARM_set related by class->...->S_BPARM[Rxxx]
      .//assign S_BPARM_set_cardinality = cardinality S_BPARM_set
      .//print "S_BPARM ${S_BPARM_set_cardinality} ${class.Name}"
   .//S_BPARM ${S_BPARM_set_cardinality} ${class.Name}
      .//select many S_BRG_set related by class->...->S_BRG[Rxxx]
      .//assign S_BRG_set_cardinality = cardinality S_BRG_set
      .//print "S_BRG ${S_BRG_set_cardinality} ${class.Name}"
   .//S_BRG ${S_BRG_set_cardinality} ${class.Name}
      .//select many S_CDT_set related by class->...->S_CDT[Rxxx]
      .//assign S_CDT_set_cardinality = cardinality S_CDT_set
      .//print "S_CDT ${S_CDT_set_cardinality} ${class.Name}"
   .//S_CDT ${S_CDT_set_cardinality} ${class.Name}
      .//select many S_DPIP_set related by class->...->S_DPIP[Rxxx]
      .//assign S_DPIP_set_cardinality = cardinality S_DPIP_set
      .//print "S_DPIP ${S_DPIP_set_cardinality} ${class.Name}"
   .//S_DPIP ${S_DPIP_set_cardinality} ${class.Name}
      .//select many S_DPK_set related by class->...->S_DPK[Rxxx]
      .//assign S_DPK_set_cardinality = cardinality S_DPK_set
      .//print "S_DPK ${S_DPK_set_cardinality} ${class.Name}"
   .//S_DPK ${S_DPK_set_cardinality} ${class.Name}
      .//select many S_DIP_set related by class->...->S_DIP[Rxxx]
      .//assign S_DIP_set_cardinality = cardinality S_DIP_set
      .//print "S_DIP ${S_DIP_set_cardinality} ${class.Name}"
   .//S_DIP ${S_DIP_set_cardinality} ${class.Name}
      .//select many S_DT_set related by class->...->S_DT[Rxxx]
      .//assign S_DT_set_cardinality = cardinality S_DT_set
      .//print "S_DT ${S_DT_set_cardinality} ${class.Name}"
   .//S_DT ${S_DT_set_cardinality} ${class.Name}
      .//select many S_DIS_set related by class->...->S_DIS[Rxxx]
      .//assign S_DIS_set_cardinality = cardinality S_DIS_set
      .//print "S_DIS ${S_DIS_set_cardinality} ${class.Name}"
   .//S_DIS ${S_DIS_set_cardinality} ${class.Name}
      .//select many S_DIM_set related by class->...->S_DIM[Rxxx]
      .//assign S_DIM_set_cardinality = cardinality S_DIM_set
      .//print "S_DIM ${S_DIM_set_cardinality} ${class.Name}"
   .//S_DIM ${S_DIM_set_cardinality} ${class.Name}
      .//select many S_DOM_set related by class->...->S_DOM[Rxxx]
      .//assign S_DOM_set_cardinality = cardinality S_DOM_set
      .//print "S_DOM ${S_DOM_set_cardinality} ${class.Name}"
   .//S_DOM ${S_DOM_set_cardinality} ${class.Name}
      .//select many S_EEPIP_set related by class->...->S_EEPIP[Rxxx]
      .//assign S_EEPIP_set_cardinality = cardinality S_EEPIP_set
      .//print "S_EEPIP ${S_EEPIP_set_cardinality} ${class.Name}"
   .//S_EEPIP ${S_EEPIP_set_cardinality} ${class.Name}
      .//select many S_EDT_set related by class->...->S_EDT[Rxxx]
      .//assign S_EDT_set_cardinality = cardinality S_EDT_set
      .//print "S_EDT ${S_EDT_set_cardinality} ${class.Name}"
   .//S_EDT ${S_EDT_set_cardinality} ${class.Name}
      .//select many S_ENUM_set related by class->...->S_ENUM[Rxxx]
      .//assign S_ENUM_set_cardinality = cardinality S_ENUM_set
      .//print "S_ENUM ${S_ENUM_set_cardinality} ${class.Name}"
   .//S_ENUM ${S_ENUM_set_cardinality} ${class.Name}
      .//select many S_EEDI_set related by class->...->S_EEDI[Rxxx]
      .//assign S_EEDI_set_cardinality = cardinality S_EEDI_set
      .//print "S_EEDI ${S_EEDI_set_cardinality} ${class.Name}"
   .//S_EEDI ${S_EEDI_set_cardinality} ${class.Name}
      .//select many S_EEEDI_set related by class->...->S_EEEDI[Rxxx]
      .//assign S_EEEDI_set_cardinality = cardinality S_EEEDI_set
      .//print "S_EEEDI ${S_EEEDI_set_cardinality} ${class.Name}"
   .//S_EEEDI ${S_EEEDI_set_cardinality} ${class.Name}
      .//select many S_EEEDT_set related by class->...->S_EEEDT[Rxxx]
      .//assign S_EEEDT_set_cardinality = cardinality S_EEEDT_set
      .//print "S_EEEDT ${S_EEEDT_set_cardinality} ${class.Name}"
   .//S_EEEDT ${S_EEEDT_set_cardinality} ${class.Name}
      .//select many S_EEEVT_set related by class->...->S_EEEVT[Rxxx]
      .//assign S_EEEVT_set_cardinality = cardinality S_EEEVT_set
      .//print "S_EEEVT ${S_EEEVT_set_cardinality} ${class.Name}"
   .//S_EEEVT ${S_EEEVT_set_cardinality} ${class.Name}
      .//select many S_EEPK_set related by class->...->S_EEPK[Rxxx]
      .//assign S_EEPK_set_cardinality = cardinality S_EEPK_set
      .//print "S_EEPK ${S_EEPK_set_cardinality} ${class.Name}"
   .//S_EEPK ${S_EEPK_set_cardinality} ${class.Name}
      .//select many S_EEM_set related by class->...->S_EEM[Rxxx]
      .//assign S_EEM_set_cardinality = cardinality S_EEM_set
      .//print "S_EEM ${S_EEM_set_cardinality} ${class.Name}"
   .//S_EEM ${S_EEM_set_cardinality} ${class.Name}
      .//select many S_EEIP_set related by class->...->S_EEIP[Rxxx]
      .//assign S_EEIP_set_cardinality = cardinality S_EEIP_set
      .//print "S_EEIP ${S_EEIP_set_cardinality} ${class.Name}"
   .//S_EEIP ${S_EEIP_set_cardinality} ${class.Name}
      .//select many S_EE_set related by class->...->S_EE[Rxxx]
      .//assign S_EE_set_cardinality = cardinality S_EE_set
      .//print "S_EE ${S_EE_set_cardinality} ${class.Name}"
   .//S_EE ${S_EE_set_cardinality} ${class.Name}
      .//select many S_FPIP_set related by class->...->S_FPIP[Rxxx]
      .//assign S_FPIP_set_cardinality = cardinality S_FPIP_set
      .//print "S_FPIP ${S_FPIP_set_cardinality} ${class.Name}"
   .//S_FPIP ${S_FPIP_set_cardinality} ${class.Name}
      .//select many S_FPK_set related by class->...->S_FPK[Rxxx]
      .//assign S_FPK_set_cardinality = cardinality S_FPK_set
      .//print "S_FPK ${S_FPK_set_cardinality} ${class.Name}"
   .//S_FPK ${S_FPK_set_cardinality} ${class.Name}
      .//select many S_SPARM_set related by class->...->S_SPARM[Rxxx]
      .//assign S_SPARM_set_cardinality = cardinality S_SPARM_set
      .//print "S_SPARM ${S_SPARM_set_cardinality} ${class.Name}"
   .//S_SPARM ${S_SPARM_set_cardinality} ${class.Name}
      .//select many S_FIP_set related by class->...->S_FIP[Rxxx]
      .//assign S_FIP_set_cardinality = cardinality S_FIP_set
      .//print "S_FIP ${S_FIP_set_cardinality} ${class.Name}"
   .//S_FIP ${S_FIP_set_cardinality} ${class.Name}
      .//select many S_SYNC_set related by class->...->S_SYNC[Rxxx]
      .//assign S_SYNC_set_cardinality = cardinality S_SYNC_set
      .//print "S_SYNC ${S_SYNC_set_cardinality} ${class.Name}"
   .//S_SYNC ${S_SYNC_set_cardinality} ${class.Name}
      .//select many S_IRDT_set related by class->...->S_IRDT[Rxxx]
      .//assign S_IRDT_set_cardinality = cardinality S_IRDT_set
      .//print "S_IRDT ${S_IRDT_set_cardinality} ${class.Name}"
   .//S_IRDT ${S_IRDT_set_cardinality} ${class.Name}
      .//select many S_MBR_set related by class->...->S_MBR[Rxxx]
      .//assign S_MBR_set_cardinality = cardinality S_MBR_set
      .//print "S_MBR ${S_MBR_set_cardinality} ${class.Name}"
   .//S_MBR ${S_MBR_set_cardinality} ${class.Name}
      .//select many S_SDT_set related by class->...->S_SDT[Rxxx]
      .//assign S_SDT_set_cardinality = cardinality S_SDT_set
      .//print "S_SDT ${S_SDT_set_cardinality} ${class.Name}"
   .//S_SDT ${S_SDT_set_cardinality} ${class.Name}
      .//select many S_SID_set related by class->...->S_SID[Rxxx]
      .//assign S_SID_set_cardinality = cardinality S_SID_set
      .//print "S_SID ${S_SID_set_cardinality} ${class.Name}"
   .//S_SID ${S_SID_set_cardinality} ${class.Name}
      .//select many S_SIS_set related by class->...->S_SIS[Rxxx]
      .//assign S_SIS_set_cardinality = cardinality S_SIS_set
      .//print "S_SIS ${S_SIS_set_cardinality} ${class.Name}"
   .//S_SIS ${S_SIS_set_cardinality} ${class.Name}
      .//select many S_SS_set related by class->...->S_SS[Rxxx]
      .//assign S_SS_set_cardinality = cardinality S_SS_set
      .//print "S_SS ${S_SS_set_cardinality} ${class.Name}"
   .//S_SS ${S_SS_set_cardinality} ${class.Name}
      .//select many S_SYS_set related by class->...->S_SYS[Rxxx]
      .//assign S_SYS_set_cardinality = cardinality S_SYS_set
      .//print "S_SYS ${S_SYS_set_cardinality} ${class.Name}"
   .//S_SYS ${S_SYS_set_cardinality} ${class.Name}
      .//select many S_UDT_set related by class->...->S_UDT[Rxxx]
      .//assign S_UDT_set_cardinality = cardinality S_UDT_set
      .//print "S_UDT ${S_UDT_set_cardinality} ${class.Name}"
   .//S_UDT ${S_UDT_set_cardinality} ${class.Name}
      .//------------------------------------------------------------------------
      .//Package Linking
      .//select many PL_EEPID_set related by class->...->PL_EEPID[Rxxx]
      .//assign PL_EEPID_set_cardinality = cardinality PL_EEPID_set
      .//print "PL_EEPID ${PL_EEPID_set_cardinality} ${class.Name}"
   .//PL_EEPID ${PL_EEPID_set_cardinality} ${class.Name}
      .//select many PL_FPID_set related by class->...->PL_FPID[Rxxx]
      .//assign PL_FPID_set_cardinality = cardinality PL_FPID_set
      .//print "PL_FPID ${PL_FPID_set_cardinality} ${class.Name}"
   .//PL_FPID ${PL_FPID_set_cardinality} ${class.Name}
      .//------------------------------------------------------------------------
      .//Element Packaging
      .//select many EP_PIP_set related by class->...->EP_PIP[Rxxx]
      .//assign EP_PIP_set_cardinality = cardinality EP_PIP_set
      .//print "EP_PIP ${EP_PIP_set_cardinality} ${class.Name}"
   .//EP_PIP ${EP_PIP_set_cardinality} ${class.Name}
      .//select many EP_PKG_set related by class->...->EP_PKG[Rxxx]
      .//assign EP_PKG_set_cardinality = cardinality EP_PKG_set
      .//print "EP_PKG ${EP_PKG_set_cardinality} ${class.Name}"
   .//EP_PKG ${EP_PKG_set_cardinality} ${class.Name}
      .//select many EP_SPKG_set related by class->...->EP_SPKG[Rxxx]
      .//assign EP_SPKG_set_cardinality = cardinality EP_SPKG_set
      .//print "EP_SPKG ${EP_SPKG_set_cardinality} ${class.Name}"
   .//EP_SPKG ${EP_SPKG_set_cardinality} ${class.Name}
      .//------------------------------------------------------------------------
      .//Event
      .//select many E_CES_set related by class->...->E_CES[Rxxx]
      .//assign E_CES_set_cardinality = cardinality E_CES_set
      .//print "E_CES ${E_CES_set_cardinality} ${class.Name}"
   .//E_CES ${E_CES_set_cardinality} ${class.Name}
      .//select many E_CEA_set related by class->...->E_CEA[Rxxx]
      .//assign E_CEA_set_cardinality = cardinality E_CEA_set
      .//print "E_CEA ${E_CEA_set_cardinality} ${class.Name}"
   .//E_CEA ${E_CEA_set_cardinality} ${class.Name}
      .//select many E_CEC_set related by class->...->E_CEC[Rxxx]
      .//assign E_CEC_set_cardinality = cardinality E_CEC_set
      .//print "E_CEC ${E_CEC_set_cardinality} ${class.Name}"
   .//E_CEC ${E_CEC_set_cardinality} ${class.Name}
      .//select many E_CEE_set related by class->...->E_CEE[Rxxx]
      .//assign E_CEE_set_cardinality = cardinality E_CEE_set
      .//print "E_CEE ${E_CEE_set_cardinality} ${class.Name}"
   .//E_CEE ${E_CEE_set_cardinality} ${class.Name}
      .//select many E_CEI_set related by class->...->E_CEI[Rxxx]
      .//assign E_CEI_set_cardinality = cardinality E_CEI_set
      .//print "E_CEI ${E_CEI_set_cardinality} ${class.Name}"
   .//E_CEI ${E_CEI_set_cardinality} ${class.Name}
      .//select many E_CSME_set related by class->...->E_CSME[Rxxx]
      .//assign E_CSME_set_cardinality = cardinality E_CSME_set
      .//print "E_CSME ${E_CSME_set_cardinality} ${class.Name}"
   .//E_CSME ${E_CSME_set_cardinality} ${class.Name}
      .select many E_ESS_set related by ACT_ACT_set->ACT_BLK[R601]->ACT_SMT[R602]->E_ESS[R603]
      .assign E_ESS_set_cardinality = cardinality E_ESS_set
      .print "E_ESS ${E_ESS_set_cardinality} ${class.Name}"
E_ESS ${E_ESS_set_cardinality} ${class.Name}
      .select many E_GES_set related by ACT_ACT_set->ACT_BLK[R601]->ACT_SMT[R602]->E_ESS[R603]->E_GES[R701]
      .assign E_GES_set_cardinality = cardinality E_GES_set
      .print "E_GES ${E_GES_set_cardinality} ${class.Name}"
E_GES ${E_GES_set_cardinality} ${class.Name}
      .//select many E_GPR_set related by class->...->E_GPR[Rxxx]
      .//assign E_GPR_set_cardinality = cardinality E_GPR_set
      .//print "E_GPR ${E_GPR_set_cardinality} ${class.Name}"
   .//E_GPR ${E_GPR_set_cardinality} ${class.Name}
      .select many E_GSME_set related by ACT_ACT_set->ACT_BLK[R601]->ACT_SMT[R602]->E_ESS[R603]->E_GES[R701]->E_GSME[R703]
      .assign E_GSME_set_cardinality = cardinality E_GSME_set
      .print "E_GSME ${E_GSME_set_cardinality} ${class.Name}"
E_GSME ${E_GSME_set_cardinality} ${class.Name}
      .//select many E_GAR_set related by class->...->E_GAR[Rxxx]
      .//assign E_GAR_set_cardinality = cardinality E_GAR_set
      .//print "E_GAR ${E_GAR_set_cardinality} ${class.Name}"
   .//E_GAR ${E_GAR_set_cardinality} ${class.Name}
      .//select many E_GEC_set related by class->...->E_GEC[Rxxx]
      .//assign E_GEC_set_cardinality = cardinality E_GEC_set
      .//print "E_GEC ${E_GEC_set_cardinality} ${class.Name}"
   .//E_GEC ${E_GEC_set_cardinality} ${class.Name}
      .//select many E_GEE_set related by class->...->E_GEE[Rxxx]
      .//assign E_GEE_set_cardinality = cardinality E_GEE_set
      .//print "E_GEE ${E_GEE_set_cardinality} ${class.Name}"
   .//E_GEE ${E_GEE_set_cardinality} ${class.Name}
      .select many E_GEN_set related by ACT_ACT_set->ACT_BLK[R601]->ACT_SMT[R602]->E_ESS[R603]->E_GES[R701]->E_GSME[R703]->E_GEN[R705]
      .assign E_GEN_set_cardinality = cardinality E_GEN_set
      .print "E_GEN ${E_GEN_set_cardinality} ${class.Name}"
E_GEN ${E_GEN_set_cardinality} ${class.Name}
      .//------------------------------------------------------------------------
      .//Globals
      .//select many G_EIS_set related by class->...->G_EIS[Rxxx]
      .//assign G_EIS_set_cardinality = cardinality G_EIS_set
      .//print "G_EIS ${G_EIS_set_cardinality} ${class.Name}"
   .//G_EIS ${G_EIS_set_cardinality} ${class.Name}
      .//------------------------------------------------------------------------
      .//Instance Access
      .select many ACT_AI_set related by ACT_ACT_set->ACT_BLK[R601]->ACT_SMT[R602]->ACT_AI[R603]
      .assign ACT_AI_set_cardinality = cardinality ACT_AI_set
      .print "ACT_AI ${ACT_AI_set_cardinality} ${class.Name}"
ACT_AI ${ACT_AI_set_cardinality} ${class.Name}
      .//select many ACT_CNV_set related by class->...->ACT_CNV[Rxxx]
      .//assign ACT_CNV_set_cardinality = cardinality ACT_CNV_set
      .//print "ACT_CNV ${ACT_CNV_set_cardinality} ${class.Name}"
   .//ACT_CNV ${ACT_CNV_set_cardinality} ${class.Name}
      .select many ACT_CR_set related by ACT_ACT_set->ACT_BLK[R601]->ACT_SMT[R602]->ACT_CR[R603]
      .assign ACT_CR_set_cardinality = cardinality ACT_CR_set
      .print "ACT_CR ${ACT_CR_set_cardinality} ${class.Name}"
ACT_CR ${ACT_CR_set_cardinality} ${class.Name}
      .select many ACT_DEL_set related by ACT_ACT_set->ACT_BLK[R601]->ACT_SMT[R602]->ACT_DEL[R603]
      .assign ACT_DEL_set_cardinality = cardinality ACT_DEL_set
      .print "ACT_DEL ${ACT_DEL_set_cardinality} ${class.Name}"
ACT_DEL ${ACT_DEL_set_cardinality} ${class.Name}
      .//------------------------------------------------------------------------
      .//Interface Package
      .//select many IP_IPINIP_set related by class->...->IP_IPINIP[Rxxx]
      .//assign IP_IPINIP_set_cardinality = cardinality IP_IPINIP_set
      .//print "IP_IPINIP ${IP_IPINIP_set_cardinality} ${class.Name}"
   .//IP_IPINIP ${IP_IPINIP_set_cardinality} ${class.Name}
      .//select many IP_IP_set related by class->...->IP_IP[Rxxx]
      .//assign IP_IP_set_cardinality = cardinality IP_IP_set
      .//print "IP_IP ${IP_IP_set_cardinality} ${class.Name}"
   .//IP_IP ${IP_IP_set_cardinality} ${class.Name}
      .//------------------------------------------------------------------------
      .//Invocation
      .select many ACT_BRG_set related by ACT_ACT_set->ACT_BLK[R601]->ACT_SMT[R602]->ACT_BRG[R603]
      .assign ACT_BRG_set_cardinality = cardinality ACT_BRG_set
      .print "ACT_BRG ${ACT_BRG_set_cardinality} ${class.Name}"
ACT_BRG ${ACT_BRG_set_cardinality} ${class.Name}
      .select many ACT_FNC_set related by ACT_ACT_set->ACT_BLK[R601]->ACT_SMT[R602]->ACT_FNC[R603]
      .assign ACT_FNC_set_cardinality = cardinality ACT_FNC_set
      .print "ACT_FNC ${ACT_FNC_set_cardinality} ${class.Name}"
ACT_FNC ${ACT_FNC_set_cardinality} ${class.Name}
      .//select many ACT_IOP_set related by class->...->ACT_IOP[Rxxx]
      .//assign ACT_IOP_set_cardinality = cardinality ACT_IOP_set
      .//print "ACT_IOP ${ACT_IOP_set_cardinality} ${class.Name}"
   .//ACT_IOP ${ACT_IOP_set_cardinality} ${class.Name}
      .select many ACT_TFM_set related by ACT_ACT_set->ACT_BLK[R601]->ACT_SMT[R602]->ACT_TFM[R603]
      .assign ACT_TFM_set_cardinality = cardinality ACT_TFM_set
      .print "ACT_TFM ${ACT_TFM_set_cardinality} ${class.Name}"
ACT_TFM ${ACT_TFM_set_cardinality} ${class.Name}
      .select many ACT_RET_set related by ACT_ACT_set->ACT_BLK[R601]->ACT_SMT[R602]->ACT_RET[R603]
      .assign ACT_RET_set_cardinality = cardinality ACT_RET_set
      .print "ACT_RET ${ACT_RET_set_cardinality} ${class.Name}"
ACT_RET ${ACT_RET_set_cardinality} ${class.Name}
      .select many ACT_SGN_set related by ACT_ACT_set->ACT_BLK[R601]->ACT_SMT[R602]->ACT_SGN[R603]
      .assign ACT_SGN_set_cardinality = cardinality ACT_SGN_set
      .print "ACT_SGN ${ACT_SGN_set_cardinality} ${class.Name}"
ACT_SGN ${ACT_SGN_set_cardinality} ${class.Name}
      .//------------------------------------------------------------------------
      .//Packageable Element
      .//select many PE_CRS_set related by class->...->PE_CRS[Rxxx]
      .//assign PE_CRS_set_cardinality = cardinality PE_CRS_set
      .//print "PE_CRS ${PE_CRS_set_cardinality} ${class.Name}"
   .//PE_CRS ${PE_CRS_set_cardinality} ${class.Name}
      .//select many PE_CVS_set related by class->...->PE_CVS[Rxxx]
      .//assign PE_CVS_set_cardinality = cardinality PE_CVS_set
      .//print "PE_CVS ${PE_CVS_set_cardinality} ${class.Name}"
   .//PE_CVS ${PE_CVS_set_cardinality} ${class.Name}
      .//select many PE_VIS_set related by class->...->PE_VIS[Rxxx]
      .//assign PE_VIS_set_cardinality = cardinality PE_VIS_set
      .//print "PE_VIS ${PE_VIS_set_cardinality} ${class.Name}"
   .//PE_VIS ${PE_VIS_set_cardinality} ${class.Name}
      .//select many PE_PE_set related by class->...->PE_PE[Rxxx]
      .//assign PE_PE_set_cardinality = cardinality PE_PE_set
      .//print "PE_PE ${PE_PE_set_cardinality} ${class.Name}"
   .//PE_PE ${PE_PE_set_cardinality} ${class.Name}
      .//select many PE_SRS_set related by class->...->PE_SRS[Rxxx]
      .//assign PE_SRS_set_cardinality = cardinality PE_SRS_set
      .//print "PE_SRS ${PE_SRS_set_cardinality} ${class.Name}"
   .//PE_SRS ${PE_SRS_set_cardinality} ${class.Name}
      .//------------------------------------------------------------------------
      .//Relate And Unrelate
      .//select many ACT_RU_set related by class->...->ACT_RU[Rxxx]
      .//assign ACT_RU_set_cardinality = cardinality ACT_RU_set
      .//print "ACT_RU ${ACT_RU_set_cardinality} ${class.Name}"
   .//ACT_RU ${ACT_RU_set_cardinality} ${class.Name}
      .select many ACT_REL_set related by ACT_ACT_set->ACT_BLK[R601]->ACT_SMT[R602]->ACT_REL[R603]
      .assign ACT_REL_set_cardinality = cardinality ACT_REL_set
      .print "ACT_REL ${ACT_REL_set_cardinality} ${class.Name}"
ACT_REL ${ACT_REL_set_cardinality} ${class.Name}
      .//select many ACT_URU_set related by class->...->ACT_URU[Rxxx]
      .//assign ACT_URU_set_cardinality = cardinality ACT_URU_set
      .//print "ACT_URU ${ACT_URU_set_cardinality} ${class.Name}"
   .//ACT_URU ${ACT_URU_set_cardinality} ${class.Name}
      .select many ACT_UNR_set related by ACT_ACT_set->ACT_BLK[R601]->ACT_SMT[R602]->ACT_UNR[R603]
      .assign ACT_UNR_set_cardinality = cardinality ACT_UNR_set
      .print "ACT_UNR ${ACT_UNR_set_cardinality} ${class.Name}"
ACT_UNR ${ACT_UNR_set_cardinality} ${class.Name}
      .//------------------------------------------------------------------------
      .//Selection
      .select many ACT_LNK_set from instances of ACT_LNK where false
      .select many first_ACT_LNK_set related by ACT_ACT_set->ACT_BLK[R601]->ACT_SMT[R602]->ACT_SEL[R603]->ACT_LNK[R637]
      .for each first_ACT_LNK_inst in first_ACT_LNK_set
         .assign ACT_LNK_set = ACT_LNK_set|first_ACT_LNK_inst
         .select many empty_ACT_LNK_set from instances of ACT_LNK where false
         .invoke fls = find_remaining_links(first_ACT_LNK_inst, empty_ACT_LNK_set)
         .assign further_lnk_set = fls.result
         .assign ACT_LNK_set = ACT_LNK_set|further_lnk_set
      .end for
      .assign ACT_LNK_set_cardinality = cardinality ACT_LNK_set
      .print "ACT_LNK ${ACT_LNK_set_cardinality} ${class.Name}"
ACT_LNK ${ACT_LNK_set_cardinality} ${class.Name}
      .select many ACT_FIW_set related by ACT_ACT_set->ACT_BLK[R601]->ACT_SMT[R602]->ACT_FIW[R603]
      .assign ACT_FIW_set_cardinality = cardinality ACT_FIW_set
      .print "ACT_FIW ${ACT_FIW_set_cardinality} ${class.Name}"
ACT_FIW ${ACT_FIW_set_cardinality} ${class.Name}
      .select many ACT_FIO_set related by ACT_ACT_set->ACT_BLK[R601]->ACT_SMT[R602]->ACT_FIO[R603]
      .assign ACT_FIO_set_cardinality = cardinality ACT_FIO_set
      .print "ACT_FIO ${ACT_FIO_set_cardinality} ${class.Name}"
ACT_FIO ${ACT_FIO_set_cardinality} ${class.Name}
      .select many ACT_SR_set related by ACT_ACT_set->ACT_BLK[R601]->ACT_SMT[R602]->ACT_SEL[R603]->ACT_SR[R664]
      .assign ACT_SR_set_cardinality = cardinality ACT_SR_set
      .print "ACT_SR ${ACT_SR_set_cardinality} ${class.Name}"
ACT_SR ${ACT_SR_set_cardinality} ${class.Name}
      .//select many ACT_SRW_set related by class->...->ACT_SRW[Rxxx]
      .//assign ACT_SRW_set_cardinality = cardinality ACT_SRW_set
      .//print "ACT_SRW ${ACT_SRW_set_cardinality} ${class.Name}"
   .//ACT_SRW ${ACT_SRW_set_cardinality} ${class.Name}
      .select many ACT_SEL_set related by ACT_ACT_set->ACT_BLK[R601]->ACT_SMT[R602]->ACT_SEL[R603]
      .assign ACT_SEL_set_cardinality = cardinality ACT_SEL_set
      .print "ACT_SEL ${ACT_SEL_set_cardinality} ${class.Name}"
ACT_SEL ${ACT_SEL_set_cardinality} ${class.Name}
      .//------------------------------------------------------------------------
      .//State Machine
      .select many SM_AH_set related by class->SM_ISM[R518]->SM_SM[R517]->SM_ACT[R515]->SM_AH[R514]
      .assign SM_AH_set_cardinality = cardinality SM_AH_set
      .print "SM_AH ${SM_AH_set_cardinality} ${class.Name}"
SM_AH ${SM_AH_set_cardinality} ${class.Name}
      .select many SM_ACT_set related by class->SM_ISM[R518]->SM_SM[R517]->SM_ACT[R515]
      .assign SM_ACT_set_cardinality = cardinality SM_ACT_set
      .print "SM_ACT ${SM_ACT_set_cardinality} ${class.Name}"
SM_ACT ${SM_ACT_set_cardinality} ${class.Name}
      .select many SM_CH_set related by class->SM_ISM[R518]->SM_SM[R517]->SM_STATE[R501]->SM_SEME[R503]->SM_CH[R504]
      .assign SM_CH_set_cardinality = cardinality SM_CH_set
      .print "SM_CH ${SM_CH_set_cardinality} ${class.Name}"
SM_CH ${SM_CH_set_cardinality} ${class.Name}
      .//select many SM_ASM_set related by class->...->SM_ASM[Rxxx]
      .//assign SM_ASM_set_cardinality = cardinality SM_ASM_set
      .//print "SM_ASM ${SM_ASM_set_cardinality} ${class.Name}"
   .//SM_ASM ${SM_ASM_set_cardinality} ${class.Name}
      .//select many SM_CRTXN_set related by class->...->SM_CRTXN[Rxxx]
      .//assign SM_CRTXN_set_cardinality = cardinality SM_CRTXN_set
      .//print "SM_CRTXN ${SM_CRTXN_set_cardinality} ${class.Name}"
   .//SM_CRTXN ${SM_CRTXN_set_cardinality} ${class.Name}
      .//select many SM_EIGN_set related by class->SM_ISM[R518]->SM_SM[R517]->SM_STATE[R501]->SM_SEME[R503]->SM_EIGN[R504]
      .//assign SM_EIGN_set_cardinality = cardinality SM_EIGN_set
      .//print "SM_EIGN ${SM_EIGN_set_cardinality} ${class.Name}"
   .//SM_EIGN ${SM_EIGN_set_cardinality} ${class.Name}
      .//select many SM_SUPDT_set related by class->...->SM_SUPDT[Rxxx]
      .//assign SM_SUPDT_set_cardinality = cardinality SM_SUPDT_set
      .//print "SM_SUPDT ${SM_SUPDT_set_cardinality} ${class.Name}"
   .//SM_SUPDT ${SM_SUPDT_set_cardinality} ${class.Name}
      .select many SM_ISM_set related by class->SM_ISM[R518]
      .assign SM_ISM_set_cardinality = cardinality SM_ISM_set
      .print "SM_ISM ${SM_ISM_set_cardinality} ${class.Name}"
SM_ISM ${SM_ISM_set_cardinality} ${class.Name}
      .select many SM_LEVT_set related by class->SM_ISM[R518]->SM_SM[R517]->SM_EVT[R502]->SM_SEVT[R525]->SM_LEVT[R526]
      .assign SM_LEVT_set_cardinality = cardinality SM_LEVT_set
      .print "SM_LEVT ${SM_LEVT_set_cardinality} ${class.Name}"
SM_LEVT ${SM_LEVT_set_cardinality} ${class.Name}
      .//select many SM_MEAH_set related by class->...->SM_MEAH[Rxxx]
      .//assign SM_MEAH_set_cardinality = cardinality SM_MEAH_set
      .//print "SM_MEAH ${SM_MEAH_set_cardinality} ${class.Name}"
   .//SM_MEAH ${SM_MEAH_set_cardinality} ${class.Name}
      .//select many SM_MEALY_set related by class->...->SM_MEALY[Rxxx]
      .//assign SM_MEALY_set_cardinality = cardinality SM_MEALY_set
      .//print "SM_MEALY ${SM_MEALY_set_cardinality} ${class.Name}"
   .//SM_MEALY ${SM_MEALY_set_cardinality} ${class.Name}
      .select many SM_MOAH_set related by class->SM_ISM[R518]->SM_SM[R517]->SM_ACT[R515]->SM_AH[R514]->SM_MOAH[R513]
      .assign SM_MOAH_set_cardinality = cardinality SM_MOAH_set
      .print "SM_MOAH ${SM_MOAH_set_cardinality} ${class.Name}"
SM_MOAH ${SM_MOAH_set_cardinality} ${class.Name}
      .select many SM_MOORE_set related by class->SM_ISM[R518]->SM_SM[R517]->SM_MOORE[R510]
      .assign SM_MOORE_set_cardinality = cardinality SM_MOORE_set
      .print "SM_MOORE ${SM_MOORE_set_cardinality} ${class.Name}"
SM_MOORE ${SM_MOORE_set_cardinality} ${class.Name}
      .select many SM_NSTXN_set related by class->SM_ISM[R518]->SM_SM[R517]->SM_STATE[R501]->SM_SEME[R503]->SM_NSTXN[R504]
      .assign SM_NSTXN_set_cardinality = cardinality SM_NSTXN_set
      .print "SM_NSTXN ${SM_NSTXN_set_cardinality} ${class.Name}"
SM_NSTXN ${SM_NSTXN_set_cardinality} ${class.Name}
      .//select many SM_NETXN_set related by class->...->SM_NETXN[Rxxx]
      .//assign SM_NETXN_set_cardinality = cardinality SM_NETXN_set
      .//print "SM_NETXN ${SM_NETXN_set_cardinality} ${class.Name}"
   .//SM_NETXN ${SM_NETXN_set_cardinality} ${class.Name}
      .//select many SM_NLEVT_set related by class->...->SM_NLEVT[Rxxx]
      .//assign SM_NLEVT_set_cardinality = cardinality SM_NLEVT_set
      .//print "SM_NLEVT ${SM_NLEVT_set_cardinality} ${class.Name}"
   .//SM_NLEVT ${SM_NLEVT_set_cardinality} ${class.Name}
      .//select many SM_PEVT_set related by class->...->SM_PEVT[Rxxx]
      .//assign SM_PEVT_set_cardinality = cardinality SM_PEVT_set
      .//print "SM_PEVT ${SM_PEVT_set_cardinality} ${class.Name}"
   .//SM_PEVT ${SM_PEVT_set_cardinality} ${class.Name}
      .select many SM_SEVT_set related by class->SM_ISM[R518]->SM_SM[R517]->SM_EVT[R502]->SM_SEVT[R525]
      .assign SM_SEVT_set_cardinality = cardinality SM_SEVT_set
      .print "SM_SEVT ${SM_SEVT_set_cardinality} ${class.Name}"
SM_SEVT ${SM_SEVT_set_cardinality} ${class.Name}
      .//select many SM_SGEVT_set related by class->...->SM_SGEVT[Rxxx]
      .//assign SM_SGEVT_set_cardinality = cardinality SM_SGEVT_set
      .//print "SM_SGEVT ${SM_SGEVT_set_cardinality} ${class.Name}"
   .//SM_SGEVT ${SM_SGEVT_set_cardinality} ${class.Name}
      .select many SM_SEME_set related by class->SM_ISM[R518]->SM_SM[R517]->SM_STATE[R501]->SM_SEME[R503]
      .assign SM_SEME_set_cardinality = cardinality SM_SEME_set
      .print "SM_SEME ${SM_SEME_set_cardinality} ${class.Name}"
SM_SEME ${SM_SEME_set_cardinality} ${class.Name}
      .select many SM_EVTDI_set related by class->SM_ISM[R518]->SM_SM[R517]->SM_EVT[R502]->SM_EVTDI[R532]
      .assign SM_EVTDI_set_cardinality = cardinality SM_EVTDI_set
      .print "SM_EVTDI ${SM_EVTDI_set_cardinality} ${class.Name}"
SM_EVTDI ${SM_EVTDI_set_cardinality} ${class.Name}
      .select many SM_EVT_set related by class->SM_ISM[R518]->SM_SM[R517]->SM_EVT[R502]
      .assign SM_EVT_set_cardinality = cardinality SM_EVT_set
      .print "SM_EVT ${SM_EVT_set_cardinality} ${class.Name}"
SM_EVT ${SM_EVT_set_cardinality} ${class.Name}
      .select many SM_STATE_set related by class->SM_ISM[R518]->SM_SM[R517]->SM_STATE[R501]
      .assign SM_STATE_set_cardinality = cardinality SM_STATE_set
      .print "SM_STATE ${SM_STATE_set_cardinality} ${class.Name}"
SM_STATE ${SM_STATE_set_cardinality} ${class.Name}
      .select many SM_SM_set related by class->SM_ISM[R518]->SM_SM[R517]
      .assign SM_SM_set_cardinality = cardinality SM_SM_set
      .print "SM_SM ${SM_SM_set_cardinality} ${class.Name}"
SM_SM ${SM_SM_set_cardinality} ${class.Name}
      .//select many SM_SDI_set related by class->...->SM_SDI[Rxxx]
      .//assign SM_SDI_set_cardinality = cardinality SM_SDI_set
      .//print "SM_SDI ${SM_SDI_set_cardinality} ${class.Name}"
   .//SM_SDI ${SM_SDI_set_cardinality} ${class.Name}
      .select many SM_TAH_set related by class->SM_ISM[R518]->SM_SM[R517]->SM_ACT[R515]->SM_AH[R514]->SM_TAH[R513]
      .assign SM_TAH_set_cardinality = cardinality SM_TAH_set
      .print "SM_TAH ${SM_TAH_set_cardinality} ${class.Name}"
SM_TAH ${SM_TAH_set_cardinality} ${class.Name}
      .select many SM_TXN_set related by class->SM_ISM[R518]->SM_SM[R517]->SM_TXN[R505]
      .assign SM_TXN_set_cardinality = cardinality SM_TXN_set
      .print "SM_TXN ${SM_TXN_set_cardinality} ${class.Name}"
SM_TXN ${SM_TXN_set_cardinality} ${class.Name}
      .//------------------------------------------------------------------------
      .//Subsystem
      .//select many O_REF_set related by class->...->O_REF[Rxxx]
      .//assign O_REF_set_cardinality = cardinality O_REF_set
      .//print "O_REF ${O_REF_set_cardinality} ${class.Name}"
   .//O_REF ${O_REF_set_cardinality} ${class.Name}
      .select many O_ATTR_set related by class->O_ATTR[R102]
      .assign O_ATTR_set_cardinality = cardinality O_ATTR_set
      .print "O_ATTR ${O_ATTR_set_cardinality} ${class.Name}"
O_ATTR ${O_ATTR_set_cardinality} ${class.Name}
      .select many O_BATTR_set related by class->O_ATTR[R102]->O_BATTR[R106]
      .assign O_BATTR_set_cardinality = cardinality O_BATTR_set
      .print "O_BATTR ${O_BATTR_set_cardinality} ${class.Name}"
O_BATTR ${O_BATTR_set_cardinality} ${class.Name}
      .//select many O_OIDA_set related by class->...->O_OIDA[Rxxx]
      .//assign O_OIDA_set_cardinality = cardinality O_OIDA_set
      .//print "O_OIDA ${O_OIDA_set_cardinality} ${class.Name}"
   .//O_OIDA ${O_OIDA_set_cardinality} ${class.Name}
      .select many O_ID_set related by class->O_ID[R104]
      .assign O_ID_set_cardinality = cardinality O_ID_set
      .print "O_ID ${O_ID_set_cardinality} ${class.Name}"
O_ID ${O_ID_set_cardinality} ${class.Name}
      .select many O_DBATTR_set related by class->O_ATTR[R102]->O_BATTR[R106]->O_DBATTR[R107]
      .assign O_DBATTR_set_cardinality = cardinality O_DBATTR_set
      .print "O_DBATTR ${O_DBATTR_set_cardinality} ${class.Name}"
O_DBATTR ${O_DBATTR_set_cardinality} ${class.Name}
      .//select many O_IOBJ_set related by class->...->O_IOBJ[Rxxx]
      .//assign O_IOBJ_set_cardinality = cardinality O_IOBJ_set
      .//print "O_IOBJ ${O_IOBJ_set_cardinality} ${class.Name}"
   .//O_IOBJ ${O_IOBJ_set_cardinality} ${class.Name}
      .select many O_OBJ_set from instances of O_OBJ where (selected.Name==class.Name)
      .assign O_OBJ_set_cardinality = cardinality O_OBJ_set
      .print "O_OBJ ${O_OBJ_set_cardinality} ${class.Name}"
O_OBJ ${O_OBJ_set_cardinality} ${class.Name}
      .select many O_NBATTR_set related by class->O_ATTR[R102]->O_BATTR[R106]->O_NBATTR[R107]
      .assign O_NBATTR_set_cardinality = cardinality O_NBATTR_set
      .print "O_NBATTR ${O_NBATTR_set_cardinality} ${class.Name}"
O_NBATTR ${O_NBATTR_set_cardinality} ${class.Name}
      .select many O_TPARM_set related by class->O_TFR[R115]->O_TPARM[R117]
      .assign O_TPARM_set_cardinality = cardinality O_TPARM_set
      .print "O_TPARM ${O_TPARM_set_cardinality} ${class.Name}"
O_TPARM ${O_TPARM_set_cardinality} ${class.Name}
      .select many O_TFR_set related by class->O_TFR[R115]
      .assign O_TFR_set_cardinality = cardinality O_TFR_set
      .print "O_TFR ${O_TFR_set_cardinality} ${class.Name}"
O_TFR ${O_TFR_set_cardinality} ${class.Name}
      .//select many O_RAVR_set related by class->...->O_RAVR[Rxxx]
      .//assign O_RAVR_set_cardinality = cardinality O_RAVR_set
      .//print "O_RAVR ${O_RAVR_set_cardinality} ${class.Name}"
   .//O_RAVR ${O_RAVR_set_cardinality} ${class.Name}
      .//select many O_RATTR_set related by class->...->O_RATTR[Rxxx]
      .//assign O_RATTR_set_cardinality = cardinality O_RATTR_set
      .//print "O_RATTR ${O_RATTR_set_cardinality} ${class.Name}"
   .//O_RATTR ${O_RATTR_set_cardinality} ${class.Name}
      .//select many O_RTIDA_set related by class->...->O_RTIDA[Rxxx]
      .//assign O_RTIDA_set_cardinality = cardinality O_RTIDA_set
      .//print "O_RTIDA ${O_RTIDA_set_cardinality} ${class.Name}"
   .//O_RTIDA ${O_RTIDA_set_cardinality} ${class.Name}
      .//------------------------------------------------------------------------
      .//System Level Datatypes
      .//select many SLD_SCINP_set related by class->...->SLD_SCINP[Rxxx]
      .//assign SLD_SCINP_set_cardinality = cardinality SLD_SCINP_set
      .//print "SLD_SCINP ${SLD_SCINP_set_cardinality} ${class.Name}"
   .//SLD_SCINP ${SLD_SCINP_set_cardinality} ${class.Name}
      .//select many SLD_SDP_set related by class->...->SLD_SDP[Rxxx]
      .//assign SLD_SDP_set_cardinality = cardinality SLD_SDP_set
      .//print "SLD_SDP ${SLD_SDP_set_cardinality} ${class.Name}"
   .//SLD_SDP ${SLD_SDP_set_cardinality} ${class.Name}
      .//select many SLD_SDINP_set related by class->...->SLD_SDINP[Rxxx]
      .//assign SLD_SDINP_set_cardinality = cardinality SLD_SDINP_set
      .//print "SLD_SDINP ${SLD_SDINP_set_cardinality} ${class.Name}"
   .//SLD_SDINP ${SLD_SDINP_set_cardinality} ${class.Name}
      .//------------------------------------------------------------------------
      .//Value
      .select many V_PAR_set related by ACT_ACT_set->ACT_BLK[R601]->V_VAL[R826]->V_PAR[R800]
      .assign V_PAR_set_cardinality = cardinality V_PAR_set
      .print "V_PAR ${V_PAR_set_cardinality} ${class.Name}"
V_PAR ${V_PAR_set_cardinality} ${class.Name}
      .select many V_AER_set related by ACT_ACT_set->ACT_BLK[R601]->V_VAL[R826]->V_AER[R801]
      .assign V_AER_set_cardinality = cardinality V_AER_set
      .print "V_AER ${V_AER_set_cardinality} ${class.Name}"
V_AER ${V_AER_set_cardinality} ${class.Name}
      .select many V_ALV_set related by ACT_ACT_set->ACT_BLK[R601]->V_VAL[R826]->V_ALV[R801]
      .assign V_ALV_set_cardinality = cardinality V_ALV_set
      .print "V_ALV ${V_ALV_set_cardinality} ${class.Name}"
V_ALV ${V_ALV_set_cardinality} ${class.Name}
      .select many V_AVL_set related by ACT_ACT_set->ACT_BLK[R601]->V_VAL[R826]->V_AVL[R801]
      .assign V_AVL_set_cardinality = cardinality V_AVL_set
      .print "V_AVL ${V_AVL_set_cardinality} ${class.Name}"
V_AVL ${V_AVL_set_cardinality} ${class.Name}
      .select many V_BIN_set related by ACT_ACT_set->ACT_BLK[R601]->V_VAL[R826]->V_BIN[R801]
      .assign V_BIN_set_cardinality = cardinality V_BIN_set
      .print "V_BIN ${V_BIN_set_cardinality} ${class.Name}"
V_BIN ${V_BIN_set_cardinality} ${class.Name}
      .select many V_BRV_set related by ACT_ACT_set->ACT_BLK[R601]->V_VAL[R826]->V_BRV[R801]
      .assign V_BRV_set_cardinality = cardinality V_BRV_set
      .print "V_BRV ${V_BRV_set_cardinality} ${class.Name}"
V_BRV ${V_BRV_set_cardinality} ${class.Name}
      .select many V_EDV_set related by ACT_ACT_set->ACT_BLK[R601]->V_VAL[R826]->V_EDV[R801]
      .assign V_EDV_set_cardinality = cardinality V_EDV_set
      .print "V_EDV ${V_EDV_set_cardinality} ${class.Name}"
V_EDV ${V_EDV_set_cardinality} ${class.Name}
      .//select many V_EPR_set related by class->...->V_EPR[R801]
      .//assign V_EPR_set_cardinality = cardinality V_EPR_set
      .//print "V_EPR ${V_EPR_set_cardinality} ${class.Name}"
   .//V_EPR ${V_EPR_set_cardinality} ${class.Name}
      .//select many V_FNV_set related by ACT_ACT_set->ACT_BLK[R601]->V_VAL[R826]->V_FNV[R801]
      .//assign V_FNV_set_cardinality = cardinality V_FNV_set
      .//print "V_FNV ${V_FNV_set_cardinality} ${class.Name}"
   .//V_FNV ${V_FNV_set_cardinality} ${class.Name}
      .select many V_INT_set related by ACT_ACT_set->ACT_BLK[R601]->V_VAR[R823]->V_INT[R814]
      .assign V_INT_set_cardinality = cardinality V_INT_set
      .print "V_INT ${V_INT_set_cardinality} ${class.Name}"
V_INT ${V_INT_set_cardinality} ${class.Name}
      .select many V_IRF_set related by ACT_ACT_set->ACT_BLK[R601]->V_VAL[R826]->V_IRF[R801]
      .assign V_IRF_set_cardinality = cardinality V_IRF_set
      .print "V_IRF ${V_IRF_set_cardinality} ${class.Name}"
V_IRF ${V_IRF_set_cardinality} ${class.Name}
      .select many V_ISR_set related by ACT_ACT_set->ACT_BLK[R601]->V_VAL[R826]->V_ISR[R801]
      .assign V_ISR_set_cardinality = cardinality V_ISR_set
      .print "V_ISR ${V_ISR_set_cardinality} ${class.Name}"
V_ISR ${V_ISR_set_cardinality} ${class.Name}
      .select many V_INS_set related by ACT_ACT_set->ACT_BLK[R601]->V_VAR[R823]->V_INS[R814]
      .assign V_INS_set_cardinality = cardinality V_INS_set
      .print "V_INS ${V_INS_set_cardinality} ${class.Name}"
V_INS ${V_INS_set_cardinality} ${class.Name}
      .select many V_LBO_set related by ACT_ACT_set->ACT_BLK[R601]->V_VAL[R826]->V_LBO[R801]
      .assign V_LBO_set_cardinality = cardinality V_LBO_set
      .print "V_LBO ${V_LBO_set_cardinality} ${class.Name}"
V_LBO ${V_LBO_set_cardinality} ${class.Name}
      .select many V_LEN_set related by ACT_ACT_set->ACT_BLK[R601]->V_VAL[R826]->V_LEN[R801]
      .assign V_LEN_set_cardinality = cardinality V_LEN_set
      .print "V_LEN ${V_LEN_set_cardinality} ${class.Name}"
V_LEN ${V_LEN_set_cardinality} ${class.Name}
      .select many V_LIN_set related by ACT_ACT_set->ACT_BLK[R601]->V_VAL[R826]->V_LIN[R801]
      .assign V_LIN_set_cardinality = cardinality V_LIN_set
      .print "V_LIN ${V_LIN_set_cardinality} ${class.Name}"
V_LIN ${V_LIN_set_cardinality} ${class.Name}
      .select many V_LRL_set related by ACT_ACT_set->ACT_BLK[R601]->V_VAL[R826]->V_LRL[R801]
      .assign V_LRL_set_cardinality = cardinality V_LRL_set
      .print "V_LRL ${V_LRL_set_cardinality} ${class.Name}"
V_LRL ${V_LRL_set_cardinality} ${class.Name}
      .select many V_LST_set related by ACT_ACT_set->ACT_BLK[R601]->V_VAL[R826]->V_LST[R801]
      .assign V_LST_set_cardinality = cardinality V_LST_set
      .print "V_LST ${V_LST_set_cardinality} ${class.Name}"
V_LST ${V_LST_set_cardinality} ${class.Name}
      .select many V_MVL_set related by ACT_ACT_set->ACT_BLK[R601]->V_VAL[R826]->V_MVL[R801]
      .assign V_MVL_set_cardinality = cardinality V_MVL_set
      .print "V_MVL ${V_MVL_set_cardinality} ${class.Name}"
V_MVL ${V_MVL_set_cardinality} ${class.Name}
      .select many V_MSV_set related by ACT_ACT_set->ACT_BLK[R601]->V_VAL[R826]->V_MSV[R801]
      .assign V_MSV_set_cardinality = cardinality V_MSV_set
      .print "V_MSV ${V_MSV_set_cardinality} ${class.Name}"
V_MSV ${V_MSV_set_cardinality} ${class.Name}
      .select many V_TRV_set related by ACT_ACT_set->ACT_BLK[R601]->V_VAL[R826]->V_TRV[R801]
      .assign V_TRV_set_cardinality = cardinality V_TRV_set
      .print "V_TRV ${V_TRV_set_cardinality} ${class.Name}"
V_TRV ${V_TRV_set_cardinality} ${class.Name}
      .select many V_PVL_set related by ACT_ACT_set->ACT_BLK[R601]->V_VAL[R826]->V_PVL[R801]
      .assign V_PVL_set_cardinality = cardinality V_PVL_set
      .print "V_PVL ${V_PVL_set_cardinality} ${class.Name}"
V_PVL ${V_PVL_set_cardinality} ${class.Name}
      .//select many V_SLR_set related by ACT_ACT_set->ACT_BLK[R601]->V_VAL[R826]->V_SLR[R801]
      .//assign V_SLR_set_cardinality = cardinality V_SLR_set
      .//print "V_SLR ${V_SLR_set_cardinality} ${class.Name}"
   .//V_SLR ${V_SLR_set_cardinality} ${class.Name}
      .select many V_SCV_set related by ACT_ACT_set->ACT_BLK[R601]->V_VAL[R826]->V_SCV[R801]
      .assign V_SCV_set_cardinality = cardinality V_SCV_set
      .print "V_SCV ${V_SCV_set_cardinality} ${class.Name}"
V_SCV ${V_SCV_set_cardinality} ${class.Name}
      .select many V_TVL_set related by ACT_ACT_set->ACT_BLK[R601]->V_VAL[R826]->V_TVL[R801]
      .assign V_TVL_set_cardinality = cardinality V_TVL_set
      .print "V_TVL ${V_TVL_set_cardinality} ${class.Name}"
V_TVL ${V_TVL_set_cardinality} ${class.Name}
      .select many V_TRN_set related by ACT_ACT_set->ACT_BLK[R601]->V_VAR[R823]->V_TRN[R814]
      .assign V_TRN_set_cardinality = cardinality V_TRN_set
      .print "V_TRN ${V_TRN_set_cardinality} ${class.Name}"
V_TRN ${V_TRN_set_cardinality} ${class.Name}
      .select many V_UNY_set related by ACT_ACT_set->ACT_BLK[R601]->V_VAL[R826]->V_UNY[R801]
      .assign V_UNY_set_cardinality = cardinality V_UNY_set
      .print "V_UNY ${V_UNY_set_cardinality} ${class.Name}"
V_UNY ${V_UNY_set_cardinality} ${class.Name}
      .select many V_VAL_set related by ACT_ACT_set->ACT_BLK[R601]->V_VAL[R826]
      .assign V_VAL_set_cardinality = cardinality V_VAL_set
      .print "V_VAL ${V_VAL_set_cardinality} ${class.Name}"
V_VAL ${V_VAL_set_cardinality} ${class.Name}
      .//select many V_LOC_set related by ACT_ACT_set->ACT_BLK[R601]->V_VAR[R823]->V_LOC[R835]
      .//assign V_LOC_set_cardinality = cardinality V_LOC_set
      .//print "V_LOC ${V_LOC_set_cardinality} ${class.Name}"
   .//V_LOC ${V_LOC_set_cardinality} ${class.Name}
      .select many V_VAR_set related by ACT_ACT_set->ACT_BLK[R601]->V_VAR[R823]
      .assign V_VAR_set_cardinality = cardinality V_VAR_set
      .print "V_VAR ${V_VAR_set_cardinality} ${class.Name}"
V_VAR ${V_VAR_set_cardinality} ${class.Name}
      .//------------------------------------------------------------------------
      .//Wiring
      .//select many S_AW_set related by class->...->S_AW[Rxxx]
      .print S_AW_set_cardinality = cardinality S_AW_set
      .//print "S_AW ${S_AW_set_cardinality} ${class.Name}"
   .//S_AW ${S_AW_set_cardinality} ${class.Name}
   .end for
   .//------------------------------------------------------------------------
   .print " "
   .select any sys from instances of S_SYS
   .print "Sys.ID = ${sys.Sys_ID}"
   .emit to file "C:/Temp/${sys.Name}_MetaclassCardinalityHorizontally.csv"
