${indent}.select many ${cls.Key_Lett}_inst_set from instances of ${cls.Key_Lett}
${indent}.assign  ${cls.Key_Lett}_num = cardinality ${cls.Key_Lett}_inst_set
${indent}.print "Metaclass ${cls.Name} (${cls.Key_Lett}) has $${${cls.Key_Lett}_num} instances."
${indent}.if(not (${cls.Key_Lett}_num==0))
${cls.Key_Lett} $${${cls.Key_Lett}_num}
${indent}.end if