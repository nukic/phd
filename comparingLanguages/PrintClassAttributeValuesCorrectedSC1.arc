.select many S_SYNC_instances from instances of S_SYNC
.for each S_SYNC_inst in S_SYNC_instances
"${S_SYNC_inst.Name}"
"${S_SYNC_inst.Descrip}"
"${S_SYNC_inst.Action_Semantics}"
.//"${S_SYNC_inst.Action_Semantics_internal}"
"${S_SYNC_inst.Return_Dimensions}"
.end for
.//
.select many PE_CRS_instances from instances of PE_CRS
.for each PE_CRS_inst in PE_CRS_instances
"${PE_CRS_inst.Name}"
.end for
.//
.select many SM_ACT_instances from instances of SM_ACT
.for each SM_ACT_inst in SM_ACT_instances
"${SM_ACT_inst.Action_Semantics}"
"${SM_ACT_inst.Descrip}"
.//"${SM_ACT_inst.Action_Semantics_internal}"
.end for
.//
.select many O_REF_instances from instances of O_REF
.for each O_REF_inst in O_REF_instances
"${O_REF_inst.Descrip}"
"${O_REF_inst.RObj_Name}"
"${O_REF_inst.RAttr_Name}"
"${O_REF_inst.Rel_Name}"
.end for
.//
.select many ACT_FIO_instances from instances of ACT_FIO
.for each ACT_FIO_inst in ACT_FIO_instances
"${ACT_FIO_inst.cardinality}"
.end for
.//
.select many S_DOM_instances from instances of S_DOM
.for each S_DOM_inst in S_DOM_instances
"${S_DOM_inst.Name}"
"${S_DOM_inst.Descrip}"
.end for
.//
.select many R_PART_instances from instances of R_PART
.for each R_PART_inst in R_PART_instances
"${R_PART_inst.Txt_Phrs}"
.end for
.//
.select many S_ENUM_instances from instances of S_ENUM
.for each S_ENUM_inst in S_ENUM_instances
"${S_ENUM_inst.Name}"
"${S_ENUM_inst.Descrip}"
.end for
.//
.select many IP_IP_instances from instances of IP_IP
.for each IP_IP_inst in IP_IP_instances
"${IP_IP_inst.Name}"
"${IP_IP_inst.Descrip}"
.end for
.//
.select many V_BIN_instances from instances of V_BIN
.for each V_BIN_inst in V_BIN_instances
"${V_BIN_inst.Operator}"
.end for
.//
.select many SPR_RO_instances from instances of SPR_RO
.for each SPR_RO_inst in SPR_RO_instances
"${SPR_RO_inst.Name}"
"${SPR_RO_inst.Descrip}"
"${SPR_RO_inst.Action_Semantics}"
"${SPR_RO_inst.Action_Semantics_internal}"
.end for
.//
.select many O_RATTR_instances from instances of O_RATTR
.for each O_RATTR_inst in O_RATTR_instances
"${O_RATTR_inst.BaseAttrName}"
.end for
.//
.select many S_EEEVT_instances from instances of S_EEEVT
.for each S_EEEVT_inst in S_EEEVT_instances
"${S_EEEVT_inst.Mning}"
"${S_EEEVT_inst.Unq_Lbl}"
"${S_EEEVT_inst.Drv_Lbl}"
"${S_EEEVT_inst.Descrip}"
.end for
.//
.select many ACT_UNR_instances from instances of ACT_UNR
.for each ACT_UNR_inst in ACT_UNR_instances
"${ACT_UNR_inst.relationship_phrase}"
.end for
.//
.select many R_COMP_instances from instances of R_COMP
.for each R_COMP_inst in R_COMP_instances
"${R_COMP_inst.Rel_Chn}"
.end for
.//
.select many SPR_RS_instances from instances of SPR_RS
.for each SPR_RS_inst in SPR_RS_instances
"${SPR_RS_inst.Name}"
"${SPR_RS_inst.Descrip}"
"${SPR_RS_inst.Action_Semantics}"
.//"${SPR_RS_inst.Action_Semantics_internal}"
.end for
.//
.select many ACT_URU_instances from instances of ACT_URU
.for each ACT_URU_inst in ACT_URU_instances
"${ACT_URU_inst.relationship_phrase}"
.end for
.//
.select many V_UNY_instances from instances of V_UNY
.for each V_UNY_inst in V_UNY_instances
"${V_UNY_inst.Operator}"
.end for
.//
.select many R_REL_instances from instances of R_REL
.for each R_REL_inst in R_REL_instances
"${R_REL_inst.Descrip}"
.end for
.//
.select many R_AOTH_instances from instances of R_AOTH
.for each R_AOTH_inst in R_AOTH_instances
"${R_AOTH_inst.Txt_Phrs}"
.end for
.//
.select many ACT_LNK_instances from instances of ACT_LNK
.for each ACT_LNK_inst in ACT_LNK_instances
"${ACT_LNK_inst.Rel_Phrase}"
.end for
.//
.select many CP_CP_instances from instances of CP_CP
.for each CP_CP_inst in CP_CP_instances
"${CP_CP_inst.Name}"
"${CP_CP_inst.Descrip}"
.end for
.//
.select many S_EE_instances from instances of S_EE
.for each S_EE_inst in S_EE_instances
"${S_EE_inst.Name}"
"${S_EE_inst.Descrip}"
"${S_EE_inst.Key_Lett}"
.end for
.//
.select many S_SPARM_instances from instances of S_SPARM
.for each S_SPARM_inst in S_SPARM_instances
"${S_SPARM_inst.Name}"
"${S_SPARM_inst.Dimensions}"
"${S_SPARM_inst.Descrip}"
.end for
.//
.select many V_LST_instances from instances of V_LST
.for each V_LST_inst in V_LST_instances
"${V_LST_inst.Value}"
.end for
.//
.select many CNST_CSP_instances from instances of CNST_CSP
.for each CNST_CSP_inst in CNST_CSP_instances
"${CNST_CSP_inst.InformalGroupName}"
"${CNST_CSP_inst.Descrip}"
.end for
.//
.select many ACT_ACT_instances from instances of ACT_ACT
.for each ACT_ACT_inst in ACT_ACT_instances
"${ACT_ACT_inst.Type}"
"${ACT_ACT_inst.Label}"
.end for
.//
.select many C_AS_instances from instances of C_AS
.for each C_AS_inst in C_AS_instances
"${C_AS_inst.Name}"
"${C_AS_inst.Descrip}"
.end for
.//
.select many SM_EVTDI_instances from instances of SM_EVTDI
.for each SM_EVTDI_inst in SM_EVTDI_instances
"${SM_EVTDI_inst.Name}"
"${SM_EVTDI_inst.Descrip}"
"${SM_EVTDI_inst.Dimensions}"
.end for
.//
.select many ACT_FIW_instances from instances of ACT_FIW
.for each ACT_FIW_inst in ACT_FIW_instances
"${ACT_FIW_inst.cardinality}"
.end for
.//
.select many SM_EIGN_instances from instances of SM_EIGN
.for each SM_EIGN_inst in SM_EIGN_instances
"${SM_EIGN_inst.Descrip}"
.end for
.//
.select many O_TFR_instances from instances of O_TFR
.for each O_TFR_inst in O_TFR_instances
"${O_TFR_inst.Name}"
"${O_TFR_inst.Descrip}"
"${O_TFR_inst.Action_Semantics}"
.//"${O_TFR_inst.Action_Semantics_internal}"
"${O_TFR_inst.Return_Dimensions}"
.end for
.//
.select many S_BRG_instances from instances of S_BRG
.for each S_BRG_inst in S_BRG_instances
"${S_BRG_inst.Name}"
"${S_BRG_inst.Descrip}"
"${S_BRG_inst.Action_Semantics}"
.//"${S_BRG_inst.Action_Semantics_internal}"
"${S_BRG_inst.Return_Dimensions}"
.end for
.//
.select many R_CONE_instances from instances of R_CONE
.for each R_CONE_inst in R_CONE_instances
"${R_CONE_inst.Txt_Phrs}"
.end for
.//
.select many C_EP_instances from instances of C_EP
.for each C_EP_inst in C_EP_instances
"${C_EP_inst.Name}"
"${C_EP_inst.Descrip}"
.end for
.//
.select many ACT_REL_instances from instances of ACT_REL
.for each ACT_REL_inst in ACT_REL_instances
"${ACT_REL_inst.relationship_phrase}"
.end for
.//
.select many C_IO_instances from instances of C_IO
.for each C_IO_inst in C_IO_instances
"${C_IO_inst.Name}"
"${C_IO_inst.Descrip}"
"${C_IO_inst.Return_Dimensions}"
.end for
.//
.select many O_TPARM_instances from instances of O_TPARM
.for each O_TPARM_inst in O_TPARM_instances
"${O_TPARM_inst.Name}"
"${O_TPARM_inst.Dimensions}"
"${O_TPARM_inst.Descrip}"
.end for
.//
.select many C_C_instances from instances of C_C
.for each C_C_inst in C_C_instances
"${C_C_inst.Name}"
"${C_C_inst.Descrip}"
.//"${C_C_inst.Label}"
"${C_C_inst.Realized_Class_Path}"
.end for
.//
.select many O_ATTR_instances from instances of O_ATTR
.for each O_ATTR_inst in O_ATTR_instances
"${O_ATTR_inst.Name}"
"${O_ATTR_inst.Descrip}"
"${O_ATTR_inst.Prefix}"
"${O_ATTR_inst.Root_Nam}"
"${O_ATTR_inst.Dimensions}"
"${O_ATTR_inst.DefaultValue}"
.end for
.//
.select many EP_PKG_instances from instances of EP_PKG
.for each EP_PKG_inst in EP_PKG_instances
"${EP_PKG_inst.Name}"
"${EP_PKG_inst.Descrip}"
.end for
.//
.select many S_BPARM_instances from instances of S_BPARM
.for each S_BPARM_inst in S_BPARM_instances
"${S_BPARM_inst.Name}"
"${S_BPARM_inst.Dimensions}"
"${S_BPARM_inst.Descrip}"
.end for
.//
.select many C_I_instances from instances of C_I
.for each C_I_inst in C_I_instances
"${C_I_inst.Name}"
"${C_I_inst.Descrip}"
.end for
.//
.select many S_MBR_instances from instances of S_MBR
.for each S_MBR_inst in S_MBR_instances
"${S_MBR_inst.Name}"
"${S_MBR_inst.Descrip}"
"${S_MBR_inst.Dimensions}"
.end for
.//
.select many V_TRN_instances from instances of V_TRN
.for each V_TRN_inst in V_TRN_instances
"${V_TRN_inst.Dimensions}"
.end for
.//
.select many C_SF_instances from instances of C_SF
.for each C_SF_inst in C_SF_instances
"${C_SF_inst.Descrip}"
"${C_SF_inst.Label}"
.end for
.//
.select many S_EEEDI_instances from instances of S_EEEDI
.for each S_EEEDI_inst in S_EEEDI_instances
"${S_EEEDI_inst.Name}"
"${S_EEEDI_inst.Descrip}"
.end for
.//
.select many SM_PEVT_instances from instances of SM_PEVT
.for each SM_PEVT_inst in SM_PEVT_instances
"${SM_PEVT_inst.localClassName}"
"${SM_PEVT_inst.localClassKL}"
"${SM_PEVT_inst.localEventMning}"
.end for
.//
.select many C_P_instances from instances of C_P
.for each C_P_inst in C_P_instances
"${C_P_inst.Name}"
"${C_P_inst.Descrip}"
"${C_P_inst.InformalName}"
"${C_P_inst.pathFromComponent}"
.end for
.//
.select many SM_EVT_instances from instances of SM_EVT
.for each SM_EVT_inst in SM_EVT_instances
"${SM_EVT_inst.Mning}"
"${SM_EVT_inst.Unq_Lbl}"
"${SM_EVT_inst.Drv_Lbl}"
"${SM_EVT_inst.Descrip}"
.end for
.//
.select many C_R_instances from instances of C_R
.for each C_R_inst in C_R_instances
"${C_R_inst.Name}"
"${C_R_inst.Descrip}"
"${C_R_inst.InformalName}"
"${C_R_inst.reversePathFromComponent}"
.end for
.//
.select many R_FORM_instances from instances of R_FORM
.for each R_FORM_inst in R_FORM_instances
"${R_FORM_inst.Txt_Phrs}"
.end for
.//
.select many V_LBO_instances from instances of V_LBO
.for each V_LBO_inst in V_LBO_instances
"${V_LBO_inst.Value}"
.end for
.//
.select many SPR_PO_instances from instances of SPR_PO
.for each SPR_PO_inst in SPR_PO_instances
"${SPR_PO_inst.Name}"
"${SPR_PO_inst.Descrip}"
"${SPR_PO_inst.Action_Semantics}"
"${SPR_PO_inst.Action_Semantics_internal}"
.end for
.//
.select many SM_SGEVT_instances from instances of SM_SGEVT
.for each SM_SGEVT_inst in SM_SGEVT_instances
"${SM_SGEVT_inst.signal_name}"
.end for
.//
.select many SM_STATE_instances from instances of SM_STATE
.for each SM_STATE_inst in SM_STATE_instances
"${SM_STATE_inst.Name}"
.end for
.//
.select many S_DT_instances from instances of S_DT
.for each S_DT_inst in S_DT_instances
"${S_DT_inst.Name}"
"${S_DT_inst.Descrip}"
"${S_DT_inst.DefaultValue}"
.end for
.//
.select many SM_CH_instances from instances of SM_CH
.for each SM_CH_inst in SM_CH_instances
"${SM_CH_inst.Descrip}"
.end for
.//
.select many SPR_PS_instances from instances of SPR_PS
.for each SPR_PS_inst in SPR_PS_instances
"${SPR_PS_inst.Name}"
"${SPR_PS_inst.Descrip}"
"${SPR_PS_inst.Action_Semantics}"
.//"${SPR_PS_inst.Action_Semantics_internal}"
.end for
.//
.select many C_DG_instances from instances of C_DG
.for each C_DG_inst in C_DG_instances
"${C_DG_inst.Name}"
.end for
.//
.select many V_LIN_instances from instances of V_LIN
.for each V_LIN_inst in V_LIN_instances
"${V_LIN_inst.Value}"
.end for
.//
.select many ACT_BLK_instances from instances of ACT_BLK
.for each ACT_BLK_inst in ACT_BLK_instances
"${ACT_BLK_inst.TempBuffer}"
"${ACT_BLK_inst.SupData1}"
"${ACT_BLK_inst.SupData2}"
.end for
.//
.select many O_OBJ_instances from instances of O_OBJ
.for each O_OBJ_inst in O_OBJ_instances
"${O_OBJ_inst.Name}"
"${O_OBJ_inst.Key_Lett}"
"${O_OBJ_inst.Descrip}"
.end for
.//
.select many C_PP_instances from instances of C_PP
.for each C_PP_inst in C_PP_instances
"${C_PP_inst.Name}"
"${C_PP_inst.Descrip}"
"${C_PP_inst.Dimensions}"
.end for
.//
.select many C_PO_instances from instances of C_PO
.for each C_PO_inst in C_PO_instances
"${C_PO_inst.Name}"
.end for
.//
.select many CL_IC_instances from instances of CL_IC
.for each CL_IC_inst in CL_IC_instances
"${CL_IC_inst.ClassifierName}"
"${CL_IC_inst.Name}"
"${CL_IC_inst.Descrip}"
.end for
.//
.select many S_FPK_instances from instances of S_FPK
.for each S_FPK_inst in S_FPK_instances
"${S_FPK_inst.Name}"
.end for
.//
.select many O_DBATTR_instances from instances of O_DBATTR
.for each O_DBATTR_inst in O_DBATTR_instances
"${O_DBATTR_inst.Action_Semantics}"
.//"${O_DBATTR_inst.Action_Semantics_internal}"
.end for
.//
.select many CL_IP_instances from instances of CL_IP
.for each CL_IP_inst in CL_IP_instances
"${CL_IP_inst.Name}"
"${CL_IP_inst.Descrip}"
.end for
.//
.select many CNST_SYC_instances from instances of CNST_SYC
.for each CNST_SYC_inst in CNST_SYC_instances
"${CNST_SYC_inst.Name}"
"${CNST_SYC_inst.Descrip}"
.end for
.//
.select many SM_SM_instances from instances of SM_SM
.for each SM_SM_inst in SM_SM_instances
"${SM_SM_inst.Descrip}"
.end for
.//
.select many O_OIDA_instances from instances of O_OIDA
.for each O_OIDA_inst in O_OIDA_instances
"${O_OIDA_inst.localAttributeName}"
.end for
.//
.select many R_COTH_instances from instances of R_COTH
.for each R_COTH_inst in R_COTH_instances
"${R_COTH_inst.Txt_Phrs}"
.end for
.//
.select many CNST_LSC_instances from instances of CNST_LSC
.for each CNST_LSC_inst in CNST_LSC_instances
"${CNST_LSC_inst.Value}"
.end for
.//
.select many CL_IR_instances from instances of CL_IR
.for each CL_IR_inst in CL_IR_instances
"${CL_IR_inst.Name}"
"${CL_IR_inst.Descrip}"
.end for
.//
.select many S_SS_instances from instances of S_SS
.for each S_SS_inst in S_SS_instances
"${S_SS_inst.Name}"
"${S_SS_inst.Descrip}"
"${S_SS_inst.Prefix}"
.end for
.//
.select many S_EEDI_instances from instances of S_EEDI
.for each S_EEDI_inst in S_EEDI_instances
"${S_EEDI_inst.Name}"
"${S_EEDI_inst.Descrip}"
.end for
.//
.select many SM_NLEVT_instances from instances of SM_NLEVT
.for each SM_NLEVT_inst in SM_NLEVT_instances
"${SM_NLEVT_inst.Local_Meaning}"
.//"${SM_NLEVT_inst.Name}"
.end for
.//
.select many S_DPK_instances from instances of S_DPK
.for each S_DPK_inst in S_DPK_instances
"${S_DPK_inst.Name}"
.end for
.//
.select many V_PAR_instances from instances of V_PAR
.for each V_PAR_inst in V_PAR_instances
"${V_PAR_inst.Name}"
.end for
.//
.//select many V_VAL_instances from instances of V_VAL
.//for each V_VAL_inst in V_VAL_instances
.//"${V_VAL_inst.Text}"
.//end for
.//
.select many ACT_RU_instances from instances of ACT_RU
.for each ACT_RU_inst in ACT_RU_instances
"${ACT_RU_inst.relationship_phrase}"
.end for
.//
.select many CL_IIR_instances from instances of CL_IIR
.for each CL_IIR_inst in CL_IIR_instances
"${CL_IIR_inst.Name}"
"${CL_IIR_inst.Descrip}"
.end for
.//
.select many R_AONE_instances from instances of R_AONE
.for each R_AONE_inst in R_AONE_instances
"${R_AONE_inst.Txt_Phrs}"
.end for
.//
.select many O_IOBJ_instances from instances of O_IOBJ
.for each O_IOBJ_inst in O_IOBJ_instances
"${O_IOBJ_inst.Obj_Name}"
"${O_IOBJ_inst.Obj_KL}"
.end for
.//
.select many V_LRL_instances from instances of V_LRL
.for each V_LRL_inst in V_LRL_instances
"${V_LRL_inst.Value}"
.end for
.//
.select many PE_SRS_instances from instances of PE_SRS
.for each PE_SRS_inst in PE_SRS_instances
"${PE_SRS_inst.Name}"
.end for
.//
.select many V_VAR_instances from instances of V_VAR
.for each V_VAR_inst in V_VAR_instances
"${V_VAR_inst.Name}"
.end for
.//
.select many ACT_SMT_instances from instances of ACT_SMT
.for each ACT_SMT_inst in ACT_SMT_instances
"${ACT_SMT_inst.Label}"
.end for
.//
.select many ACT_SEL_instances from instances of ACT_SEL
.for each ACT_SEL_inst in ACT_SEL_instances
"${ACT_SEL_inst.cardinality}"
.end for
.//
.select many S_EEPK_instances from instances of S_EEPK
.for each S_EEPK_inst in S_EEPK_instances
"${S_EEPK_inst.Name}"
.end for
.//
.select many S_SYS_instances from instances of S_SYS
.for each S_SYS_inst in S_SYS_instances
"${S_SYS_inst.Name}"
.end for
.//
.emit to file "C:/Temp/ClassAttributeValuesSC1.txt"
.//