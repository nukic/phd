.invoke pl = GET_ENV_VAR("PROJECT_LOC")
.if(pl.success)
	.print "======================================================================================"
	.assign project_location = pl.result	
	.print "Using project location ${project_location}."
	.print "======================================================================================"
	.//--------------------------------------------------------------------------------
	.//INCLUSIONS
	.include "${project_location}/utils/q.general.utils.arc"
	.include "${project_location}/utils/q.cc.horizontal.arc"
	.include "${project_location}/utils/q.cc.vertical.arc"
	.include "${project_location}/utils/q.loc.horizontal.arc"
	.include "${project_location}/utils/q.printing.arc"
	.print "Starting..."
	.select any root_pkg from instances of EP_PKG where selected.name=="ooaofooa"
	.if(not_empty root_pkg)
		.invoke acs = find_classes_recursively(root_pkg)
		.assign all_cls_set = acs.result
		.for each cls in all_cls_set
			.select many attr_set related by cls->O_ATTR[R102]
			.for each attr in attr_set
				.select one dt related by attr->S_DT[R114]
				.if(dt.Name=="string")
					.print "Found string attribute: class = ${cls.Name}, attribute = ${attr.Name}"
					.assign Lines = "${cls.Key_Lett} ${attr.Name}"
					.include "${project_location}/utils/t.part.general"
				.else
					.print "Skipping non-string attribute: class = ${cls.Name}, attribute = ${attr.Name}"
				.end if
			.end for
		.end for
		.emit to file "C:/Temp/StringAttributes.csv"
	.else
		.print "ERROR: Could not find root package!"
	.end if
	.print "DONE!"
.else
	.print "FAILED TO READ ENV VAR: PROJECT_LOC"
.end if


