package generateRslSelections;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

public class Main {

	public static void main(String[] args) {
		File file = new File("C:/Temp/StringAttributes.csv");
		FileInputStream fis = null;
		String line;

		Map<String, MMClass> name2mmClassMap = new HashMap<String, MMClass>();

		try {
			fis = new FileInputStream(file);
			InputStreamReader isr = new InputStreamReader(fis, Charset.forName("UTF-8"));
			BufferedReader br = new BufferedReader(isr);

			while ((line = br.readLine()) != null) {
				System.out.println("Read line: " + line);
				String[] tokens = line.split(" ");
				if (tokens.length == 2) {
					MMClass alreadyAddedClass = name2mmClassMap.get(tokens[0]);
					if (alreadyAddedClass == null) {
						System.out.println(
								"NEW CLASS: Adding new class " + tokens[0] + " and its attribute " + tokens[1]);
						name2mmClassMap.put(tokens[0], new MMClass(tokens[0], tokens[1]));
					} else {
						System.out.println("EXISTING CLASS: Adding attribute " + tokens[1] + " (to existing class "
								+ tokens[0] + ")");
						alreadyAddedClass.addMmAttribute(tokens[1]);
					}
				} else {
					System.err.println("ERROR: unexpected number of tokens:");
				}
			}
			br.close();

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (fis != null)
					fis.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		
		//-----------------------------------------------------------------------------------
		try {
			FileWriter fw = new FileWriter("C:/Temp/PrintClassAttributeValues.arc", true);
			for (MMClass cls : name2mmClassMap.values()) {
				fw.write(".select many " + cls.name + "_instances from instances of " + cls.name + "\n");
				fw.write(".for each " + cls.name + "_inst in " + cls.name + "_instances\n");
				for (String attr : cls.attributes) {
					// fw.write(" .print \"${"+cls.name +"_inst."+attr+"}\"\n");
					fw.write("\"${" + cls.name + "_inst." + attr + "}\"\n");
				}
				fw.write(".end for\n.//\n");

			}
			fw.write(".emit to file \"C:/Temp/ClassAttributeValues.txt\"\n.//\n");
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
