package generateRslSelections;
import java.util.ArrayList;
import java.util.List;

public class MMClass {
	public String name;
	
	public MMClass(String mmClassName, String firstAttr) {
		this.name = mmClassName;
		attributes.add(firstAttr);
	}
	
	public void addMmAttribute(String attr){
		attributes.add(attr);
	}

	public List<String> attributes = new ArrayList<String>();

}
