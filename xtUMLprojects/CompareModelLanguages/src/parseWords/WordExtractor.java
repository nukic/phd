package parseWords;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.TreeSet;

public class WordExtractor {

	public static TreeSet<String> DICTIONARY1 = new TreeSet<String>();
	public static TreeSet<String> DICTIONARY2 = new TreeSet<String>();
	public static TreeSet<String> DICTIONARY3 = new TreeSet<String>();
	public static TreeSet<String> DICTIONARY4 = new TreeSet<String>();
	public static TreeSet<String> DICTIONARY = null;

	public static int firstDictionarySize = 0;
	public static int secondDictionarySize = 0;
	public static int thirdDictionarySize = 0;
	public static int domainDictionarySize = 0;

	public static void main(String[] args) {

		File file1 = new File("res\\ClassAttributeValuesSC1.txt");
		DICTIONARY = DICTIONARY1;
		fillTheDictionaryFromInputFile(file1);
		firstDictionarySize = DICTIONARY1.size();

		File file2 = new File("res\\ClassAttributeValuesSC2.txt");
		DICTIONARY = DICTIONARY2;
		fillTheDictionaryFromInputFile(file2);
		secondDictionarySize = DICTIONARY2.size();

		File file3 = new File("res\\ClassAttributeValuesSC3.txt");
		DICTIONARY = DICTIONARY3;
		fillTheDictionaryFromInputFile(file3);
		thirdDictionarySize = DICTIONARY3.size();

		File file4 = new File("res\\Domain.txt");
		DICTIONARY = DICTIONARY4;
		fillTheDictionaryFromInputFile(file4);
		domainDictionarySize = DICTIONARY4.size();

		printDictionary(DICTIONARY1);
		printDictionary(DICTIONARY2);
		printDictionary(DICTIONARY3);
		printDictionary(DICTIONARY4);

		System.out.println("\n\n****************************************************");
		System.out.println("Comparing dictionaries: REFERENT = 1, COMPARED = 2");
		compareDictionaries(DICTIONARY1, DICTIONARY2);

		System.out.println("\n\n****************************************************");
		System.out.println("Comparing dictionaries: REFERENT = 2, COMPARED = 3");
		compareDictionaries(DICTIONARY2, DICTIONARY3);

		System.out.println("\n\n****************************************************");
		System.out.println("Comparing dictionaries: REFERENT = 1, COMPARED = 3");
		compareDictionaries(DICTIONARY1, DICTIONARY3);

		System.out.println("\n\n****************************************************");
		System.out.println("Comparing dictionaries: REFERENT = DOMAIN, COMPARED = 1");
		compareDictionaries(DICTIONARY4, DICTIONARY1);

		System.out.println("\n\n****************************************************");
		System.out.println("Comparing dictionaries: REFERENT = DOMAIN, COMPARED = 2");
		compareDictionaries(DICTIONARY4, DICTIONARY2);

		System.out.println("\n\n****************************************************");
		System.out.println("Comparing dictionaries: REFERENT = DOMAIN, COMPARED = 3");
		compareDictionaries(DICTIONARY4, DICTIONARY3);

		// Stemming check: check for words with same root
		int stemmScore1 = calculateStemScore(DICTIONARY1);
		System.out.println("Stem score 1: " + stemmScore1);
		int stemmScore2 = calculateStemScore(DICTIONARY2);
		System.out.println("Stem score 2: " + stemmScore2);
		int stemmScore3 = calculateStemScore(DICTIONARY3);
		System.out.println("Stem score 3: " + stemmScore3);
		int stemmScore4 = calculateStemScore(DICTIONARY4);
		System.out.println("Stem score 4: " + stemmScore4);
		
		System.out.println("Size of first dictionary: " + firstDictionarySize);
		System.out.println("Size of second dictionary: " + secondDictionarySize);
		System.out.println("Size of third dictionary: " + thirdDictionarySize);
		System.out.println("Size of domain dictionary: " + domainDictionarySize);

	}

	private static int calculateStemScore(TreeSet<String> DICTIONARY) {
		int stemScore = 0;
		for (String word : DICTIONARY) {
			if (word.length() >= 3) {
				for (String otherWord : DICTIONARY) {
					if ((!otherWord.equals(word)) && (otherWord.contains(word))) {
						stemScore++;
					}
				}
			}
		}
		return stemScore;
	}

	private static void compareDictionaries(TreeSet<String> referentDict, TreeSet<String> comparedDict) {
		TreeSet<String> commonTermsDictionary = new TreeSet<String>();
		TreeSet<String> onlyInReferentDictionary = new TreeSet<String>();
		TreeSet<String> onlyInComparedDictionary = new TreeSet<String>();
		for (String termFromFirst : referentDict) {
			if (comparedDict.contains(termFromFirst)) {
				commonTermsDictionary.add(termFromFirst);
			} else {
				onlyInReferentDictionary.add(termFromFirst);
			}
		}

		for (String termFromSecond : comparedDict) {
			if (!referentDict.contains(termFromSecond)) {
				onlyInComparedDictionary.add(termFromSecond);
			}
		}
		System.out.println("----------------------------------------------------------");
		System.out.println("COMMON TERMS: " + commonTermsDictionary.size());
		printDictionary(commonTermsDictionary);

		System.out.println("ONLY IN REFERENT DICTIONARY: " + onlyInReferentDictionary.size());
		printDictionary(onlyInReferentDictionary);

		System.out.println("ONLY IN COMPARED DICTIONARY: " + onlyInComparedDictionary.size());
		printDictionary(onlyInComparedDictionary);

	}

	public static void printDictionary(TreeSet<String> dict) {
		for (String term : dict) {
			System.out.print(term + ";");
		}
		System.out.println();
	}

	public static void fillTheDictionaryFromInputFile(File file) {
		FileInputStream fis = null;
		String line;

		try {
			fis = new FileInputStream(file);
			InputStreamReader isr = new InputStreamReader(fis, Charset.forName("UTF-8"));
			// InputStreamReader isr = new InputStreamReader(fis,
			// Charset.forName("ISO-8859-1"));
			BufferedReader br = new BufferedReader(isr);

			while ((line = br.readLine()) != null) {
				// System.out.println("Read line: " + line);
				if (line.isEmpty() || line.equals(" ") || line.equals("  ")) {
					System.out.println("Skipping empty/whitespace line...");
				} else {
					String cleanedLine = prepareLineForParsing(line);
					if (!cleanedLine.equals("")) {
						System.out.println("LINE READY FOR PARSING: " + cleanedLine);
						String[] tokens = cleanedLine.split(" ");
						for (String token : tokens) {
							WordExtractor.detectWordsAndAddToDictionary(token);
						}
						System.out.println("Size of dictionary: " + DICTIONARY.size());
					}
				}

			}

			br.close();

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (fis != null)
					fis.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

	}

	public static void detectWordsAndAddToDictionary(String token) {
		if (token.length() != 1) {
			if (token.matches(".*[a-zA-Z]+.*")) {
				// System.out.println("Potentially complex word:" + token);
				if (token.matches("[a-z]+")) {
					if (!DICTIONARY.contains(token.toLowerCase())) {
						System.out.println("Adding simple lowercase word: " + token);
						DICTIONARY.add(token.toLowerCase());
					} else {
						System.out.println("Dictionary already contains word: " + token);
					}

				} else if (token.matches("[A-Z]+")) {
					if (!DICTIONARY.contains(token.toLowerCase())) {
						System.out.println("Adding simple uppercase word: " + token);
						DICTIONARY.add(token.toLowerCase());
					} else {
						System.out.println("Dictionary already contains word: " + token);
					}
				} else if (token.matches("^[A-Z]+[a-z]*")) {

					if (!DICTIONARY.contains(token.toLowerCase())) {
						System.out.println("Adding simple capitalized word: " + token);
						DICTIONARY.add(token.toLowerCase());
					} else {
						System.out.println("Dictionary already contains word: " + token);
					}

				} else if (token.matches("^[a-zA-Z]+[0-9]+")) {
					token = token.replaceAll("[0-9]", "");
					if (!DICTIONARY.contains(token.toLowerCase())) {
						System.out.println("Adding word ending with number: " + token);
						DICTIONARY.add(token.toLowerCase());
					} else {
						System.out.println("Dictionary already contains word: " + token);
					}
				}

				else if (token.matches(".*[a-z]+.*") && token.matches(".*[A-Z]+.*")) {
					System.out.println("Complex string detected: " + token);
					String[] tokens2 = token.split("(?=\\p{Upper})");
					for (String token2 : tokens2) {
						WordExtractor.detectWordsAndAddToDictionary(token2);
					}

				} else {
					System.out.println("Unmatched string:" + token);
				}
			}
		}
	}

	public static String prepareLineForParsing(String line) {
		line = line.replaceAll("^\"|\"$", "");
		if (line.matches(".*[a-zA-Z]+.*")) {
			line = line.replaceAll("\t", "");
			line = line.replaceAll(";", "");
			line = line.replaceAll("::", " ");
			line = line.replaceAll("\\+", " ");
			line = line.replaceAll("=", " ");
			line = line.replaceAll("-", " ");
			line = line.replaceAll("%", " ");
			line = line.replaceAll("<", " ");
			line = line.replaceAll(">", " ");
			line = line.replaceAll("\\*", " ");
			line = line.replaceAll("\\]", " ");
			line = line.replaceAll("\\[", " ");
			line = line.replaceAll("\\(", " ");
			line = line.replaceAll("\\)", " ");
			line = line.replaceAll("!", " ");
			line = line.replaceAll("\\.", " ");
			line = line.replaceAll("//", " ");
			line = line.replaceAll(":", " ");
			line = line.replaceAll("\"", " ");
			line = line.replaceAll(",", " ");
			line = line.replaceAll("_", " ");
			line = line.replaceAll("\\?", " ");
			line = line.replaceAll("/", " ");
			line = line.replaceAll("'", " ");
			line = line.replaceAll("[0-9]", " ");
		} else {
			line = "";
		}
		while (line.contains("  ")) {
			line = line.replaceAll("  ", " ");
		}
		return line;
	}

}
