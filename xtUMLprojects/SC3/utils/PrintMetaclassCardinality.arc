   .//------------------------------------------------------------------------
   .print " "
   .print "Printing cardinality of metaclass instances in package Association."
Association
   .select many R_REL_inst_set from instances of R_REL
   .assign  R_REL_num = cardinality R_REL_inst_set
   .print "Metaclass Association (R_REL) has ${R_REL_num} instances."
   .if(not (R_REL_num==0))
R_REL ${R_REL_num}
   .end if
   .select many R_AONE_inst_set from instances of R_AONE
   .assign  R_AONE_num = cardinality R_AONE_inst_set
   .print "Metaclass Class As Associated One Side (R_AONE) has ${R_AONE_num} instances."
   .if(not (R_AONE_num==0))
R_AONE ${R_AONE_num}
   .end if
   .select many R_AOTH_inst_set from instances of R_AOTH
   .assign  R_AOTH_num = cardinality R_AOTH_inst_set
   .print "Metaclass Class As Associated Other Side (R_AOTH) has ${R_AOTH_num} instances."
   .if(not (R_AOTH_num==0))
R_AOTH ${R_AOTH_num}
   .end if
   .select many R_CONE_inst_set from instances of R_CONE
   .assign  R_CONE_num = cardinality R_CONE_inst_set
   .print "Metaclass Class As Derived One Side (R_CONE) has ${R_CONE_num} instances."
   .if(not (R_CONE_num==0))
R_CONE ${R_CONE_num}
   .end if
   .select many R_COTH_inst_set from instances of R_COTH
   .assign  R_COTH_num = cardinality R_COTH_inst_set
   .print "Metaclass Class As Derived Other Side (R_COTH) has ${R_COTH_num} instances."
   .if(not (R_COTH_num==0))
R_COTH ${R_COTH_num}
   .end if
   .select many R_ASSR_inst_set from instances of R_ASSR
   .assign  R_ASSR_num = cardinality R_ASSR_inst_set
   .print "Metaclass Class As Link (R_ASSR) has ${R_ASSR_num} instances."
   .if(not (R_ASSR_num==0))
R_ASSR ${R_ASSR_num}
   .end if
   .select many R_FORM_inst_set from instances of R_FORM
   .assign  R_FORM_num = cardinality R_FORM_inst_set
   .print "Metaclass Class As Simple Formalizer (R_FORM) has ${R_FORM_num} instances."
   .if(not (R_FORM_num==0))
R_FORM ${R_FORM_num}
   .end if
   .select many R_PART_inst_set from instances of R_PART
   .assign  R_PART_num = cardinality R_PART_inst_set
   .print "Metaclass Class As Simple Participant (R_PART) has ${R_PART_num} instances."
   .if(not (R_PART_num==0))
R_PART ${R_PART_num}
   .end if
   .select many R_SUB_inst_set from instances of R_SUB
   .assign  R_SUB_num = cardinality R_SUB_inst_set
   .print "Metaclass Class As Subtype (R_SUB) has ${R_SUB_num} instances."
   .if(not (R_SUB_num==0))
R_SUB ${R_SUB_num}
   .end if
   .select many R_SUPER_inst_set from instances of R_SUPER
   .assign  R_SUPER_num = cardinality R_SUPER_inst_set
   .print "Metaclass Class As Supertype (R_SUPER) has ${R_SUPER_num} instances."
   .if(not (R_SUPER_num==0))
R_SUPER ${R_SUPER_num}
   .end if
   .select many R_OIR_inst_set from instances of R_OIR
   .assign  R_OIR_num = cardinality R_OIR_inst_set
   .print "Metaclass Class In Association (R_OIR) has ${R_OIR_num} instances."
   .if(not (R_OIR_num==0))
R_OIR ${R_OIR_num}
   .end if
   .select many R_COMP_inst_set from instances of R_COMP
   .assign  R_COMP_num = cardinality R_COMP_inst_set
   .print "Metaclass Derived Association (R_COMP) has ${R_COMP_num} instances."
   .if(not (R_COMP_num==0))
R_COMP ${R_COMP_num}
   .end if
   .select many R_ASSOC_inst_set from instances of R_ASSOC
   .assign  R_ASSOC_num = cardinality R_ASSOC_inst_set
   .print "Metaclass Linked Association (R_ASSOC) has ${R_ASSOC_num} instances."
   .if(not (R_ASSOC_num==0))
R_ASSOC ${R_ASSOC_num}
   .end if
   .select many R_RTO_inst_set from instances of R_RTO
   .assign  R_RTO_num = cardinality R_RTO_inst_set
   .print "Metaclass Referred To Class in Assoc (R_RTO) has ${R_RTO_num} instances."
   .if(not (R_RTO_num==0))
R_RTO ${R_RTO_num}
   .end if
   .select many R_RGO_inst_set from instances of R_RGO
   .assign  R_RGO_num = cardinality R_RGO_inst_set
   .print "Metaclass Referring Class In Assoc (R_RGO) has ${R_RGO_num} instances."
   .if(not (R_RGO_num==0))
R_RGO ${R_RGO_num}
   .end if
   .select many R_SIMP_inst_set from instances of R_SIMP
   .assign  R_SIMP_num = cardinality R_SIMP_inst_set
   .print "Metaclass Simple Association (R_SIMP) has ${R_SIMP_num} instances."
   .if(not (R_SIMP_num==0))
R_SIMP ${R_SIMP_num}
   .end if
   .select many R_SUBSUP_inst_set from instances of R_SUBSUP
   .assign  R_SUBSUP_num = cardinality R_SUBSUP_inst_set
   .print "Metaclass Subtype Supertype Association (R_SUBSUP) has ${R_SUBSUP_num} instances."
   .if(not (R_SUBSUP_num==0))
R_SUBSUP ${R_SUBSUP_num}
   .end if
   .//------------------------------------------------------------------------
   .print " "
   .print "Printing cardinality of metaclass instances in package Body."
Body
   .select many ACT_BLK_inst_set from instances of ACT_BLK
   .assign  ACT_BLK_num = cardinality ACT_BLK_inst_set
   .print "Metaclass Block (ACT_BLK) has ${ACT_BLK_num} instances."
   .if(not (ACT_BLK_num==0))
ACT_BLK ${ACT_BLK_num}
   .end if
   .select many ACT_BIC_inst_set from instances of ACT_BIC
   .assign  ACT_BIC_num = cardinality ACT_BIC_inst_set
   .print "Metaclass Body in Component (ACT_BIC) has ${ACT_BIC_num} instances."
   .if(not (ACT_BIC_num==0))
ACT_BIC ${ACT_BIC_num}
   .end if
   .select many ACT_BIE_inst_set from instances of ACT_BIE
   .assign  ACT_BIE_num = cardinality ACT_BIE_inst_set
   .print "Metaclass Body in Element (ACT_BIE) has ${ACT_BIE_num} instances."
   .if(not (ACT_BIE_num==0))
ACT_BIE ${ACT_BIE_num}
   .end if
   .select many ACT_ACT_inst_set from instances of ACT_ACT
   .assign  ACT_ACT_num = cardinality ACT_ACT_inst_set
   .print "Metaclass Body (ACT_ACT) has ${ACT_ACT_num} instances."
   .if(not (ACT_ACT_num==0))
ACT_ACT ${ACT_ACT_num}
   .end if
   .select many ACT_BRK_inst_set from instances of ACT_BRK
   .assign  ACT_BRK_num = cardinality ACT_BRK_inst_set
   .print "Metaclass Break (ACT_BRK) has ${ACT_BRK_num} instances."
   .if(not (ACT_BRK_num==0))
ACT_BRK ${ACT_BRK_num}
   .end if
   .select many ACT_BRB_inst_set from instances of ACT_BRB
   .assign  ACT_BRB_num = cardinality ACT_BRB_inst_set
   .print "Metaclass Bridge Body (ACT_BRB) has ${ACT_BRB_num} instances."
   .if(not (ACT_BRB_num==0))
ACT_BRB ${ACT_BRB_num}
   .end if
   .select many ACT_CON_inst_set from instances of ACT_CON
   .assign  ACT_CON_num = cardinality ACT_CON_inst_set
   .print "Metaclass Continue (ACT_CON) has ${ACT_CON_num} instances."
   .if(not (ACT_CON_num==0))
ACT_CON ${ACT_CON_num}
   .end if
   .select many ACT_CTL_inst_set from instances of ACT_CTL
   .assign  ACT_CTL_num = cardinality ACT_CTL_inst_set
   .print "Metaclass Control (ACT_CTL) has ${ACT_CTL_num} instances."
   .if(not (ACT_CTL_num==0))
ACT_CTL ${ACT_CTL_num}
   .end if
   .select many ACT_DAB_inst_set from instances of ACT_DAB
   .assign  ACT_DAB_num = cardinality ACT_DAB_inst_set
   .print "Metaclass Derived Attribute Body (ACT_DAB) has ${ACT_DAB_num} instances."
   .if(not (ACT_DAB_num==0))
ACT_DAB ${ACT_DAB_num}
   .end if
   .select many ACT_E_inst_set from instances of ACT_E
   .assign  ACT_E_num = cardinality ACT_E_inst_set
   .print "Metaclass Else Stmt (ACT_E) has ${ACT_E_num} instances."
   .if(not (ACT_E_num==0))
ACT_E ${ACT_E_num}
   .end if
   .select many ACT_EL_inst_set from instances of ACT_EL
   .assign  ACT_EL_num = cardinality ACT_EL_inst_set
   .print "Metaclass ElseIf Stmt (ACT_EL) has ${ACT_EL_num} instances."
   .if(not (ACT_EL_num==0))
ACT_EL ${ACT_EL_num}
   .end if
   .select many ACT_FOR_inst_set from instances of ACT_FOR
   .assign  ACT_FOR_num = cardinality ACT_FOR_inst_set
   .print "Metaclass For Stmt (ACT_FOR) has ${ACT_FOR_num} instances."
   .if(not (ACT_FOR_num==0))
ACT_FOR ${ACT_FOR_num}
   .end if
   .select many ACT_FNB_inst_set from instances of ACT_FNB
   .assign  ACT_FNB_num = cardinality ACT_FNB_inst_set
   .print "Metaclass Function Body (ACT_FNB) has ${ACT_FNB_num} instances."
   .if(not (ACT_FNB_num==0))
ACT_FNB ${ACT_FNB_num}
   .end if
   .select many ACT_IF_inst_set from instances of ACT_IF
   .assign  ACT_IF_num = cardinality ACT_IF_inst_set
   .print "Metaclass If Stmt (ACT_IF) has ${ACT_IF_num} instances."
   .if(not (ACT_IF_num==0))
ACT_IF ${ACT_IF_num}
   .end if
   .select many ACT_OPB_inst_set from instances of ACT_OPB
   .assign  ACT_OPB_num = cardinality ACT_OPB_inst_set
   .print "Metaclass Operation Body (ACT_OPB) has ${ACT_OPB_num} instances."
   .if(not (ACT_OPB_num==0))
ACT_OPB ${ACT_OPB_num}
   .end if
   .select many ACT_POB_inst_set from instances of ACT_POB
   .assign  ACT_POB_num = cardinality ACT_POB_inst_set
   .print "Metaclass Provided Operation Body (ACT_POB) has ${ACT_POB_num} instances."
   .if(not (ACT_POB_num==0))
ACT_POB ${ACT_POB_num}
   .end if
   .select many ACT_PSB_inst_set from instances of ACT_PSB
   .assign  ACT_PSB_num = cardinality ACT_PSB_inst_set
   .print "Metaclass Provided Signal Body (ACT_PSB) has ${ACT_PSB_num} instances."
   .if(not (ACT_PSB_num==0))
ACT_PSB ${ACT_PSB_num}
   .end if
   .select many ACT_ROB_inst_set from instances of ACT_ROB
   .assign  ACT_ROB_num = cardinality ACT_ROB_inst_set
   .print "Metaclass Required Operation Body (ACT_ROB) has ${ACT_ROB_num} instances."
   .if(not (ACT_ROB_num==0))
ACT_ROB ${ACT_ROB_num}
   .end if
   .select many ACT_RSB_inst_set from instances of ACT_RSB
   .assign  ACT_RSB_num = cardinality ACT_RSB_inst_set
   .print "Metaclass Required Signal Body (ACT_RSB) has ${ACT_RSB_num} instances."
   .if(not (ACT_RSB_num==0))
ACT_RSB ${ACT_RSB_num}
   .end if
   .select many ACT_SAB_inst_set from instances of ACT_SAB
   .assign  ACT_SAB_num = cardinality ACT_SAB_inst_set
   .print "Metaclass State Action Body (ACT_SAB) has ${ACT_SAB_num} instances."
   .if(not (ACT_SAB_num==0))
ACT_SAB ${ACT_SAB_num}
   .end if
   .select many ACT_SMT_inst_set from instances of ACT_SMT
   .assign  ACT_SMT_num = cardinality ACT_SMT_inst_set
   .print "Metaclass Statement (ACT_SMT) has ${ACT_SMT_num} instances."
   .if(not (ACT_SMT_num==0))
ACT_SMT ${ACT_SMT_num}
   .end if
   .select many ACT_TAB_inst_set from instances of ACT_TAB
   .assign  ACT_TAB_num = cardinality ACT_TAB_inst_set
   .print "Metaclass Transition Action Body (ACT_TAB) has ${ACT_TAB_num} instances."
   .if(not (ACT_TAB_num==0))
ACT_TAB ${ACT_TAB_num}
   .end if
   .select many ACT_WHL_inst_set from instances of ACT_WHL
   .assign  ACT_WHL_num = cardinality ACT_WHL_inst_set
   .print "Metaclass While Stmt (ACT_WHL) has ${ACT_WHL_num} instances."
   .if(not (ACT_WHL_num==0))
ACT_WHL ${ACT_WHL_num}
   .end if
   .//------------------------------------------------------------------------
   .print " "
   .print "Printing cardinality of metaclass instances in package Component."
Component
   .select many C_C_inst_set from instances of C_C
   .assign  C_C_num = cardinality C_C_inst_set
   .print "Metaclass Component (C_C) has ${C_C_num} instances."
   .if(not (C_C_num==0))
C_C ${C_C_num}
   .end if
   .select many C_DG_inst_set from instances of C_DG
   .assign  C_DG_num = cardinality C_DG_inst_set
   .print "Metaclass Delegation (C_DG) has ${C_DG_num} instances."
   .if(not (C_DG_num==0))
C_DG ${C_DG_num}
   .end if
   .select many C_EP_inst_set from instances of C_EP
   .assign  C_EP_num = cardinality C_EP_inst_set
   .print "Metaclass Executable Property (C_EP) has ${C_EP_num} instances."
   .if(not (C_EP_num==0))
C_EP ${C_EP_num}
   .end if
   .select many C_IO_inst_set from instances of C_IO
   .assign  C_IO_num = cardinality C_IO_inst_set
   .print "Metaclass Interface Operation (C_IO) has ${C_IO_num} instances."
   .if(not (C_IO_num==0))
C_IO ${C_IO_num}
   .end if
   .select many C_RID_inst_set from instances of C_RID
   .assign  C_RID_num = cardinality C_RID_inst_set
   .print "Metaclass Interface Reference In Delegation (C_RID) has ${C_RID_num} instances."
   .if(not (C_RID_num==0))
C_RID ${C_RID_num}
   .end if
   .select many C_IR_inst_set from instances of C_IR
   .assign  C_IR_num = cardinality C_IR_inst_set
   .print "Metaclass Interface Reference (C_IR) has ${C_IR_num} instances."
   .if(not (C_IR_num==0))
C_IR ${C_IR_num}
   .end if
   .select many C_AS_inst_set from instances of C_AS
   .assign  C_AS_num = cardinality C_AS_inst_set
   .print "Metaclass Interface Signal (C_AS) has ${C_AS_num} instances."
   .if(not (C_AS_num==0))
C_AS ${C_AS_num}
   .end if
   .select many C_I_inst_set from instances of C_I
   .assign  C_I_num = cardinality C_I_inst_set
   .print "Metaclass Interface (C_I) has ${C_I_num} instances."
   .if(not (C_I_num==0))
C_I ${C_I_num}
   .end if
   .select many C_PO_inst_set from instances of C_PO
   .assign  C_PO_num = cardinality C_PO_inst_set
   .print "Metaclass Port (C_PO) has ${C_PO_num} instances."
   .if(not (C_PO_num==0))
C_PO ${C_PO_num}
   .end if
   .select many C_PP_inst_set from instances of C_PP
   .assign  C_PP_num = cardinality C_PP_inst_set
   .print "Metaclass Property Parameter (C_PP) has ${C_PP_num} instances."
   .if(not (C_PP_num==0))
C_PP ${C_PP_num}
   .end if
   .select many C_P_inst_set from instances of C_P
   .assign  C_P_num = cardinality C_P_inst_set
   .print "Metaclass Provision (C_P) has ${C_P_num} instances."
   .if(not (C_P_num==0))
C_P ${C_P_num}
   .end if
   .select many C_R_inst_set from instances of C_R
   .assign  C_R_num = cardinality C_R_inst_set
   .print "Metaclass Requirement (C_R) has ${C_R_num} instances."
   .if(not (C_R_num==0))
C_R ${C_R_num}
   .end if
   .select many C_SF_inst_set from instances of C_SF
   .assign  C_SF_num = cardinality C_SF_inst_set
   .print "Metaclass Satisfaction (C_SF) has ${C_SF_num} instances."
   .if(not (C_SF_num==0))
C_SF ${C_SF_num}
   .end if
   .//------------------------------------------------------------------------
   .print " "
   .print "Printing cardinality of metaclass instances in package Component Library."
Component Library
   .select many CL_IC_inst_set from instances of CL_IC
   .assign  CL_IC_num = cardinality CL_IC_inst_set
   .print "Metaclass Component Reference (CL_IC) has ${CL_IC_num} instances."
   .if(not (CL_IC_num==0))
CL_IC ${CL_IC_num}
   .end if
   .select many CL_IPINS_inst_set from instances of CL_IPINS
   .assign  CL_IPINS_num = cardinality CL_IPINS_inst_set
   .print "Metaclass Imported Provision In Satisfaction (CL_IPINS) has ${CL_IPINS_num} instances."
   .if(not (CL_IPINS_num==0))
CL_IPINS ${CL_IPINS_num}
   .end if
   .select many CL_IP_inst_set from instances of CL_IP
   .assign  CL_IP_num = cardinality CL_IP_inst_set
   .print "Metaclass Imported Provision (CL_IP) has ${CL_IP_num} instances."
   .if(not (CL_IP_num==0))
CL_IP ${CL_IP_num}
   .end if
   .select many CL_IIR_inst_set from instances of CL_IIR
   .assign  CL_IIR_num = cardinality CL_IIR_inst_set
   .print "Metaclass Imported Reference (CL_IIR) has ${CL_IIR_num} instances."
   .if(not (CL_IIR_num==0))
CL_IIR ${CL_IIR_num}
   .end if
   .select many CL_IR_inst_set from instances of CL_IR
   .assign  CL_IR_num = cardinality CL_IR_inst_set
   .print "Metaclass Imported Requirement (CL_IR) has ${CL_IR_num} instances."
   .if(not (CL_IR_num==0))
CL_IR ${CL_IR_num}
   .end if
   .//------------------------------------------------------------------------
   .print " "
   .print "Printing cardinality of metaclass instances in package Component Nesting."
Component Nesting
   .select many CN_CIC_inst_set from instances of CN_CIC
   .assign  CN_CIC_num = cardinality CN_CIC_inst_set
   .print "Metaclass Component in Component (CN_CIC) has ${CN_CIC_num} instances."
   .if(not (CN_CIC_num==0))
CN_CIC ${CN_CIC_num}
   .end if
   .select many CN_DC_inst_set from instances of CN_DC
   .assign  CN_DC_num = cardinality CN_DC_inst_set
   .print "Metaclass Domain As Component (CN_DC) has ${CN_DC_num} instances."
   .if(not (CN_DC_num==0))
CN_DC ${CN_DC_num}
   .end if
   .//------------------------------------------------------------------------
   .print " "
   .print "Printing cardinality of metaclass instances in package Component Packaging."
Component Packaging
   .select many CP_CPINP_inst_set from instances of CP_CPINP
   .assign  CP_CPINP_num = cardinality CP_CPINP_inst_set
   .print "Metaclass Component Package in Package (CP_CPINP) has ${CP_CPINP_num} instances."
   .if(not (CP_CPINP_num==0))
CP_CPINP ${CP_CPINP_num}
   .end if
   .select many CP_CP_inst_set from instances of CP_CP
   .assign  CP_CP_num = cardinality CP_CP_inst_set
   .print "Metaclass Component Package (CP_CP) has ${CP_CP_num} instances."
   .if(not (CP_CP_num==0))
CP_CP ${CP_CP_num}
   .end if
   .//------------------------------------------------------------------------
   .print " "
   .print "Printing cardinality of metaclass instances in package Signal Provisions and Requirements."
Signal Provisions and Requirements
   .select many SPR_PEP_inst_set from instances of SPR_PEP
   .assign  SPR_PEP_num = cardinality SPR_PEP_inst_set
   .print "Metaclass Provided Executable Property (SPR_PEP) has ${SPR_PEP_num} instances."
   .if(not (SPR_PEP_num==0))
SPR_PEP ${SPR_PEP_num}
   .end if
   .select many SPR_PO_inst_set from instances of SPR_PO
   .assign  SPR_PO_num = cardinality SPR_PO_inst_set
   .print "Metaclass Provided Operation (SPR_PO) has ${SPR_PO_num} instances."
   .if(not (SPR_PO_num==0))
SPR_PO ${SPR_PO_num}
   .end if
   .select many SPR_PS_inst_set from instances of SPR_PS
   .assign  SPR_PS_num = cardinality SPR_PS_inst_set
   .print "Metaclass Provided Signal (SPR_PS) has ${SPR_PS_num} instances."
   .if(not (SPR_PS_num==0))
SPR_PS ${SPR_PS_num}
   .end if
   .select many SPR_REP_inst_set from instances of SPR_REP
   .assign  SPR_REP_num = cardinality SPR_REP_inst_set
   .print "Metaclass Required Executable Property (SPR_REP) has ${SPR_REP_num} instances."
   .if(not (SPR_REP_num==0))
SPR_REP ${SPR_REP_num}
   .end if
   .select many SPR_RO_inst_set from instances of SPR_RO
   .assign  SPR_RO_num = cardinality SPR_RO_inst_set
   .print "Metaclass Required Operation (SPR_RO) has ${SPR_RO_num} instances."
   .if(not (SPR_RO_num==0))
SPR_RO ${SPR_RO_num}
   .end if
   .select many SPR_RS_inst_set from instances of SPR_RS
   .assign  SPR_RS_num = cardinality SPR_RS_inst_set
   .print "Metaclass Required Signal (SPR_RS) has ${SPR_RS_num} instances."
   .if(not (SPR_RS_num==0))
SPR_RS ${SPR_RS_num}
   .end if
   .//------------------------------------------------------------------------
   .print " "
   .print "Printing cardinality of metaclass instances in package Constants."
Constants
   .select many CNST_CSP_inst_set from instances of CNST_CSP
   .assign  CNST_CSP_num = cardinality CNST_CSP_inst_set
   .print "Metaclass Constant Specification (CNST_CSP) has ${CNST_CSP_num} instances."
   .if(not (CNST_CSP_num==0))
CNST_CSP ${CNST_CSP_num}
   .end if
   .select many CNST_CIP_inst_set from instances of CNST_CIP
   .assign  CNST_CIP_num = cardinality CNST_CIP_inst_set
   .print "Metaclass Constant in Package (CNST_CIP) has ${CNST_CIP_num} instances."
   .if(not (CNST_CIP_num==0))
CNST_CIP ${CNST_CIP_num}
   .end if
   .select many CNST_LFSC_inst_set from instances of CNST_LFSC
   .assign  CNST_LFSC_num = cardinality CNST_LFSC_inst_set
   .print "Metaclass Leaf Symbolic Constant (CNST_LFSC) has ${CNST_LFSC_num} instances."
   .if(not (CNST_LFSC_num==0))
CNST_LFSC ${CNST_LFSC_num}
   .end if
   .select many CNST_LSC_inst_set from instances of CNST_LSC
   .assign  CNST_LSC_num = cardinality CNST_LSC_inst_set
   .print "Metaclass Literal Symbolic Constant (CNST_LSC) has ${CNST_LSC_num} instances."
   .if(not (CNST_LSC_num==0))
CNST_LSC ${CNST_LSC_num}
   .end if
   .select many CNST_SYC_inst_set from instances of CNST_SYC
   .assign  CNST_SYC_num = cardinality CNST_SYC_inst_set
   .print "Metaclass Symbolic Constant (CNST_SYC) has ${CNST_SYC_num} instances."
   .if(not (CNST_SYC_num==0))
CNST_SYC ${CNST_SYC_num}
   .end if
   .//------------------------------------------------------------------------
   .print " "
   .print "Printing cardinality of metaclass instances in package Domain."
Domain
   .select many S_BPARM_inst_set from instances of S_BPARM
   .assign  S_BPARM_num = cardinality S_BPARM_inst_set
   .print "Metaclass Bridge Parameter (S_BPARM) has ${S_BPARM_num} instances."
   .if(not (S_BPARM_num==0))
S_BPARM ${S_BPARM_num}
   .end if
   .select many S_BRG_inst_set from instances of S_BRG
   .assign  S_BRG_num = cardinality S_BRG_inst_set
   .print "Metaclass Bridge (S_BRG) has ${S_BRG_num} instances."
   .if(not (S_BRG_num==0))
S_BRG ${S_BRG_num}
   .end if
   .select many S_CDT_inst_set from instances of S_CDT
   .assign  S_CDT_num = cardinality S_CDT_inst_set
   .print "Metaclass Core Data Type (S_CDT) has ${S_CDT_num} instances."
   .if(not (S_CDT_num==0))
S_CDT ${S_CDT_num}
   .end if
   .select many S_DPIP_inst_set from instances of S_DPIP
   .assign  S_DPIP_num = cardinality S_DPIP_inst_set
   .print "Metaclass Data Type Package in Package (S_DPIP) has ${S_DPIP_num} instances."
   .if(not (S_DPIP_num==0))
S_DPIP ${S_DPIP_num}
   .end if
   .select many S_DPK_inst_set from instances of S_DPK
   .assign  S_DPK_num = cardinality S_DPK_inst_set
   .print "Metaclass Data Type Package (S_DPK) has ${S_DPK_num} instances."
   .if(not (S_DPK_num==0))
S_DPK ${S_DPK_num}
   .end if
   .select many S_DIP_inst_set from instances of S_DIP
   .assign  S_DIP_num = cardinality S_DIP_inst_set
   .print "Metaclass Data Type in Package (S_DIP) has ${S_DIP_num} instances."
   .if(not (S_DIP_num==0))
S_DIP ${S_DIP_num}
   .end if
   .select many S_DT_inst_set from instances of S_DT
   .assign  S_DT_num = cardinality S_DT_inst_set
   .print "Metaclass Data Type (S_DT) has ${S_DT_num} instances."
   .if(not (S_DT_num==0))
S_DT ${S_DT_num}
   .end if
   .select many S_DIS_inst_set from instances of S_DIS
   .assign  S_DIS_num = cardinality S_DIS_inst_set
   .print "Metaclass Datatype In Suppression (S_DIS) has ${S_DIS_num} instances."
   .if(not (S_DIS_num==0))
S_DIS ${S_DIS_num}
   .end if
   .select many S_DIM_inst_set from instances of S_DIM
   .assign  S_DIM_num = cardinality S_DIM_inst_set
   .print "Metaclass Dimensions (S_DIM) has ${S_DIM_num} instances."
   .if(not (S_DIM_num==0))
S_DIM ${S_DIM_num}
   .end if
   .select many S_DOM_inst_set from instances of S_DOM
   .assign  S_DOM_num = cardinality S_DOM_inst_set
   .print "Metaclass Domain (S_DOM) has ${S_DOM_num} instances."
   .if(not (S_DOM_num==0))
S_DOM ${S_DOM_num}
   .end if
   .select many S_EEPIP_inst_set from instances of S_EEPIP
   .assign  S_EEPIP_num = cardinality S_EEPIP_inst_set
   .print "Metaclass EE Package in Package (S_EEPIP) has ${S_EEPIP_num} instances."
   .if(not (S_EEPIP_num==0))
S_EEPIP ${S_EEPIP_num}
   .end if
   .select many S_EDT_inst_set from instances of S_EDT
   .assign  S_EDT_num = cardinality S_EDT_inst_set
   .print "Metaclass Enumeration Data Type (S_EDT) has ${S_EDT_num} instances."
   .if(not (S_EDT_num==0))
S_EDT ${S_EDT_num}
   .end if
   .select many S_ENUM_inst_set from instances of S_ENUM
   .assign  S_ENUM_num = cardinality S_ENUM_inst_set
   .print "Metaclass Enumerator (S_ENUM) has ${S_ENUM_num} instances."
   .if(not (S_ENUM_num==0))
S_ENUM ${S_ENUM_num}
   .end if
   .select many S_EEDI_inst_set from instances of S_EEDI
   .assign  S_EEDI_num = cardinality S_EEDI_inst_set
   .print "Metaclass External Entity Data Item (S_EEDI) has ${S_EEDI_num} instances."
   .if(not (S_EEDI_num==0))
S_EEDI ${S_EEDI_num}
   .end if
   .select many S_EEEDI_inst_set from instances of S_EEEDI
   .assign  S_EEEDI_num = cardinality S_EEEDI_inst_set
   .print "Metaclass External Entity Event Data Item (S_EEEDI) has ${S_EEEDI_num} instances."
   .if(not (S_EEEDI_num==0))
S_EEEDI ${S_EEEDI_num}
   .end if
   .select many S_EEEDT_inst_set from instances of S_EEEDT
   .assign  S_EEEDT_num = cardinality S_EEEDT_inst_set
   .print "Metaclass External Entity Event Data (S_EEEDT) has ${S_EEEDT_num} instances."
   .if(not (S_EEEDT_num==0))
S_EEEDT ${S_EEEDT_num}
   .end if
   .select many S_EEEVT_inst_set from instances of S_EEEVT
   .assign  S_EEEVT_num = cardinality S_EEEVT_inst_set
   .print "Metaclass External Entity Event (S_EEEVT) has ${S_EEEVT_num} instances."
   .if(not (S_EEEVT_num==0))
S_EEEVT ${S_EEEVT_num}
   .end if
   .select many S_EEPK_inst_set from instances of S_EEPK
   .assign  S_EEPK_num = cardinality S_EEPK_inst_set
   .print "Metaclass External Entity Package (S_EEPK) has ${S_EEPK_num} instances."
   .if(not (S_EEPK_num==0))
S_EEPK ${S_EEPK_num}
   .end if
   .select many S_EEM_inst_set from instances of S_EEM
   .assign  S_EEM_num = cardinality S_EEM_inst_set
   .print "Metaclass External Entity in Model (S_EEM) has ${S_EEM_num} instances."
   .if(not (S_EEM_num==0))
S_EEM ${S_EEM_num}
   .end if
   .select many S_EEIP_inst_set from instances of S_EEIP
   .assign  S_EEIP_num = cardinality S_EEIP_inst_set
   .print "Metaclass External Entity in Package (S_EEIP) has ${S_EEIP_num} instances."
   .if(not (S_EEIP_num==0))
S_EEIP ${S_EEIP_num}
   .end if
   .select many S_EE_inst_set from instances of S_EE
   .assign  S_EE_num = cardinality S_EE_inst_set
   .print "Metaclass External Entity (S_EE) has ${S_EE_num} instances."
   .if(not (S_EE_num==0))
S_EE ${S_EE_num}
   .end if
   .select many S_FPIP_inst_set from instances of S_FPIP
   .assign  S_FPIP_num = cardinality S_FPIP_inst_set
   .print "Metaclass Function Package in Package (S_FPIP) has ${S_FPIP_num} instances."
   .if(not (S_FPIP_num==0))
S_FPIP ${S_FPIP_num}
   .end if
   .select many S_FPK_inst_set from instances of S_FPK
   .assign  S_FPK_num = cardinality S_FPK_inst_set
   .print "Metaclass Function Package (S_FPK) has ${S_FPK_num} instances."
   .if(not (S_FPK_num==0))
S_FPK ${S_FPK_num}
   .end if
   .select many S_SPARM_inst_set from instances of S_SPARM
   .assign  S_SPARM_num = cardinality S_SPARM_inst_set
   .print "Metaclass Function Parameter (S_SPARM) has ${S_SPARM_num} instances."
   .if(not (S_SPARM_num==0))
S_SPARM ${S_SPARM_num}
   .end if
   .select many S_FIP_inst_set from instances of S_FIP
   .assign  S_FIP_num = cardinality S_FIP_inst_set
   .print "Metaclass Function in Package (S_FIP) has ${S_FIP_num} instances."
   .if(not (S_FIP_num==0))
S_FIP ${S_FIP_num}
   .end if
   .select many S_SYNC_inst_set from instances of S_SYNC
   .assign  S_SYNC_num = cardinality S_SYNC_inst_set
   .print "Metaclass Function (S_SYNC) has ${S_SYNC_num} instances."
   .if(not (S_SYNC_num==0))
S_SYNC ${S_SYNC_num}
   .end if
   .select many S_IRDT_inst_set from instances of S_IRDT
   .assign  S_IRDT_num = cardinality S_IRDT_inst_set
   .print "Metaclass Instance Reference Data Type (S_IRDT) has ${S_IRDT_num} instances."
   .if(not (S_IRDT_num==0))
S_IRDT ${S_IRDT_num}
   .end if
   .select many S_MBR_inst_set from instances of S_MBR
   .assign  S_MBR_num = cardinality S_MBR_inst_set
   .print "Metaclass Structure Member (S_MBR) has ${S_MBR_num} instances."
   .if(not (S_MBR_num==0))
S_MBR ${S_MBR_num}
   .end if
   .select many S_SDT_inst_set from instances of S_SDT
   .assign  S_SDT_num = cardinality S_SDT_inst_set
   .print "Metaclass Structured Data Type (S_SDT) has ${S_SDT_num} instances."
   .if(not (S_SDT_num==0))
S_SDT ${S_SDT_num}
   .end if
   .select many S_SID_inst_set from instances of S_SID
   .assign  S_SID_num = cardinality S_SID_inst_set
   .print "Metaclass Subsystem in Domain (S_SID) has ${S_SID_num} instances."
   .if(not (S_SID_num==0))
S_SID ${S_SID_num}
   .end if
   .select many S_SIS_inst_set from instances of S_SIS
   .assign  S_SIS_num = cardinality S_SIS_inst_set
   .print "Metaclass Subsystem in Subsystem (S_SIS) has ${S_SIS_num} instances."
   .if(not (S_SIS_num==0))
S_SIS ${S_SIS_num}
   .end if
   .select many S_SS_inst_set from instances of S_SS
   .assign  S_SS_num = cardinality S_SS_inst_set
   .print "Metaclass Subsystem (S_SS) has ${S_SS_num} instances."
   .if(not (S_SS_num==0))
S_SS ${S_SS_num}
   .end if
   .select many S_SYS_inst_set from instances of S_SYS
   .assign  S_SYS_num = cardinality S_SYS_inst_set
   .print "Metaclass System Model (S_SYS) has ${S_SYS_num} instances."
   .if(not (S_SYS_num==0))
S_SYS ${S_SYS_num}
   .end if
   .select many S_UDT_inst_set from instances of S_UDT
   .assign  S_UDT_num = cardinality S_UDT_inst_set
   .print "Metaclass User Data Type (S_UDT) has ${S_UDT_num} instances."
   .if(not (S_UDT_num==0))
S_UDT ${S_UDT_num}
   .end if
   .//------------------------------------------------------------------------
   .print " "
   .print "Printing cardinality of metaclass instances in package Package Linking."
Package Linking
   .select many PL_EEPID_inst_set from instances of PL_EEPID
   .assign  PL_EEPID_num = cardinality PL_EEPID_inst_set
   .print "Metaclass External Entity Package in Domain (PL_EEPID) has ${PL_EEPID_num} instances."
   .if(not (PL_EEPID_num==0))
PL_EEPID ${PL_EEPID_num}
   .end if
   .select many PL_FPID_inst_set from instances of PL_FPID
   .assign  PL_FPID_num = cardinality PL_FPID_inst_set
   .print "Metaclass Function Package in Domain (PL_FPID) has ${PL_FPID_num} instances."
   .if(not (PL_FPID_num==0))
PL_FPID ${PL_FPID_num}
   .end if
   .//------------------------------------------------------------------------
   .print " "
   .print "Printing cardinality of metaclass instances in package Element Packaging."
Element Packaging
   .select many EP_PIP_inst_set from instances of EP_PIP
   .assign  EP_PIP_num = cardinality EP_PIP_inst_set
   .print "Metaclass Package In Package (EP_PIP) has ${EP_PIP_num} instances."
   .if(not (EP_PIP_num==0))
EP_PIP ${EP_PIP_num}
   .end if
   .select many EP_PKG_inst_set from instances of EP_PKG
   .assign  EP_PKG_num = cardinality EP_PKG_inst_set
   .print "Metaclass Package (EP_PKG) has ${EP_PKG_num} instances."
   .if(not (EP_PKG_num==0))
EP_PKG ${EP_PKG_num}
   .end if
   .select many EP_SPKG_inst_set from instances of EP_SPKG
   .assign  EP_SPKG_num = cardinality EP_SPKG_inst_set
   .print "Metaclass Specification Package (EP_SPKG) has ${EP_SPKG_num} instances."
   .if(not (EP_SPKG_num==0))
EP_SPKG ${EP_SPKG_num}
   .end if
   .//------------------------------------------------------------------------
   .print " "
   .print "Printing cardinality of metaclass instances in package Event."
Event
   .select many E_CES_inst_set from instances of E_CES
   .assign  E_CES_num = cardinality E_CES_inst_set
   .print "Metaclass Create Event Statement (E_CES) has ${E_CES_num} instances."
   .if(not (E_CES_num==0))
E_CES ${E_CES_num}
   .end if
   .select many E_CEA_inst_set from instances of E_CEA
   .assign  E_CEA_num = cardinality E_CEA_inst_set
   .print "Metaclass Create Event to Class (E_CEA) has ${E_CEA_num} instances."
   .if(not (E_CEA_num==0))
E_CEA ${E_CEA_num}
   .end if
   .select many E_CEC_inst_set from instances of E_CEC
   .assign  E_CEC_num = cardinality E_CEC_inst_set
   .print "Metaclass Create Event to Creator (E_CEC) has ${E_CEC_num} instances."
   .if(not (E_CEC_num==0))
E_CEC ${E_CEC_num}
   .end if
   .select many E_CEE_inst_set from instances of E_CEE
   .assign  E_CEE_num = cardinality E_CEE_inst_set
   .print "Metaclass Create Event to External Entity (E_CEE) has ${E_CEE_num} instances."
   .if(not (E_CEE_num==0))
E_CEE ${E_CEE_num}
   .end if
   .select many E_CEI_inst_set from instances of E_CEI
   .assign  E_CEI_num = cardinality E_CEI_inst_set
   .print "Metaclass Create Event to Instance (E_CEI) has ${E_CEI_num} instances."
   .if(not (E_CEI_num==0))
E_CEI ${E_CEI_num}
   .end if
   .select many E_CSME_inst_set from instances of E_CSME
   .assign  E_CSME_num = cardinality E_CSME_inst_set
   .print "Metaclass Create SM Event Statement (E_CSME) has ${E_CSME_num} instances."
   .if(not (E_CSME_num==0))
E_CSME ${E_CSME_num}
   .end if
   .select many E_ESS_inst_set from instances of E_ESS
   .assign  E_ESS_num = cardinality E_ESS_inst_set
   .print "Metaclass Event Specification Statement (E_ESS) has ${E_ESS_num} instances."
   .if(not (E_ESS_num==0))
E_ESS ${E_ESS_num}
   .end if
   .select many E_GES_inst_set from instances of E_GES
   .assign  E_GES_num = cardinality E_GES_inst_set
   .print "Metaclass Generate Event Statement (E_GES) has ${E_GES_num} instances."
   .if(not (E_GES_num==0))
E_GES ${E_GES_num}
   .end if
   .select many E_GPR_inst_set from instances of E_GPR
   .assign  E_GPR_num = cardinality E_GPR_inst_set
   .print "Metaclass Generate Preexisting Event (E_GPR) has ${E_GPR_num} instances."
   .if(not (E_GPR_num==0))
E_GPR ${E_GPR_num}
   .end if
   .select many E_GSME_inst_set from instances of E_GSME
   .assign  E_GSME_num = cardinality E_GSME_inst_set
   .print "Metaclass Generate SM Event Statement (E_GSME) has ${E_GSME_num} instances."
   .if(not (E_GSME_num==0))
E_GSME ${E_GSME_num}
   .end if
   .select many E_GAR_inst_set from instances of E_GAR
   .assign  E_GAR_num = cardinality E_GAR_inst_set
   .print "Metaclass Generate to Class (E_GAR) has ${E_GAR_num} instances."
   .if(not (E_GAR_num==0))
E_GAR ${E_GAR_num}
   .end if
   .select many E_GEC_inst_set from instances of E_GEC
   .assign  E_GEC_num = cardinality E_GEC_inst_set
   .print "Metaclass Generate to Creator (E_GEC) has ${E_GEC_num} instances."
   .if(not (E_GEC_num==0))
E_GEC ${E_GEC_num}
   .end if
   .select many E_GEE_inst_set from instances of E_GEE
   .assign  E_GEE_num = cardinality E_GEE_inst_set
   .print "Metaclass Generate to External Entity (E_GEE) has ${E_GEE_num} instances."
   .if(not (E_GEE_num==0))
E_GEE ${E_GEE_num}
   .end if
   .select many E_GEN_inst_set from instances of E_GEN
   .assign  E_GEN_num = cardinality E_GEN_inst_set
   .print "Metaclass Generate (E_GEN) has ${E_GEN_num} instances."
   .if(not (E_GEN_num==0))
E_GEN ${E_GEN_num}
   .end if
   .//------------------------------------------------------------------------
   .print " "
   .print "Printing cardinality of metaclass instances in package Globals."
Globals
   .select many G_EIS_inst_set from instances of G_EIS
   .assign  G_EIS_num = cardinality G_EIS_inst_set
   .print "Metaclass Global Element in System (G_EIS) has ${G_EIS_num} instances."
   .if(not (G_EIS_num==0))
G_EIS ${G_EIS_num}
   .end if
   .//------------------------------------------------------------------------
   .print " "
   .print "Printing cardinality of metaclass instances in package Instance Access."
Instance Access
   .select many ACT_AI_inst_set from instances of ACT_AI
   .assign  ACT_AI_num = cardinality ACT_AI_inst_set
   .print "Metaclass Assign to Member (ACT_AI) has ${ACT_AI_num} instances."
   .if(not (ACT_AI_num==0))
ACT_AI ${ACT_AI_num}
   .end if
   .select many ACT_CNV_inst_set from instances of ACT_CNV
   .assign  ACT_CNV_num = cardinality ACT_CNV_inst_set
   .print "Metaclass Create No Variable (ACT_CNV) has ${ACT_CNV_num} instances."
   .if(not (ACT_CNV_num==0))
ACT_CNV ${ACT_CNV_num}
   .end if
   .select many ACT_CR_inst_set from instances of ACT_CR
   .assign  ACT_CR_num = cardinality ACT_CR_inst_set
   .print "Metaclass Create (ACT_CR) has ${ACT_CR_num} instances."
   .if(not (ACT_CR_num==0))
ACT_CR ${ACT_CR_num}
   .end if
   .select many ACT_DEL_inst_set from instances of ACT_DEL
   .assign  ACT_DEL_num = cardinality ACT_DEL_inst_set
   .print "Metaclass Delete (ACT_DEL) has ${ACT_DEL_num} instances."
   .if(not (ACT_DEL_num==0))
ACT_DEL ${ACT_DEL_num}
   .end if
   .//------------------------------------------------------------------------
   .print " "
   .print "Printing cardinality of metaclass instances in package Interface Package."
Interface Package
   .select many IP_IPINIP_inst_set from instances of IP_IPINIP
   .assign  IP_IPINIP_num = cardinality IP_IPINIP_inst_set
   .print "Metaclass Interface Package in Interface Package (IP_IPINIP) has ${IP_IPINIP_num} instances."
   .if(not (IP_IPINIP_num==0))
IP_IPINIP ${IP_IPINIP_num}
   .end if
   .select many IP_IP_inst_set from instances of IP_IP
   .assign  IP_IP_num = cardinality IP_IP_inst_set
   .print "Metaclass Interface Package (IP_IP) has ${IP_IP_num} instances."
   .if(not (IP_IP_num==0))
IP_IP ${IP_IP_num}
   .end if
   .//------------------------------------------------------------------------
   .print " "
   .print "Printing cardinality of metaclass instances in package Invocation."
Invocation
   .select many ACT_BRG_inst_set from instances of ACT_BRG
   .assign  ACT_BRG_num = cardinality ACT_BRG_inst_set
   .print "Metaclass Bridge Invocation (ACT_BRG) has ${ACT_BRG_num} instances."
   .if(not (ACT_BRG_num==0))
ACT_BRG ${ACT_BRG_num}
   .end if
   .select many ACT_FNC_inst_set from instances of ACT_FNC
   .assign  ACT_FNC_num = cardinality ACT_FNC_inst_set
   .print "Metaclass Function Invocation (ACT_FNC) has ${ACT_FNC_num} instances."
   .if(not (ACT_FNC_num==0))
ACT_FNC ${ACT_FNC_num}
   .end if
   .select many ACT_IOP_inst_set from instances of ACT_IOP
   .assign  ACT_IOP_num = cardinality ACT_IOP_inst_set
   .print "Metaclass Interface Operation Invocation (ACT_IOP) has ${ACT_IOP_num} instances."
   .if(not (ACT_IOP_num==0))
ACT_IOP ${ACT_IOP_num}
   .end if
   .select many ACT_TFM_inst_set from instances of ACT_TFM
   .assign  ACT_TFM_num = cardinality ACT_TFM_inst_set
   .print "Metaclass Operation Invocation (ACT_TFM) has ${ACT_TFM_num} instances."
   .if(not (ACT_TFM_num==0))
ACT_TFM ${ACT_TFM_num}
   .end if
   .select many ACT_RET_inst_set from instances of ACT_RET
   .assign  ACT_RET_num = cardinality ACT_RET_inst_set
   .print "Metaclass Return Stmt (ACT_RET) has ${ACT_RET_num} instances."
   .if(not (ACT_RET_num==0))
ACT_RET ${ACT_RET_num}
   .end if
   .select many ACT_SGN_inst_set from instances of ACT_SGN
   .assign  ACT_SGN_num = cardinality ACT_SGN_inst_set
   .print "Metaclass Signal Invocation (ACT_SGN) has ${ACT_SGN_num} instances."
   .if(not (ACT_SGN_num==0))
ACT_SGN ${ACT_SGN_num}
   .end if
   .//------------------------------------------------------------------------
   .print " "
   .print "Printing cardinality of metaclass instances in package Packageable Element."
Packageable Element
   .select many PE_CRS_inst_set from instances of PE_CRS
   .assign  PE_CRS_num = cardinality PE_CRS_inst_set
   .print "Metaclass Component Result Set (PE_CRS) has ${PE_CRS_num} instances."
   .if(not (PE_CRS_num==0))
PE_CRS ${PE_CRS_num}
   .end if
   .select many PE_CVS_inst_set from instances of PE_CVS
   .assign  PE_CVS_num = cardinality PE_CVS_inst_set
   .print "Metaclass Component Visibility (PE_CVS) has ${PE_CVS_num} instances."
   .if(not (PE_CVS_num==0))
PE_CVS ${PE_CVS_num}
   .end if
   .select many PE_VIS_inst_set from instances of PE_VIS
   .assign  PE_VIS_num = cardinality PE_VIS_inst_set
   .print "Metaclass Element Visibility (PE_VIS) has ${PE_VIS_num} instances."
   .if(not (PE_VIS_num==0))
PE_VIS ${PE_VIS_num}
   .end if
   .select many PE_PE_inst_set from instances of PE_PE
   .assign  PE_PE_num = cardinality PE_PE_inst_set
   .print "Metaclass Packageable Element (PE_PE) has ${PE_PE_num} instances."
   .if(not (PE_PE_num==0))
PE_PE ${PE_PE_num}
   .end if
   .select many PE_SRS_inst_set from instances of PE_SRS
   .assign  PE_SRS_num = cardinality PE_SRS_inst_set
   .print "Metaclass Search Result Set (PE_SRS) has ${PE_SRS_num} instances."
   .if(not (PE_SRS_num==0))
PE_SRS ${PE_SRS_num}
   .end if
   .//------------------------------------------------------------------------
   .print " "
   .print "Printing cardinality of metaclass instances in package Relate And Unrelate."
Relate And Unrelate
   .select many ACT_RU_inst_set from instances of ACT_RU
   .assign  ACT_RU_num = cardinality ACT_RU_inst_set
   .print "Metaclass Relate Using (ACT_RU) has ${ACT_RU_num} instances."
   .if(not (ACT_RU_num==0))
ACT_RU ${ACT_RU_num}
   .end if
   .select many ACT_REL_inst_set from instances of ACT_REL
   .assign  ACT_REL_num = cardinality ACT_REL_inst_set
   .print "Metaclass Relate (ACT_REL) has ${ACT_REL_num} instances."
   .if(not (ACT_REL_num==0))
ACT_REL ${ACT_REL_num}
   .end if
   .select many ACT_URU_inst_set from instances of ACT_URU
   .assign  ACT_URU_num = cardinality ACT_URU_inst_set
   .print "Metaclass Unrelate Using (ACT_URU) has ${ACT_URU_num} instances."
   .if(not (ACT_URU_num==0))
ACT_URU ${ACT_URU_num}
   .end if
   .select many ACT_UNR_inst_set from instances of ACT_UNR
   .assign  ACT_UNR_num = cardinality ACT_UNR_inst_set
   .print "Metaclass Unrelate (ACT_UNR) has ${ACT_UNR_num} instances."
   .if(not (ACT_UNR_num==0))
ACT_UNR ${ACT_UNR_num}
   .end if
   .//------------------------------------------------------------------------
   .print " "
   .print "Printing cardinality of metaclass instances in package Selection."
Selection
   .select many ACT_LNK_inst_set from instances of ACT_LNK
   .assign  ACT_LNK_num = cardinality ACT_LNK_inst_set
   .print "Metaclass Chain Link (ACT_LNK) has ${ACT_LNK_num} instances."
   .if(not (ACT_LNK_num==0))
ACT_LNK ${ACT_LNK_num}
   .end if
   .select many ACT_FIW_inst_set from instances of ACT_FIW
   .assign  ACT_FIW_num = cardinality ACT_FIW_inst_set
   .print "Metaclass Select From Instances Where (ACT_FIW) has ${ACT_FIW_num} instances."
   .if(not (ACT_FIW_num==0))
ACT_FIW ${ACT_FIW_num}
   .end if
   .select many ACT_FIO_inst_set from instances of ACT_FIO
   .assign  ACT_FIO_num = cardinality ACT_FIO_inst_set
   .print "Metaclass Select From Instances (ACT_FIO) has ${ACT_FIO_num} instances."
   .if(not (ACT_FIO_num==0))
ACT_FIO ${ACT_FIO_num}
   .end if
   .select many ACT_SR_inst_set from instances of ACT_SR
   .assign  ACT_SR_num = cardinality ACT_SR_inst_set
   .print "Metaclass Select Related By (ACT_SR) has ${ACT_SR_num} instances."
   .if(not (ACT_SR_num==0))
ACT_SR ${ACT_SR_num}
   .end if
   .select many ACT_SRW_inst_set from instances of ACT_SRW
   .assign  ACT_SRW_num = cardinality ACT_SRW_inst_set
   .print "Metaclass Select Related Where (ACT_SRW) has ${ACT_SRW_num} instances."
   .if(not (ACT_SRW_num==0))
ACT_SRW ${ACT_SRW_num}
   .end if
   .select many ACT_SEL_inst_set from instances of ACT_SEL
   .assign  ACT_SEL_num = cardinality ACT_SEL_inst_set
   .print "Metaclass Select (ACT_SEL) has ${ACT_SEL_num} instances."
   .if(not (ACT_SEL_num==0))
ACT_SEL ${ACT_SEL_num}
   .end if
   .//------------------------------------------------------------------------
   .print " "
   .print "Printing cardinality of metaclass instances in package State Machine."
State Machine
   .select many SM_AH_inst_set from instances of SM_AH
   .assign  SM_AH_num = cardinality SM_AH_inst_set
   .print "Metaclass Action Home (SM_AH) has ${SM_AH_num} instances."
   .if(not (SM_AH_num==0))
SM_AH ${SM_AH_num}
   .end if
   .select many SM_ACT_inst_set from instances of SM_ACT
   .assign  SM_ACT_num = cardinality SM_ACT_inst_set
   .print "Metaclass Action (SM_ACT) has ${SM_ACT_num} instances."
   .if(not (SM_ACT_num==0))
SM_ACT ${SM_ACT_num}
   .end if
   .select many SM_CH_inst_set from instances of SM_CH
   .assign  SM_CH_num = cardinality SM_CH_inst_set
   .print "Metaclass Cant Happen (SM_CH) has ${SM_CH_num} instances."
   .if(not (SM_CH_num==0))
SM_CH ${SM_CH_num}
   .end if
   .select many SM_ASM_inst_set from instances of SM_ASM
   .assign  SM_ASM_num = cardinality SM_ASM_inst_set
   .print "Metaclass Class State Machine (SM_ASM) has ${SM_ASM_num} instances."
   .if(not (SM_ASM_num==0))
SM_ASM ${SM_ASM_num}
   .end if
   .select many SM_CRTXN_inst_set from instances of SM_CRTXN
   .assign  SM_CRTXN_num = cardinality SM_CRTXN_inst_set
   .print "Metaclass Creation Transition (SM_CRTXN) has ${SM_CRTXN_num} instances."
   .if(not (SM_CRTXN_num==0))
SM_CRTXN ${SM_CRTXN_num}
   .end if
   .select many SM_EIGN_inst_set from instances of SM_EIGN
   .assign  SM_EIGN_num = cardinality SM_EIGN_inst_set
   .print "Metaclass Event Ignored (SM_EIGN) has ${SM_EIGN_num} instances."
   .if(not (SM_EIGN_num==0))
SM_EIGN ${SM_EIGN_num}
   .end if
   .select many SM_SUPDT_inst_set from instances of SM_SUPDT
   .assign  SM_SUPDT_num = cardinality SM_SUPDT_inst_set
   .print "Metaclass Event Supplemental Data (SM_SUPDT) has ${SM_SUPDT_num} instances."
   .if(not (SM_SUPDT_num==0))
SM_SUPDT ${SM_SUPDT_num}
   .end if
   .select many SM_ISM_inst_set from instances of SM_ISM
   .assign  SM_ISM_num = cardinality SM_ISM_inst_set
   .print "Metaclass Instance State Machine (SM_ISM) has ${SM_ISM_num} instances."
   .if(not (SM_ISM_num==0))
SM_ISM ${SM_ISM_num}
   .end if
   .select many SM_LEVT_inst_set from instances of SM_LEVT
   .assign  SM_LEVT_num = cardinality SM_LEVT_inst_set
   .print "Metaclass Local Event (SM_LEVT) has ${SM_LEVT_num} instances."
   .if(not (SM_LEVT_num==0))
SM_LEVT ${SM_LEVT_num}
   .end if
   .select many SM_MEAH_inst_set from instances of SM_MEAH
   .assign  SM_MEAH_num = cardinality SM_MEAH_inst_set
   .print "Metaclass Mealy Action Home (SM_MEAH) has ${SM_MEAH_num} instances."
   .if(not (SM_MEAH_num==0))
SM_MEAH ${SM_MEAH_num}
   .end if
   .select many SM_MEALY_inst_set from instances of SM_MEALY
   .assign  SM_MEALY_num = cardinality SM_MEALY_inst_set
   .print "Metaclass Mealy State Machine (SM_MEALY) has ${SM_MEALY_num} instances."
   .if(not (SM_MEALY_num==0))
SM_MEALY ${SM_MEALY_num}
   .end if
   .select many SM_MOAH_inst_set from instances of SM_MOAH
   .assign  SM_MOAH_num = cardinality SM_MOAH_inst_set
   .print "Metaclass Moore Action Home (SM_MOAH) has ${SM_MOAH_num} instances."
   .if(not (SM_MOAH_num==0))
SM_MOAH ${SM_MOAH_num}
   .end if
   .select many SM_MOORE_inst_set from instances of SM_MOORE
   .assign  SM_MOORE_num = cardinality SM_MOORE_inst_set
   .print "Metaclass Moore State Machine (SM_MOORE) has ${SM_MOORE_num} instances."
   .if(not (SM_MOORE_num==0))
SM_MOORE ${SM_MOORE_num}
   .end if
   .select many SM_NSTXN_inst_set from instances of SM_NSTXN
   .assign  SM_NSTXN_num = cardinality SM_NSTXN_inst_set
   .print "Metaclass New State Transition (SM_NSTXN) has ${SM_NSTXN_num} instances."
   .if(not (SM_NSTXN_num==0))
SM_NSTXN ${SM_NSTXN_num}
   .end if
   .select many SM_NETXN_inst_set from instances of SM_NETXN
   .assign  SM_NETXN_num = cardinality SM_NETXN_inst_set
   .print "Metaclass No Event Transition (SM_NETXN) has ${SM_NETXN_num} instances."
   .if(not (SM_NETXN_num==0))
SM_NETXN ${SM_NETXN_num}
   .end if
   .select many SM_NLEVT_inst_set from instances of SM_NLEVT
   .assign  SM_NLEVT_num = cardinality SM_NLEVT_inst_set
   .print "Metaclass Non Local Event (SM_NLEVT) has ${SM_NLEVT_num} instances."
   .if(not (SM_NLEVT_num==0))
SM_NLEVT ${SM_NLEVT_num}
   .end if
   .select many SM_PEVT_inst_set from instances of SM_PEVT
   .assign  SM_PEVT_num = cardinality SM_PEVT_inst_set
   .print "Metaclass Polymorphic Event (SM_PEVT) has ${SM_PEVT_num} instances."
   .if(not (SM_PEVT_num==0))
SM_PEVT ${SM_PEVT_num}
   .end if
   .select many SM_SEVT_inst_set from instances of SM_SEVT
   .assign  SM_SEVT_num = cardinality SM_SEVT_inst_set
   .print "Metaclass SEM Event (SM_SEVT) has ${SM_SEVT_num} instances."
   .if(not (SM_SEVT_num==0))
SM_SEVT ${SM_SEVT_num}
   .end if
   .select many SM_SGEVT_inst_set from instances of SM_SGEVT
   .assign  SM_SGEVT_num = cardinality SM_SGEVT_inst_set
   .print "Metaclass Signal Event (SM_SGEVT) has ${SM_SGEVT_num} instances."
   .if(not (SM_SGEVT_num==0))
SM_SGEVT ${SM_SGEVT_num}
   .end if
   .select many SM_SEME_inst_set from instances of SM_SEME
   .assign  SM_SEME_num = cardinality SM_SEME_inst_set
   .print "Metaclass State Event Matrix Entry (SM_SEME) has ${SM_SEME_num} instances."
   .if(not (SM_SEME_num==0))
SM_SEME ${SM_SEME_num}
   .end if
   .select many SM_EVTDI_inst_set from instances of SM_EVTDI
   .assign  SM_EVTDI_num = cardinality SM_EVTDI_inst_set
   .print "Metaclass State Machine Event Data Item (SM_EVTDI) has ${SM_EVTDI_num} instances."
   .if(not (SM_EVTDI_num==0))
SM_EVTDI ${SM_EVTDI_num}
   .end if
   .select many SM_EVT_inst_set from instances of SM_EVT
   .assign  SM_EVT_num = cardinality SM_EVT_inst_set
   .print "Metaclass State Machine Event (SM_EVT) has ${SM_EVT_num} instances."
   .if(not (SM_EVT_num==0))
SM_EVT ${SM_EVT_num}
   .end if
   .select many SM_STATE_inst_set from instances of SM_STATE
   .assign  SM_STATE_num = cardinality SM_STATE_inst_set
   .print "Metaclass State Machine State (SM_STATE) has ${SM_STATE_num} instances."
   .if(not (SM_STATE_num==0))
SM_STATE ${SM_STATE_num}
   .end if
   .select many SM_SM_inst_set from instances of SM_SM
   .assign  SM_SM_num = cardinality SM_SM_inst_set
   .print "Metaclass State Machine (SM_SM) has ${SM_SM_num} instances."
   .if(not (SM_SM_num==0))
SM_SM ${SM_SM_num}
   .end if
   .select many SM_SDI_inst_set from instances of SM_SDI
   .assign  SM_SDI_num = cardinality SM_SDI_inst_set
   .print "Metaclass Supplemental Data Items (SM_SDI) has ${SM_SDI_num} instances."
   .if(not (SM_SDI_num==0))
SM_SDI ${SM_SDI_num}
   .end if
   .select many SM_TAH_inst_set from instances of SM_TAH
   .assign  SM_TAH_num = cardinality SM_TAH_inst_set
   .print "Metaclass Transition Action Home (SM_TAH) has ${SM_TAH_num} instances."
   .if(not (SM_TAH_num==0))
SM_TAH ${SM_TAH_num}
   .end if
   .select many SM_TXN_inst_set from instances of SM_TXN
   .assign  SM_TXN_num = cardinality SM_TXN_inst_set
   .print "Metaclass Transition (SM_TXN) has ${SM_TXN_num} instances."
   .if(not (SM_TXN_num==0))
SM_TXN ${SM_TXN_num}
   .end if
   .//------------------------------------------------------------------------
   .print " "
   .print "Printing cardinality of metaclass instances in package Subsystem."
Subsystem
   .select many O_REF_inst_set from instances of O_REF
   .assign  O_REF_num = cardinality O_REF_inst_set
   .print "Metaclass Attribute Reference in Class (O_REF) has ${O_REF_num} instances."
   .if(not (O_REF_num==0))
O_REF ${O_REF_num}
   .end if
   .select many O_ATTR_inst_set from instances of O_ATTR
   .assign  O_ATTR_num = cardinality O_ATTR_inst_set
   .print "Metaclass Attribute (O_ATTR) has ${O_ATTR_num} instances."
   .if(not (O_ATTR_num==0))
O_ATTR ${O_ATTR_num}
   .end if
   .select many O_BATTR_inst_set from instances of O_BATTR
   .assign  O_BATTR_num = cardinality O_BATTR_inst_set
   .print "Metaclass Base Attribute (O_BATTR) has ${O_BATTR_num} instances."
   .if(not (O_BATTR_num==0))
O_BATTR ${O_BATTR_num}
   .end if
   .select many O_OIDA_inst_set from instances of O_OIDA
   .assign  O_OIDA_num = cardinality O_OIDA_inst_set
   .print "Metaclass Class Identifier Attribute (O_OIDA) has ${O_OIDA_num} instances."
   .if(not (O_OIDA_num==0))
O_OIDA ${O_OIDA_num}
   .end if
   .select many O_ID_inst_set from instances of O_ID
   .assign  O_ID_num = cardinality O_ID_inst_set
   .print "Metaclass Class Identifier (O_ID) has ${O_ID_num} instances."
   .if(not (O_ID_num==0))
O_ID ${O_ID_num}
   .end if
   .select many O_DBATTR_inst_set from instances of O_DBATTR
   .assign  O_DBATTR_num = cardinality O_DBATTR_inst_set
   .print "Metaclass Derived Base Attribute (O_DBATTR) has ${O_DBATTR_num} instances."
   .if(not (O_DBATTR_num==0))
O_DBATTR ${O_DBATTR_num}
   .end if
   .select many O_IOBJ_inst_set from instances of O_IOBJ
   .assign  O_IOBJ_num = cardinality O_IOBJ_inst_set
   .print "Metaclass Imported Class (O_IOBJ) has ${O_IOBJ_num} instances."
   .if(not (O_IOBJ_num==0))
O_IOBJ ${O_IOBJ_num}
   .end if
   .select many O_OBJ_inst_set from instances of O_OBJ
   .assign  O_OBJ_num = cardinality O_OBJ_inst_set
   .print "Metaclass Model Class (O_OBJ) has ${O_OBJ_num} instances."
   .if(not (O_OBJ_num==0))
O_OBJ ${O_OBJ_num}
   .end if
   .select many O_NBATTR_inst_set from instances of O_NBATTR
   .assign  O_NBATTR_num = cardinality O_NBATTR_inst_set
   .print "Metaclass New Base Attribute (O_NBATTR) has ${O_NBATTR_num} instances."
   .if(not (O_NBATTR_num==0))
O_NBATTR ${O_NBATTR_num}
   .end if
   .select many O_TPARM_inst_set from instances of O_TPARM
   .assign  O_TPARM_num = cardinality O_TPARM_inst_set
   .print "Metaclass Operation Parameter (O_TPARM) has ${O_TPARM_num} instances."
   .if(not (O_TPARM_num==0))
O_TPARM ${O_TPARM_num}
   .end if
   .select many O_TFR_inst_set from instances of O_TFR
   .assign  O_TFR_num = cardinality O_TFR_inst_set
   .print "Metaclass Operation (O_TFR) has ${O_TFR_num} instances."
   .if(not (O_TFR_num==0))
O_TFR ${O_TFR_num}
   .end if
   .select many O_RAVR_inst_set from instances of O_RAVR
   .assign  O_RAVR_num = cardinality O_RAVR_inst_set
   .print "Metaclass Referential Attribute Visited Recorder (O_RAVR) has ${O_RAVR_num} instances."
   .if(not (O_RAVR_num==0))
O_RAVR ${O_RAVR_num}
   .end if
   .select many O_RATTR_inst_set from instances of O_RATTR
   .assign  O_RATTR_num = cardinality O_RATTR_inst_set
   .print "Metaclass Referential Attribute (O_RATTR) has ${O_RATTR_num} instances."
   .if(not (O_RATTR_num==0))
O_RATTR ${O_RATTR_num}
   .end if
   .select many O_RTIDA_inst_set from instances of O_RTIDA
   .assign  O_RTIDA_num = cardinality O_RTIDA_inst_set
   .print "Metaclass Referred To Identifier Attribute (O_RTIDA) has ${O_RTIDA_num} instances."
   .if(not (O_RTIDA_num==0))
O_RTIDA ${O_RTIDA_num}
   .end if
   .//------------------------------------------------------------------------
   .print " "
   .print "Printing cardinality of metaclass instances in package System Level Datatypes."
System Level Datatypes
   .select many SLD_SCINP_inst_set from instances of SLD_SCINP
   .assign  SLD_SCINP_num = cardinality SLD_SCINP_inst_set
   .print "Metaclass System Constant in Package (SLD_SCINP) has ${SLD_SCINP_num} instances."
   .if(not (SLD_SCINP_num==0))
SLD_SCINP ${SLD_SCINP_num}
   .end if
   .select many SLD_SDP_inst_set from instances of SLD_SDP
   .assign  SLD_SDP_num = cardinality SLD_SDP_inst_set
   .print "Metaclass System Datatype Package (SLD_SDP) has ${SLD_SDP_num} instances."
   .if(not (SLD_SDP_num==0))
SLD_SDP ${SLD_SDP_num}
   .end if
   .select many SLD_SDINP_inst_set from instances of SLD_SDINP
   .assign  SLD_SDINP_num = cardinality SLD_SDINP_inst_set
   .print "Metaclass System Datatype in Package (SLD_SDINP) has ${SLD_SDINP_num} instances."
   .if(not (SLD_SDINP_num==0))
SLD_SDINP ${SLD_SDINP_num}
   .end if
   .//------------------------------------------------------------------------
   .print " "
   .print "Printing cardinality of metaclass instances in package Value."
Value
   .select many V_PAR_inst_set from instances of V_PAR
   .assign  V_PAR_num = cardinality V_PAR_inst_set
   .print "Metaclass Actual Parameter (V_PAR) has ${V_PAR_num} instances."
   .if(not (V_PAR_num==0))
V_PAR ${V_PAR_num}
   .end if
   .select many V_AER_inst_set from instances of V_AER
   .assign  V_AER_num = cardinality V_AER_inst_set
   .print "Metaclass Array Element Reference (V_AER) has ${V_AER_num} instances."
   .if(not (V_AER_num==0))
V_AER ${V_AER_num}
   .end if
   .select many V_ALV_inst_set from instances of V_ALV
   .assign  V_ALV_num = cardinality V_ALV_inst_set
   .print "Metaclass Array Length Value (V_ALV) has ${V_ALV_num} instances."
   .if(not (V_ALV_num==0))
V_ALV ${V_ALV_num}
   .end if
   .select many V_AVL_inst_set from instances of V_AVL
   .assign  V_AVL_num = cardinality V_AVL_inst_set
   .print "Metaclass Attribute Value Reference (V_AVL) has ${V_AVL_num} instances."
   .if(not (V_AVL_num==0))
V_AVL ${V_AVL_num}
   .end if
   .select many V_BIN_inst_set from instances of V_BIN
   .assign  V_BIN_num = cardinality V_BIN_inst_set
   .print "Metaclass Binary Operation (V_BIN) has ${V_BIN_num} instances."
   .if(not (V_BIN_num==0))
V_BIN ${V_BIN_num}
   .end if
   .select many V_BRV_inst_set from instances of V_BRV
   .assign  V_BRV_num = cardinality V_BRV_inst_set
   .print "Metaclass Bridge Value (V_BRV) has ${V_BRV_num} instances."
   .if(not (V_BRV_num==0))
V_BRV ${V_BRV_num}
   .end if
   .select many V_EDV_inst_set from instances of V_EDV
   .assign  V_EDV_num = cardinality V_EDV_inst_set
   .print "Metaclass Event Datum Value (V_EDV) has ${V_EDV_num} instances."
   .if(not (V_EDV_num==0))
V_EDV ${V_EDV_num}
   .end if
   .select many V_EPR_inst_set from instances of V_EPR
   .assign  V_EPR_num = cardinality V_EPR_inst_set
   .print "Metaclass Event Parameter Reference (V_EPR) has ${V_EPR_num} instances."
   .if(not (V_EPR_num==0))
V_EPR ${V_EPR_num}
   .end if
   .select many V_FNV_inst_set from instances of V_FNV
   .assign  V_FNV_num = cardinality V_FNV_inst_set
   .print "Metaclass Function Value (V_FNV) has ${V_FNV_num} instances."
   .if(not (V_FNV_num==0))
V_FNV ${V_FNV_num}
   .end if
   .select many V_INT_inst_set from instances of V_INT
   .assign  V_INT_num = cardinality V_INT_inst_set
   .print "Metaclass Instance Handle (V_INT) has ${V_INT_num} instances."
   .if(not (V_INT_num==0))
V_INT ${V_INT_num}
   .end if
   .select many V_IRF_inst_set from instances of V_IRF
   .assign  V_IRF_num = cardinality V_IRF_inst_set
   .print "Metaclass Instance Reference (V_IRF) has ${V_IRF_num} instances."
   .if(not (V_IRF_num==0))
V_IRF ${V_IRF_num}
   .end if
   .select many V_ISR_inst_set from instances of V_ISR
   .assign  V_ISR_num = cardinality V_ISR_inst_set
   .print "Metaclass Instance Set Reference (V_ISR) has ${V_ISR_num} instances."
   .if(not (V_ISR_num==0))
V_ISR ${V_ISR_num}
   .end if
   .select many V_INS_inst_set from instances of V_INS
   .assign  V_INS_num = cardinality V_INS_inst_set
   .print "Metaclass Instance Set (V_INS) has ${V_INS_num} instances."
   .if(not (V_INS_num==0))
V_INS ${V_INS_num}
   .end if
   .select many V_LBO_inst_set from instances of V_LBO
   .assign  V_LBO_num = cardinality V_LBO_inst_set
   .print "Metaclass Literal Boolean (V_LBO) has ${V_LBO_num} instances."
   .if(not (V_LBO_num==0))
V_LBO ${V_LBO_num}
   .end if
   .select many V_LEN_inst_set from instances of V_LEN
   .assign  V_LEN_num = cardinality V_LEN_inst_set
   .print "Metaclass Literal Enumerator (V_LEN) has ${V_LEN_num} instances."
   .if(not (V_LEN_num==0))
V_LEN ${V_LEN_num}
   .end if
   .select many V_LIN_inst_set from instances of V_LIN
   .assign  V_LIN_num = cardinality V_LIN_inst_set
   .print "Metaclass Literal Integer (V_LIN) has ${V_LIN_num} instances."
   .if(not (V_LIN_num==0))
V_LIN ${V_LIN_num}
   .end if
   .select many V_LRL_inst_set from instances of V_LRL
   .assign  V_LRL_num = cardinality V_LRL_inst_set
   .print "Metaclass Literal Real (V_LRL) has ${V_LRL_num} instances."
   .if(not (V_LRL_num==0))
V_LRL ${V_LRL_num}
   .end if
   .select many V_LST_inst_set from instances of V_LST
   .assign  V_LST_num = cardinality V_LST_inst_set
   .print "Metaclass Literal String (V_LST) has ${V_LST_num} instances."
   .if(not (V_LST_num==0))
V_LST ${V_LST_num}
   .end if
   .select many V_MVL_inst_set from instances of V_MVL
   .assign  V_MVL_num = cardinality V_MVL_inst_set
   .print "Metaclass Member Value Reference (V_MVL) has ${V_MVL_num} instances."
   .if(not (V_MVL_num==0))
V_MVL ${V_MVL_num}
   .end if
   .select many V_MSV_inst_set from instances of V_MSV
   .assign  V_MSV_num = cardinality V_MSV_inst_set
   .print "Metaclass Message Value (V_MSV) has ${V_MSV_num} instances."
   .if(not (V_MSV_num==0))
V_MSV ${V_MSV_num}
   .end if
   .select many V_TRV_inst_set from instances of V_TRV
   .assign  V_TRV_num = cardinality V_TRV_inst_set
   .print "Metaclass Operation Value (V_TRV) has ${V_TRV_num} instances."
   .if(not (V_TRV_num==0))
V_TRV ${V_TRV_num}
   .end if
   .select many V_PVL_inst_set from instances of V_PVL
   .assign  V_PVL_num = cardinality V_PVL_inst_set
   .print "Metaclass Parameter Value (V_PVL) has ${V_PVL_num} instances."
   .if(not (V_PVL_num==0))
V_PVL ${V_PVL_num}
   .end if
   .select many V_SLR_inst_set from instances of V_SLR
   .assign  V_SLR_num = cardinality V_SLR_inst_set
   .print "Metaclass Selected Reference (V_SLR) has ${V_SLR_num} instances."
   .if(not (V_SLR_num==0))
V_SLR ${V_SLR_num}
   .end if
   .select many V_SCV_inst_set from instances of V_SCV
   .assign  V_SCV_num = cardinality V_SCV_inst_set
   .print "Metaclass Symbolic Constant Value (V_SCV) has ${V_SCV_num} instances."
   .if(not (V_SCV_num==0))
V_SCV ${V_SCV_num}
   .end if
   .select many V_TVL_inst_set from instances of V_TVL
   .assign  V_TVL_num = cardinality V_TVL_inst_set
   .print "Metaclass Transient Value Reference (V_TVL) has ${V_TVL_num} instances."
   .if(not (V_TVL_num==0))
V_TVL ${V_TVL_num}
   .end if
   .select many V_TRN_inst_set from instances of V_TRN
   .assign  V_TRN_num = cardinality V_TRN_inst_set
   .print "Metaclass Transient Var (V_TRN) has ${V_TRN_num} instances."
   .if(not (V_TRN_num==0))
V_TRN ${V_TRN_num}
   .end if
   .select many V_UNY_inst_set from instances of V_UNY
   .assign  V_UNY_num = cardinality V_UNY_inst_set
   .print "Metaclass Unary Operation (V_UNY) has ${V_UNY_num} instances."
   .if(not (V_UNY_num==0))
V_UNY ${V_UNY_num}
   .end if
   .select many V_VAL_inst_set from instances of V_VAL
   .assign  V_VAL_num = cardinality V_VAL_inst_set
   .print "Metaclass Value (V_VAL) has ${V_VAL_num} instances."
   .if(not (V_VAL_num==0))
V_VAL ${V_VAL_num}
   .end if
   .select many V_LOC_inst_set from instances of V_LOC
   .assign  V_LOC_num = cardinality V_LOC_inst_set
   .print "Metaclass Variable Location (V_LOC) has ${V_LOC_num} instances."
   .if(not (V_LOC_num==0))
V_LOC ${V_LOC_num}
   .end if
   .select many V_VAR_inst_set from instances of V_VAR
   .assign  V_VAR_num = cardinality V_VAR_inst_set
   .print "Metaclass Variable (V_VAR) has ${V_VAR_num} instances."
   .if(not (V_VAR_num==0))
V_VAR ${V_VAR_num}
   .end if
   .//------------------------------------------------------------------------
   .print " "
   .print "Printing cardinality of metaclass instances in package Wiring."
Wiring
   .select many S_AW_inst_set from instances of S_AW
   .assign  S_AW_num = cardinality S_AW_inst_set
   .print "Metaclass Automatic Wiring (S_AW) has ${S_AW_num} instances."
   .if(not (S_AW_num==0))
S_AW ${S_AW_num}
   .end if
   .//------------------------------------------------------------------------
   .print " "
   .//select any sys from instances of S_SYS where (selected.Sys_ID==268435457)
   .select any sys from instances of S_SYS
   .print "Sys.ID = ${sys.Sys_ID} (should be 268435457)"
   .emit to file "C:/Temp/${sys.Name}_MetaclassCardinality.csv"
