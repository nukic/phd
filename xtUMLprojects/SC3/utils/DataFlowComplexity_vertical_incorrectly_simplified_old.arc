.//------------------------------------------------------------------------------------
.//------------------------------------------------------------------------------------
.// FUNCTIONS
.//------------------------------------------------------------------------------------
.//------------------------------------------------------------------------------------
.function find_classes_in_component
.param inst_ref comp_inst
	.select many attr_result from instances of O_OBJ where false
	.select many root_pkg_set related by comp_inst->PE_PE[R8003]->EP_PKG[R8001]
	.for each root_pkg in root_pkg_set
		.invoke cips = find_classes_recursively(root_pkg)
		.assign class_in_pkg_set = cips.result
		.assign attr_result = attr_result|class_in_pkg_set
	.end for
.end function
.//
.function find_classes_recursively
.param inst_ref pkg_inst
	.select many class_set related by pkg_inst->PE_PE[R8000]->O_OBJ[R8001]
	.select many internal_pkg_set related by pkg_inst->PE_PE[R8000]->EP_PKG[R8001]
	.for each internal_pkg in internal_pkg_set
		.invoke ns = find_classes_recursively(internal_pkg)
		.assign nested_classes = ns.result
		.assign class_set = class_set|nested_classes
	.end for
	.assign attr_result = class_set
.end function
.//
.function find_relations_in_component
.param inst_ref comp_inst
	.select many attr_result from instances of R_REL where false
	.select many root_pkg_set related by comp_inst->PE_PE[R8003]->EP_PKG[R8001]
	.for each root_pkg in root_pkg_set
		.invoke rips = find_relations_recursively(root_pkg)
		.assign relation_in_pkg_set = rips.result
		.assign attr_result = attr_result|relation_in_pkg_set
	.end for
.end function
.//
.function find_relations_recursively
.param inst_ref pkg_inst
	.select many relation_set related by pkg_inst->PE_PE[R8000]->R_REL[R8001]
	.select many internal_pkg_set related by pkg_inst->PE_PE[R8000]->EP_PKG[R8001]
	.for each internal_pkg in internal_pkg_set
		.invoke nr = find_relations_recursively(internal_pkg)
		.assign nested_relations = nr.result
		.assign relation_set = relation_set|nested_relations
	.end for
	.assign attr_result = relation_set
.end function
.//
.function find_functions_recursively
.param inst_ref pkg_inst
	.select many fun_set related by pkg_inst->PE_PE[R8000]->S_SYNC[R8001]
	.select many internal_pkg_set related by pkg_inst->PE_PE[R8000]->EP_PKG[R8001]
	.for each internal_pkg in internal_pkg_set
		.invoke nf = find_functions_recursively(internal_pkg)
		.assign nested_funs = nr.result
		.assign fun_set = fun_set|nested_funs
	.end for
	.assign attr_result = fun_set
.end function
.//
.function find_ees_recursively
.param inst_ref pkg_inst
	.select many ee_set related by pkg_inst->PE_PE[R8000]->S_EE[R8001]
	.select many internal_pkg_set related by pkg_inst->PE_PE[R8000]->EP_PKG[R8001]
	.for each internal_pkg in internal_pkg_set
		.invoke nee = find_ees_recursively(internal_pkg)
		.assign nested_ees = nee.result
		.assign ee_set = ee_set|nested_ees
	.end for
	.assign attr_result = ee_set
.end function
.//
.function find_all_bodies_within_component
.param inst_ref comp_inst
	.select many body_set from instances of ACT_ACT where false
	.//Provided port bodies
	.select many provided_port_signal_body_set related by comp_inst->C_PO[R4010]->C_IR[R4016]->C_P[R4009]->SPR_PEP[R4501]->SPR_PS[R4503]->ACT_PSB[R686]->ACT_ACT[R698]
	.assign psb_num =  cardinality provided_port_signal_body_set
	.//print "Component ${comp_inst.Name} has provided signal bodies: ${psb_num}"
	.assign body_set = body_set|provided_port_signal_body_set
	.select many provided_port_operation_body_set related by comp_inst->C_PO[R4010]->C_IR[R4016]->C_P[R4009]->SPR_PEP[R4501]->SPR_PO[R4503]->ACT_POB[R687]->ACT_ACT[R698]
	.assign pob_num =  cardinality provided_port_operation_body_set
	.//print "Component ${comp_inst.Name} has provided operation bodies: ${pob_num}"
	.assign body_set = body_set|provided_port_operation_body_set
	.//Required port bodies
	.select many required_port_signal_body_set related by comp_inst->C_PO[R4010]->C_IR[R4016]->C_R[R4009]->SPR_REP[R4500]->SPR_RS[R4502]->ACT_RSB[R684]->ACT_ACT[R698]
	.assign rsb_num =  cardinality required_port_signal_body_set
	.//print "Component ${comp_inst.Name} has required signal bodies: ${rsb_num}"
	.assign body_set = body_set|required_port_signal_body_set
	.select many required_port_operation_body_set related by comp_inst->C_PO[R4010]->C_IR[R4016]->C_R[R4009]->SPR_REP[R4500]->SPR_RO[R4502]->ACT_ROB[R685]->ACT_ACT[R698]
	.assign rob_num =  cardinality required_port_operation_body_set
	.//print "Component ${comp_inst.Name} has required operation bodies: ${rob_num}"
	.assign body_set = body_set|required_port_operation_body_set
	.//In function body
	.select many root_pkg_set related by comp_inst->PE_PE[R8003]->EP_PKG[R8001]
	.select many fun_set from instances of S_SYNC where false
	.for each root_pkg in root_pkg_set
		.invoke fs = find_functions_recursively(root_pkg)
		.assign fun_set = fun_set|fs.result
	.end for
	.select many fun_body_set related by fun_set->ACT_FNB[R695]->ACT_ACT[R698]
	.assign fb_num =  cardinality fun_body_set
	.//print "Component ${comp_inst.Name} has function bodies: ${fb_num}"
	.assign body_set = body_set|fun_body_set
	.//Class operation bodies
	.select many cls_set from instances of O_OBJ where false
	.for each root_pkg in root_pkg_set
		.invoke fcr = find_classes_recursively(root_pkg)
		.assign cls_set = cls_set|fcr.result
	.end for
	.select many op_body_set related by cls_set->O_TFR[R115]->ACT_OPB[R696]->ACT_ACT[R698]
	.assign ob_num =  cardinality op_body_set
	.//print "Component ${comp_inst.Name} has class operation bodies: ${ob_num}"
	.assign body_set = body_set|op_body_set
	.//Derr attr bodies
	.select many derattr_body_set related by cls_set->O_ATTR[R102]->O_BATTR[R106]->O_DBATTR[R107]->ACT_DAB[R693]->ACT_ACT[R698]
	.assign dab_num =  cardinality derattr_body_set
	.//print "Component ${comp_inst.Name} has derived attributes bodies: ${dab_num}"
	.assign body_set = body_set|derattr_body_set
	.//Bridge bodies
	.select many ee_set from instances of S_EE where false
	.for each root_pkg in root_pkg_set
		.invoke fees = find_ees_recursively(root_pkg)
		.assign ee_set = ee_set|fees.result
	.end for
	.select many bridge_body_set related by ee_set->S_BRG[R19]->ACT_BRB[R697]->ACT_ACT[R698]
	.assign bb_num =  cardinality bridge_body_set
	.//print "Component ${comp_inst.Name} has bridge bodies: ${bb_num}"
	.assign body_set = body_set|bridge_body_set
	.//State bodies
	.select many state_body_set related by cls_set->SM_ISM[R518]->SM_SM[R517]->SM_ACT[R515]->ACT_SAB[R691]->ACT_ACT[R698]
	.assign sb_num =  cardinality state_body_set
	.//print "Component ${comp_inst.Name} has state bodies: ${sb_num}"
	.assign body_set = body_set|state_body_set
	.//Transition bodies
	.select many txn_body_set related by cls_set->SM_ISM[R518]->SM_SM[R517]->SM_ACT[R515]->ACT_TAB[R688]->ACT_ACT[R698]
	.assign tb_num =  cardinality txn_body_set
	.//print "Component ${comp_inst.Name} has transition bodies: ${tb_num}"
	.assign body_set = body_set|txn_body_set
	.assign body_set_num =  cardinality body_set
	.assign attr_result = body_set	
.end function
.//
.function calculate_all_comp_port_data_flow_complexity
.param inst_ref comp
	.assign attr_result = 0
	.select many port_set related by comp->C_PO[R4010]
	.for each port in port_set
		.//IFDirectionType::ClientServer = 0
		.//IFDirectionType::ServerClient = 1
		.select many in_sig_set related by port->C_IR[R4016]->C_I[R4012]->C_EP[R4003]->C_AS[R4004] where selected.Direction==0
		.for each in_sig in in_sig_set 
			.select many param_set related by in_sig->C_EP[R4004]->C_PP[R4006]
			.assign attr_result = attr_result + (cardinality param_set)
		.end for 
	.end for
.end function
.//
.function calculate_all_comp_class_data_flow_complexity
.param inst_ref comp
	.assign attr_result = 0	
	.invoke ac = find_classes_in_component(comp)
	.assign class_set = ac.result
	.for each class in class_set
		.select many op_set related by class->O_TFR[R115]
		.for each op in op_set
			.select many param_set related by op->O_TPARM[R117]
			.assign op_param_num = cardinality param_set
			.assign attr_result = attr_result + op_param_num
			.//print "Operation ${op.Name} in class ${class.Name} has ${op_param_num} parameters!"
		.end for
	.end for
.end function
.//
.function calculate_all_comp_state_machine_data_flow_complexity
.param inst_ref comp
	.assign attr_result = 0	
	.invoke ab = find_all_bodies_within_component(comp)
	.assign all_bodies = ab.result
	.select many non_empty_bodies related by all_bodies->ACT_BLK[R601]->ACT_SMT[R602]->ACT_BLK[R602]->ACT_ACT[R601]
	.//
	.select many non_empty_state_set related by non_empty_bodies->ACT_SAB[R698]->SM_ACT[R691]->SM_AH[R514]->SM_MOAH[R513]->SM_STATE[R511]
	.for each non_empty_state in non_empty_state_set
		.select many recvd_evt_set related by non_empty_state->SM_SEME[R503]->SM_SEVT[R503]->SM_EVT[R525]
		.for each recvd_evt in recvd_evt_set
			.select many evdti_set related by recvd_evt->SM_EVTDI[R532]
			.assign evtdi_num = cardinality evdti_set
			.print "State ${non_empty_state.Name} receives ${evtdi_num} event data items with event ${recvd_evt.Mning}"
			.assign attr_result = attr_result + evtdi_num
		.end for
	.end for
	.//
	.select many non_empty_txn_set related by non_empty_bodies->ACT_TAB[R698]->SM_ACT[R688]->SM_AH[R514]->SM_TAH[R513]->SM_TXN[R530]
	.for each non_empty_txn in non_empty_txn_set
		.select many txn_evtdi_set related by non_empty_txn->SM_NSTXN[R507]->SM_SEME[R504]->SM_SEVT[R503]->SM_EVT[R525]->SM_EVTDI[R532]
		.assign txn_evtdi_num = cardinality txn_evtdi_set
		.print "Transition behavior parametrized with ${txn_evtdi_num} event data items."
		.assign attr_result = attr_result + txn_evtdi_num
	.end for
.end function
.//
.function calculate_all_comp_body_data_flow_complexity
.param inst_ref comp
	.assign attr_result = 0	
	.invoke ab = find_all_bodies_within_component(comp)
	.assign all_bodies = ab.result
	.select many non_empty_body_set related by all_bodies->ACT_BLK[R601]->ACT_SMT[R602]->ACT_BLK[R602]->ACT_ACT[R601]
	.for each non_empty_body in non_empty_body_set
		.//Assignment
		.select many assign_stmt_set related by non_empty_body->ACT_BLK[R601]->ACT_SMT[R602]->ACT_AI[R603]
		.assign assign_stmt_num =  cardinality assign_stmt_set
		.assign attr_result =attr_result+assign_stmt_num	
		.//print "Body ${non_empty_body.Label} has ${assign_stmt_num} assign statements."
		.//Create
		.select many create_stmt_set related by non_empty_body->ACT_BLK[R601]->ACT_SMT[R602]->ACT_CR[R603]
		.assign create_stmt_num =  cardinality create_stmt_set
		.assign attr_result =attr_result+create_stmt_num	
		.//print "Body ${non_empty_body.Label} has ${create_stmt_num} create statements."
		.//select
		.select many select_stmt_set related by non_empty_body->ACT_BLK[R601]->ACT_SMT[R602]->ACT_SEL[R603]
		.assign select_stmt_num =  cardinality select_stmt_set
		.assign attr_result =attr_result+select_stmt_num	
		.//print "Body ${non_empty_body.Label} has ${select_stmt_num} select statements."
		.//select_fio
		.select many select_fio_stmt_set related by non_empty_body->ACT_BLK[R601]->ACT_SMT[R602]->ACT_FIO[R603]
		.assign select_fio_stmt_num =  cardinality select_fio_stmt_set
		.assign attr_result =attr_result+select_fio_stmt_num	
		.//print "Body ${non_empty_body.Label} has ${select_fio_stmt_num} select_fio statements."
		.//select_fiw
		.select many select_fiw_stmt_set related by non_empty_body->ACT_BLK[R601]->ACT_SMT[R602]->ACT_FIW[R603]
		.assign select_fiw_stmt_num =  cardinality select_fiw_stmt_set
		.assign attr_result =attr_result+select_fiw_stmt_num	
		.//print "Body ${non_empty_body.Label} has ${select_fiw_stmt_num} select_fiw statements."
		.//
		.//Parameter count: provided signals
		.select many provided_port_signal_parameter_set related by non_empty_body->ACT_PSB[R698]->SPR_PS[R686]->SPR_PEP[R4503]->C_EP[R4501]->C_PP[R4006]
		.assign provided_port_signal_parameter_num = cardinality provided_port_signal_parameter_set
		.assign attr_result =attr_result+provided_port_signal_parameter_num	
		.//print "${non_empty_body.Label} has parameters ${provided_port_signal_parameter_num} "
		.//
		.//Parameter count: functions
		.select many function_parameter_set related by non_empty_body->ACT_FNB[R698]->S_SYNC[R695]->S_SPARM[R24]
		.assign function_parameter_num = cardinality function_parameter_set
		.assign attr_result =attr_result+function_parameter_num	
		.//print "${non_empty_body.Label} has parameters ${function_parameter_num} "		
		.//
		.//Parameter count: opers
		.select many oper_parameter_set related by non_empty_body->ACT_OPB[R698]->O_TFR[R696]->O_TPARM[R117]
		.assign oper_parameter_num = cardinality oper_parameter_set
		.assign attr_result =attr_result+oper_parameter_num	
		.//print "${non_empty_body.Label} has parameters ${oper_parameter_num} "
		.//
		.select many non_empty_state_param_set related by non_empty_body->ACT_SAB[R698]->SM_ACT[R691]->SM_AH[R514]->SM_MOAH[R513]->SM_STATE[R511]->SM_SEME[R503]->SM_SEVT[R503]->SM_EVT[R525]->SM_EVTDI[R532]
		.assign non_empty_state_param_num = cardinality non_empty_state_param_set
		.assign attr_result =attr_result+non_empty_state_param_num	
		.//print "${non_empty_body.Label} has parameters ${non_empty_state_param_num} "
		.//
		.//
		.select many non_empty_txn_param_set related by non_empty_body->ACT_TAB[R698]->SM_ACT[R688]->SM_AH[R514]->SM_TAH[R513]->SM_TXN[R530]->SM_NSTXN[R507]->SM_SEME[R504]->SM_SEVT[R503]->SM_EVT[R525]->SM_EVTDI[R532]
		.assign non_empty_txn_param_num = cardinality non_empty_txn_param_set
		.assign attr_result =attr_result+non_empty_txn_param_num	
		.//print "${non_empty_body.Label} has parameters ${non_empty_txn_param_num} "
	.end for
.end function
.//------------------------------------------------------------------------------------
.//------------------------------------------------------------------------------------
.// END OF FUNCTIONS
.//------------------------------------------------------------------------------------
.//------------------------------------------------------------------------------------
.invoke pl = GET_ENV_VAR("PROJECT_LOC")
.invoke co = GET_ENV_VAR("COMP")
.if(pl.success and co.success)
	.print "======================================================================================"
	.assign project_location = pl.result	
	.assign comp_name = co.result	
	.print "Using project location ${project_location}."
	.print "Using component name ${comp_name}."
	.print "======================================================================================"
	.//--------------------------------------------------------------------------------
	.//INCLUSIONS
	.//include "${project_location}/utils/q.general.utils.arc"
	.//include "${project_location}/utils/q.cc.horizontal.arc"
	.//include "${project_location}/utils/q.cc.vertical.arc"
	.//include "${project_location}/utils/q.loc.horizontal.arc"
	.//include "${project_location}/utils/q.printing.arc"
	.print "Starting..."
	.select any comp from instances of C_C where selected.Name==comp_name
	.if(not_empty comp)
		.//Component data flow complexity
		.invoke cdfc = calculate_all_comp_port_data_flow_complexity(comp)	
		.assign comp_port_param_num = cdfc.result
		.print "Component ${comp.Name} on port UI has ${comp_port_param_num} parameters on incoming messages."
COMP,${comp_port_param_num}
		.//Class data flow complexity
		.invoke ccldfc = calculate_all_comp_class_data_flow_complexity(comp)
		.assign class_operation_param_num = ccldfc.result
		.print "Component ${comp.Name} has ${class_operation_param_num} parameters on all operations of all classes."
CLASS,${class_operation_param_num}
		.//State machine data flow complexity
		.invoke csmdfc = calculate_all_comp_state_machine_data_flow_complexity(comp)
		.assign sm_evtdti_num = csmdfc.result
		.print "Component ${comp.Name} has ${sm_evtdti_num} event data items on all transitions and states with non-empty bodies."
SM,${sm_evtdti_num}
		.//Body data flow complexity
		.invoke cbdfc = calculate_all_comp_body_data_flow_complexity(comp)
		.assign body_def_num = cbdfc.result
		.print "Component ${comp.Name} has ${body_def_num} definitions in all its bodies."
BODY,${body_def_num}
		.assign visual_df = (comp_port_param_num + class_operation_param_num) + sm_evtdti_num 
VISUAL,${visual_df} 
	.end if
	.print "DONE!"
	.select any sys from instances of S_SYS
	.print "Sys.ID = ${sys.Sys_ID}"
	.emit to file "C:/Temp/${sys.Name}_DataFlowComplexity_vertical.csv"
.else
	.print "FAILED TO READ ENV VAR: PROJECT_LOC or COMP"
.end if