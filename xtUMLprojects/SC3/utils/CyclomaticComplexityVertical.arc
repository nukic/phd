.//------------------------------------------------------------------------------------
.//------------------------------------------------------------------------------------
.// GENERAL HELPER FUNCTIONS
.//------------------------------------------------------------------------------------
.//------------------------------------------------------------------------------------
.function find_classes_in_component
.param inst_ref comp_inst
	.select many attr_result from instances of O_OBJ where false
	.select many root_pkg_set related by comp_inst->PE_PE[R8003]->EP_PKG[R8001]
	.for each root_pkg in root_pkg_set
		.invoke cips = find_classes_recursively(root_pkg)
		.assign class_in_pkg_set = cips.result
		.assign attr_result = attr_result|class_in_pkg_set
	.end for
.end function
.//
.function find_classes_recursively
.param inst_ref pkg_inst
	.select many class_set related by pkg_inst->PE_PE[R8000]->O_OBJ[R8001]
	.select many internal_pkg_set related by pkg_inst->PE_PE[R8000]->EP_PKG[R8001]
	.for each internal_pkg in internal_pkg_set
		.invoke ns = find_classes_recursively(internal_pkg)
		.assign nested_classes = ns.result
		.assign class_set = class_set|nested_classes
	.end for
	.assign attr_result = class_set
.end function
.//
.function find_relations_in_component
.param inst_ref comp_inst
	.select many attr_result from instances of R_REL where false
	.select many root_pkg_set related by comp_inst->PE_PE[R8003]->EP_PKG[R8001]
	.for each root_pkg in root_pkg_set
		.invoke rips = find_relations_recursively(root_pkg)
		.assign relation_in_pkg_set = rips.result
		.assign attr_result = attr_result|relation_in_pkg_set
	.end for
.end function
.//
.function find_relations_recursively
.param inst_ref pkg_inst
	.select many relation_set related by pkg_inst->PE_PE[R8000]->R_REL[R8001]
	.select many internal_pkg_set related by pkg_inst->PE_PE[R8000]->EP_PKG[R8001]
	.for each internal_pkg in internal_pkg_set
		.invoke nr = find_relations_recursively(internal_pkg)
		.assign nested_relations = nr.result
		.assign relation_set = relation_set|nested_relations
	.end for
	.assign attr_result = relation_set
.end function
.//
.function find_functions_recursively
.param inst_ref pkg_inst
	.select many fun_set related by pkg_inst->PE_PE[R8000]->S_SYNC[R8001]
	.select many internal_pkg_set related by pkg_inst->PE_PE[R8000]->EP_PKG[R8001]
	.for each internal_pkg in internal_pkg_set
		.invoke nf = find_functions_recursively(internal_pkg)
		.assign nested_funs = nr.result
		.assign fun_set = fun_set|nested_funs
	.end for
	.assign attr_result = fun_set
.end function
.//
.function find_ees_recursively
.param inst_ref pkg_inst
	.select many ee_set related by pkg_inst->PE_PE[R8000]->S_EE[R8001]
	.select many internal_pkg_set related by pkg_inst->PE_PE[R8000]->EP_PKG[R8001]
	.for each internal_pkg in internal_pkg_set
		.invoke nee = find_ees_recursively(internal_pkg)
		.assign nested_ees = nee.result
		.assign ee_set = ee_set|nested_ees
	.end for
	.assign attr_result = ee_set
.end function
.//
.function find_all_bodies_within_component
.param inst_ref comp_inst
	.select many body_set from instances of ACT_ACT where false
	.//Provided port bodies
	.select many provided_port_signal_body_set related by comp_inst->C_PO[R4010]->C_IR[R4016]->C_P[R4009]->SPR_PEP[R4501]->SPR_PS[R4503]->ACT_PSB[R686]->ACT_ACT[R698]
	.assign psb_num =  cardinality provided_port_signal_body_set
	.//print "Component ${comp_inst.Name} has provided signal bodies: ${psb_num}"
	.assign body_set = body_set|provided_port_signal_body_set
	.select many provided_port_operation_body_set related by comp_inst->C_PO[R4010]->C_IR[R4016]->C_P[R4009]->SPR_PEP[R4501]->SPR_PO[R4503]->ACT_POB[R687]->ACT_ACT[R698]
	.assign pob_num =  cardinality provided_port_operation_body_set
	.//print "Component ${comp_inst.Name} has provided operation bodies: ${pob_num}"
	.assign body_set = body_set|provided_port_operation_body_set
	.//Required port bodies
	.select many required_port_signal_body_set related by comp_inst->C_PO[R4010]->C_IR[R4016]->C_R[R4009]->SPR_REP[R4500]->SPR_RS[R4502]->ACT_RSB[R684]->ACT_ACT[R698]
	.assign rsb_num =  cardinality required_port_signal_body_set
	.//print "Component ${comp_inst.Name} has required signal bodies: ${rsb_num}"
	.assign body_set = body_set|required_port_signal_body_set
	.select many required_port_operation_body_set related by comp_inst->C_PO[R4010]->C_IR[R4016]->C_R[R4009]->SPR_REP[R4500]->SPR_RO[R4502]->ACT_ROB[R685]->ACT_ACT[R698]
	.assign rob_num =  cardinality required_port_operation_body_set
	.//print "Component ${comp_inst.Name} has required operation bodies: ${rob_num}"
	.assign body_set = body_set|required_port_operation_body_set
	.//In function body
	.select many root_pkg_set related by comp_inst->PE_PE[R8003]->EP_PKG[R8001]
	.select many fun_set from instances of S_SYNC where false
	.for each root_pkg in root_pkg_set
		.invoke fs = find_functions_recursively(root_pkg)
		.assign fun_set = fun_set|fs.result
	.end for
	.select many fun_body_set related by fun_set->ACT_FNB[R695]->ACT_ACT[R698]
	.assign fb_num =  cardinality fun_body_set
	.//print "Component ${comp_inst.Name} has function bodies: ${fb_num}"
	.assign body_set = body_set|fun_body_set
	.//Class operation bodies
	.select many cls_set from instances of O_OBJ where false
	.for each root_pkg in root_pkg_set
		.invoke fcr = find_classes_recursively(root_pkg)
		.assign cls_set = cls_set|fcr.result
	.end for
	.select many op_body_set related by cls_set->O_TFR[R115]->ACT_OPB[R696]->ACT_ACT[R698]
	.assign ob_num =  cardinality op_body_set
	.//print "Component ${comp_inst.Name} has class operation bodies: ${ob_num}"
	.assign body_set = body_set|op_body_set
	.//Derr attr bodies
	.select many derattr_body_set related by cls_set->O_ATTR[R102]->O_BATTR[R106]->O_DBATTR[R107]->ACT_DAB[R693]->ACT_ACT[R698]
	.assign dab_num =  cardinality derattr_body_set
	.//print "Component ${comp_inst.Name} has derived attributes bodies: ${dab_num}"
	.assign body_set = body_set|derattr_body_set
	.//Bridge bodies
	.select many ee_set from instances of S_EE where false
	.for each root_pkg in root_pkg_set
		.invoke fees = find_ees_recursively(root_pkg)
		.assign ee_set = ee_set|fees.result
	.end for
	.select many bridge_body_set related by ee_set->S_BRG[R19]->ACT_BRB[R697]->ACT_ACT[R698]
	.assign bb_num =  cardinality bridge_body_set
	.//print "Component ${comp_inst.Name} has bridge bodies: ${bb_num}"
	.assign body_set = body_set|bridge_body_set
	.//State bodies
	.select many state_body_set related by cls_set->SM_ISM[R518]->SM_SM[R517]->SM_ACT[R515]->ACT_SAB[R691]->ACT_ACT[R698]
	.assign sb_num =  cardinality state_body_set
	.//print "Component ${comp_inst.Name} has state bodies: ${sb_num}"
	.assign body_set = body_set|state_body_set
	.//Transition bodies
	.select many txn_body_set related by cls_set->SM_ISM[R518]->SM_SM[R517]->SM_ACT[R515]->ACT_TAB[R688]->ACT_ACT[R698]
	.assign tb_num =  cardinality txn_body_set
	.//print "Component ${comp_inst.Name} has transition bodies: ${tb_num}"
	.assign body_set = body_set|txn_body_set
	.assign body_set_num =  cardinality body_set
	.print "Component ${comp_inst.Name} has TOTAL bodies: ${body_set_num}"
	.assign attr_result = body_set	
.end function
.//
.function count_comp_bodies
.param inst_ref comp_inst
	.assign attr_result = 0;
	.//defined in in q.general.utils.arc
	.invoke bs = find_all_bodies_within_component(comp_inst)
	.assign body_set = bs.result
	.assign attr_result = (cardinality body_set)
.end function
.//
.function count_comp_sms
.param inst_ref comp_inst
	.assign attr_result = 0;
	.//defined in in q.general.utils.arc
	.invoke cls = find_classes_in_component(comp_inst)
	.assign class_set = cls.result
	.for each class in class_set
		.select one sm related by class->SM_ISM[R518]->SM_SM[R517]
		.if(not_empty sm)
			.assign attr_result = attr_result + 1;
		.end if
	.end for
.end function
.//------------------------------------------------------------------------------------
.//------------------------------------------------------------------------------------
.// END GENERAL HELPER  FUNCTIONS
.//------------------------------------------------------------------------------------
.//------------------------------------------------------------------------------------
.//
.//------------------------------------------------------------------------------------
.//------------------------------------------------------------------------------------
.// CYCLOMATIC COMPLEXITY HELPER FUNCTIONS
.//------------------------------------------------------------------------------------
.//------------------------------------------------------------------------------------
.function calculate_cc_class_complete
.param inst_ref cls_inst
	.assign attr_result = 0
	.invoke csc = calculate_cc_sm(cls_inst, true)
	.assign cc_class_sm = csc.result
	.invoke cdc = count_class_decisons_and_calls(cls_inst)
	.assign class_decisions_and_calls = cdc.result
	.invoke cbc = count_class_bodies(cls_inst)
	.assign class_bodies = cbc.result
	.assign attr_result = class_decisions_and_calls - class_bodies + cc_class_sm
	.//print "Class ${cls_inst.Name} (with state machine and bodies) contributes to cyclomatic complexity: ${attr_result}"
.end function
.//
.//If includeSmInfrastructure is true it give +5 = 6-1 for CCsm
.// We use 5 and not 6 because, when integrating with rest of the CFG, 
.//we loose the virtual edge from exit to the end of SM CFG. 
.//When integrating SM CFG into one large component CFG we get additional edge 
.//when creating instance of a class that has ISM because it is counted as 
.//invocation that starts ISM. That invocation is however counted in processing 
.//model, not in SM model
.function calculate_cc_sm
.param inst_ref class
.param boolean includeSmInfrastructure
	.assign attr_result = 0
	.select one ism related by class->SM_ISM[R518]->SM_SM[R517]
	.if(not_empty ism)
		.if(includeSmInfrastructure)
			.//See upper comment why 5 and not 6
			.assign attr_result = 5
		.end if
		.select many state_set related by ism->SM_STATE[R501]
		.//Contribution of each state
		.for each state in state_set
			.assign state_contrib = 0
			.select many state_entry_set related by state->SM_SEME[R503]
			.assign state_entry_num = (cardinality state_entry_set) 
			.//print "State ${state.Numb} in class ${class.Name} has ${state_entry_num} entries in state-event matrix."
			.select many state_ei_entry_set related by state->SM_SEME[R503]->SM_EIGN[R504]
			.assign state_ei_entry_num = (cardinality state_ei_entry_set) 
			.//print "State ${state.Numb} in class ${class.Name} has ${state_ei_entry_num} Event-Ignored entries in state-event matrix."
			.assign state_non_ei_entry_num = (state_entry_num - state_ei_entry_num)
			.//print "State ${state.Numb} in class ${class.Name} has ${state_non_ei_entry_num} non-Event-Ignored entries in state-event matrix."
			.//If state has at most one event ignored entry, state contribution to cyclomatic 
			.//complexity is equal to number of entries ni the row (which is equal to number 
			.//of received events). Otherwise, state contributes with one more than there is 
			.//non-event-ignored entries
			.if(state_ei_entry_num<=1)
				.assign state_contrib = state_entry_num
			.else
				.assign state_contrib = state_non_ei_entry_num + 1 
			.end if
			.//print "State ${state.Numb} in class ${class.Name} contributes with ${state_contrib} to SM cyclomatic complexity."
			.assign attr_result = attr_result + state_contrib
		.end for
		.//Contribution of transitions: State machine has CFG that for each RTC has 2 calls: 
		.//transition effect call and state entry body call. This means there are 2 calls per RTC.
		.//Since there are as many RTCs and there is transitions. state machine, without its bodies, 
		.//implies 2Nt invocations
		.select many transition_set related by ism->SM_TXN[R505]
		.assign num_txn =  cardinality transition_set
		.//print "Class ${class.Name} has state machine with ${num_txn} transitions (RTC steps)"
		.assign attr_result = attr_result + (2*num_txn)
		.//print "Class ${class.Name} has state machine cyclomatic complexity: ${attr_result}"
	.else
		.//print "Class ${class.Name} has not instance state machine."
	.end if
.end function
.//
.function count_class_decisons_and_calls
.param inst_ref cls_inst
	.assign attr_result = 0
	.assign body_num = 0
	.assign allDecisionsAndCalls = 0
	.select many body_set from instances of ACT_ACT where false
	.//Class operation bodies
	.select many op_body_set related by cls_inst->O_TFR[R115]->ACT_OPB[R696]->ACT_ACT[R698]
	.assign ob_num =  cardinality op_body_set
	.//print "Class ${cls_inst.Name} has class operation bodies: ${ob_num}"
	.assign body_set = body_set|op_body_set
	.//Derr attr bodies
	.select many derattr_body_set related by cls_inst->O_ATTR[R102]->O_BATTR[R106]->O_DBATTR[R107]->ACT_DAB[R693]->ACT_ACT[R698]
	.assign dab_num =  cardinality derattr_body_set
	.//print "Class ${cls_inst.Name} has derived attributes bodies: ${dab_num}"
	.assign body_set = body_set|derattr_body_set
	.//State bodies
	.select many state_body_set related by cls_inst->SM_ISM[R518]->SM_SM[R517]->SM_ACT[R515]->ACT_SAB[R691]->ACT_ACT[R698]
	.assign sb_num =  cardinality state_body_set
	.//print "Class ${cls_inst.Name} has state bodies: ${sb_num}"
	.assign body_set = body_set|state_body_set
	.//Transition bodies
	.select many txn_body_set related by cls_inst->SM_ISM[R518]->SM_SM[R517]->SM_ACT[R515]->ACT_TAB[R688]->ACT_ACT[R698]
	.assign tb_num =  cardinality txn_body_set
	.//print "Class ${cls_inst.Name} has transition bodies: ${tb_num}"
	.assign body_set = body_set|txn_body_set
	.assign body_num =  cardinality body_set
	.//
	.for each body in body_set
		.invoke dac = calculate_cc_single_body(body)
		.assign allDecisionsAndCalls = allDecisionsAndCalls + dac.result
	.end for
	.//Notice the unlikely but legal case when a class has more bodies than decisions and calls in those bodies
	.//This results that class has negative contribution to cyclomatic complexity.
	.assign attr_result = allDecisionsAndCalls - body_num
	.//print "Class ${cls_inst.Name} has processing model complexity of: ${attr_result}"
.end function
.//
.function count_class_bodies
.param inst_ref cls_inst
	.assign attr_result = 0
	.assign body_num = 0
	.select many body_set from instances of ACT_ACT where false
	.//Class operation bodies
	.select many op_body_set related by cls_inst->O_TFR[R115]->ACT_OPB[R696]->ACT_ACT[R698]
	.assign ob_num =  cardinality op_body_set
	.//print "Class ${cls_inst.Name} has class operation bodies: ${ob_num}"
	.assign body_set = body_set|op_body_set
	.//Derr attr bodies
	.select many derattr_body_set related by cls_inst->O_ATTR[R102]->O_BATTR[R106]->O_DBATTR[R107]->ACT_DAB[R693]->ACT_ACT[R698]
	.assign dab_num =  cardinality derattr_body_set
	.//print "Class ${cls_inst.Name} has derived attributes bodies: ${dab_num}"
	.assign body_set = body_set|derattr_body_set
	.//State bodies
	.select many state_body_set related by cls_inst->SM_ISM[R518]->SM_SM[R517]->SM_ACT[R515]->ACT_SAB[R691]->ACT_ACT[R698]
	.assign sb_num =  cardinality state_body_set
	.//print "Class ${cls_inst.Name} has state bodies: ${sb_num}"
	.assign body_set = body_set|state_body_set
	.//Transition bodies
	.select many txn_body_set related by cls_inst->SM_ISM[R518]->SM_SM[R517]->SM_ACT[R515]->ACT_TAB[R688]->ACT_ACT[R698]
	.assign tb_num =  cardinality txn_body_set
	.//print "Class ${cls_inst.Name} has transition bodies: ${tb_num}"
	.assign body_set = body_set|txn_body_set
	.assign body_num =  cardinality body_set
	.//
	.assign attr_result = body_num
	.//print "Class ${cls_inst.Name} has ${attr_result} bodies!"
.end function
.//
.function count_body_decisons_and_calls
.param inst_ref body
	.assign attr_result = 0
	.assign decisons = 0
	.assign calls = 0
	.select many stmt_set related by body->ACT_BLK[R601]->ACT_SMT[R602]
	.assign stmt_num =  cardinality stmt_set
	.//print "Body of ${body.Type} ${body.label} has total of ${stmt_num} statements."
	.//Decision statements
	.select many whl_set related by body->ACT_BLK[R601]->ACT_SMT[R602]->ACT_WHL[R603]
	.assign whl_num =  cardinality whl_set
	.assign decisons = decisons + whl_num
	.//print "Body of ${body.Type} ${body.label} has ${whl_num} while statements."
	.select many if_set related by body->ACT_BLK[R601]->ACT_SMT[R602]->ACT_IF[R603]
	.assign if_num =  cardinality if_set
	.assign decisons = decisons + if_num
	.//print "Body of ${body.Type} ${body.label} has ${if_num} if statements."
	.select many elif_set related by body->ACT_BLK[R601]->ACT_SMT[R602]->ACT_EL[R603]
	.assign elif_num =  cardinality elif_set
	.assign decisons = decisons + elif_num
	.//print "Body of ${body.Type} ${body.label} has ${elif_num} elif statements."
	.select many for_set related by body->ACT_BLK[R601]->ACT_SMT[R602]->ACT_FOR[R603]
	.assign for_num =  cardinality for_set
	.assign decisons = decisons + for_num	
	.//print "Body of ${body.Type} ${body.label} has ${for_num} for statements."
	.//
	.//Sync commuincations (calls)
	.select many brg_invk_set related by body->ACT_BLK[R601]->ACT_SMT[R602]->ACT_BRG[R603]
	.assign brg_invk_num =  cardinality brg_invk_set
	.assign calls = calls + brg_invk_num	
	.//print "Body of ${body.Type} ${body.label} has ${brg_invk_num} bridge invocation statements."
	.select many fnc_invk_set related by body->ACT_BLK[R601]->ACT_SMT[R602]->ACT_FNC[R603]
	.assign fnc_invk_num =  cardinality fnc_invk_set
	.assign calls = calls + fnc_invk_num
	.//print "Body of ${body.Type} ${body.label} has ${fnc_invk_num} function invocation statements."	
	.select many opr_invk_set related by body->ACT_BLK[R601]->ACT_SMT[R602]->ACT_TFM[R603]
	.assign opr_invk_num =  cardinality opr_invk_set
	.assign calls = calls + opr_invk_num	
	.//print "Body of ${body.Type} ${body.label} has ${opr_invk_num} operation invocation statements."
	.select many iop_invk_set related by body->ACT_BLK[R601]->ACT_SMT[R602]->ACT_IOP[R603]
	.assign iop_invk_num =  cardinality iop_invk_set
	.assign calls = calls + iop_invk_num	
	.//print "Body of ${body.Type} ${body.label} has ${iop_invk_num} port operation invocation statements."
	.//Async communcations (calls)
	.select many create_stmt_set related by body->ACT_BLK[R601]->ACT_SMT[R602]->ACT_CR[R603]
	.//Creation of an instance of a class that has a state machine implies starting a state machine
	.assign start_sm_call_num = 0
	.for each create_stmt in create_stmt_set
		.select one ism related by create_stmt->O_OBJ[R671]->SM_ISM[R518]
		.if(not_empty ism)
			.assign start_sm_call_num = start_sm_call_num + 1
		.end if
	.end for
	.assign calls = calls + start_sm_call_num	
	.//print "Body of ${body.Type} ${body.label} has ${start_sm_call_num} create statements for classes with ISM."	
	.select many sgn_invk_set related by body->ACT_BLK[R601]->ACT_SMT[R602]->ACT_SGN[R603]
	.assign sgn_invk_num =  cardinality sgn_invk_set
	.assign calls = calls + sgn_invk_num	
	.//print "Body of ${body.Type} ${body.label} has ${sgn_invk_num} port signal invocation statements."	
	.select many ges_set related by body->ACT_BLK[R601]->ACT_SMT[R602]->E_ESS[R603]->E_GES[R701]
	.assign ges_num =  cardinality ges_set
	.assign calls = calls + ges_num	
	.//print "Body of ${body.Type} ${body.label} has ${ges_num} generate event statements."
	.select many gpe_set related by body->ACT_BLK[R601]->ACT_SMT[R602]->E_GPR[R603]
	.assign gpe_num =  cardinality gpe_set
	.assign calls = calls + gpe_num
	.//print "Body of ${body.Type} ${body.label} has ${gpe_num} generate preexisting event statements."
	.//Invocations in expressions
	.select many oper_inv_expr_set related by body->ACT_BLK[R601]->V_VAL[R826]->V_TRV[R801]
	.assign oie_num =  cardinality oper_inv_expr_set 
	.assign calls = calls + oie_num
	.//print "Body of ${body.Type} ${body.label} has ${oie_num} statements with class operation invocation expressions."
	.select many attr_access_expr_set related by body->ACT_BLK[R601]->V_VAL[R826]->V_AVL[R801]
	.assign dbaie_num =  0
	.for each attr_access_expr in attr_access_expr_set
		.select one dbattr related by attr_access_expr->O_ATTR[R806]->O_BATTR[R106]->O_DBATTR[R107]	
		.if(not_empty dbattr)
			.select one dbattr_body related by dbattr->ACT_DAB[R693]->ACT_ACT[R698]
			.if(body.Action_ID!=dbattr_body.Action_ID)
				.assign dbaie_num =  dbaie_num + 1
			.end if
		.end if
	.end for
	.assign calls = calls + dbaie_num
	.//print "Body of ${body.Type} ${body.label} has ${dbaie_num} statements with derrived attribute access expressions."
	.select many fun_inv_expr_set related by body->ACT_BLK[R601]->V_VAL[R826]->V_FNV[R801]
	.assign fie_num =  cardinality fun_inv_expr_set
	.assign calls = calls + fie_num
	.//print "Body of ${body.Type} ${body.label} has ${fie_num} statements with function invocation expressions."
	.select many brg_inv_expr_set related by body->ACT_BLK[R601]->V_VAL[R826]->V_BRV[R801]
	.assign bie_num =  cardinality brg_inv_expr_set
	.assign calls = calls + bie_num
	.//print "Body of ${body.Type} ${body.label} has ${bie_num} statements with bridge invocation expressions."
	.select many portop_inv_expr_set related by body->ACT_BLK[R601]->V_VAL[R826]->V_MSV[R801]
	.assign popie_num =  cardinality portop_inv_expr_set
	.assign calls = calls + popie_num
	.//print "Body of ${body.Type} ${body.label} has ${popie_num} statements with port operation invocation expressions."
	.//
	.//Calculate all for this body
	.//print "Body of ${body.Type} ${body.label} has decisions: + ${decisons}"	
	.//print "Body of ${body.Type} ${body.label} has calls: + ${calls}"	
	.assign attr_result = decisons + calls
	.//print "Body of ${body.Type} ${body.label} has decisions and calls: + ${attr_result}"	
.end function
.//
.//----------------------------------------------------------------------------------------------------
.// END OF CYCLOMATIC COMPLEXITY HELPER FUNCTIONs
.//----------------------------------------------------------------------------------------------------
.//
.//----------------------------------------------------------------------------------------------------
.// VERTICAL CYCLOMATIC COMPLEXITY HELPER FUNCTIONs
.//----------------------------------------------------------------------------------------------------
.//--------------------------------------------------------------------------------
.// Component complexity = Nop + Nsig
.//--------------------------------------------------------------------------------
.function calculate_cc_comp_port
.param inst_ref comp_inst
	.assign attr_result = 0;
	.select many signal_set related by comp_inst->C_PO[R4010]->C_IR[R4016]->C_I[R4012]->C_EP[R4003]->C_AS[R4004]
	.select many operation_set related by comp_inst->C_PO[R4010]->C_IR[R4016]->C_I[R4012]->C_EP[R4003]->C_IO[R4004]
	.assign op_num = cardinality operation_set
	.assign sig_num = cardinality signal_set
	.//print "Component ${comp_inst.Name} has ${op_num} operations and ${sig_num} signals."
	.assign attr_result = (op_num + sig_num)
	.//print "Component ${comp_inst.Name} has (port) cyclomatic complexity: ${attr_result}"
.end function
.//
.//
.//-------------------------------------------------------------------------------
.// Class model cyclomatic complexity (graphical, does not include SM and bodies)
.// Class complexity = Nop + CCrelations
.//-------------------------------------------------------------------------------
.function calculate_cc_comp_class
.param inst_ref comp_inst
	.assign attr_result = 0;
	.assign op_num = 0
	.//defined in in q.general.utils.arc
	.invoke ric = find_relations_in_component(comp_inst)
	.assign relation_set = ric.result
	.assign rel_num = cardinality relation_set
	.for each rel in relation_set
		.assign cc_rel = 0
		.select one simple_rel related by rel->R_SIMP[R206]
		.select one assoc_rel related by rel->R_ASSOC[R206]
		.select one subsup_rel related by rel->R_SUBSUP[R206]
		.if(not_empty simple_rel)
			.select many participant_set related by simple_rel->R_PART[R207]
			.select one formalizer related by simple_rel->R_FORM[R208]
			.for each participant in participant_set
				.if(participant.Cond==1)
					.//print "Simple relation ${rel.Numb} is conditional on participant side."
					.assign cc_rel = cc_rel+1
				.end if
				.if(participant.Mult==1)
					.//print "Simple relation ${rel.Numb} is multiple on participant side."
					.assign cc_rel = cc_rel+1
				.end if
			.end for
			.if(not_empty formalizer)
				.if(formalizer.Cond==1)
					.//print "Simple relation ${rel.Numb} is conditional on formalizer side."
					.assign cc_rel = cc_rel+1
				.end if
				.if(formalizer.Mult==1)
					.//print "Simple relation ${rel.Numb} is multiple on formalizer side."
					.assign cc_rel = cc_rel+1
				.end if
			.end if
		.elif(not_empty assoc_rel)
			.select one one_side related by assoc_rel->R_AONE[R209]
			.select one other_side related by assoc_rel->R_AOTH[R210]
			.select one assoc_cls_side related by assoc_rel->R_ASSR[R211]
			.if(one_side.Cond==1)
				.//print "One side of associtive relation ${rel.Numb} is conditional."
				.assign cc_rel = cc_rel+1
			.end if
			.if(one_side.Mult==1)
				.//print "One side of associtive relation ${rel.Numb} is multiple."
				.assign cc_rel = cc_rel+1
			.end if
			.if(other_side.Cond==1)
				.//print "Other side of associtive relation ${rel.Numb} is conditional."
				.assign cc_rel = cc_rel+1
			.end if
			.if(other_side.Mult==1)
				.//print "Other side of associtive relation ${rel.Numb} is multiple."
				.assign cc_rel = cc_rel+1
			.end if
			.if(assoc_cls_side.Mult==1)
				.//print "Associative side of associtive relation ${rel.Numb} is multiple."
				.assign cc_rel = cc_rel+2
			.end if
		.elif(not_empty subsup_rel)
			.select many subclass_set related by subsup_rel->R_SUB[R213]
			.assign subcls_num =  (cardinality subclass_set)
			.//print "Generalization relation ${rel.Numb} defines ${subcls_num} subclasses."
			.assign cc_rel = cc_rel+subcls_num
		.else
			.print "ERROR: Relation ${rel.Numb} has unknown type! Ignoring it."
		.end if
		.//print "Relation ${rel.Numb} contributes to class cyclomtic complexity with: + ${cc_rel}"
		.assign attr_result = attr_result + cc_rel
	.end for
	.//
	.//Contribution of operations to complexity
	.invoke cls = find_classes_in_component(comp_inst)
	.assign class_set = cls.result
	.for each class in class_set
		.select many class_op_set related by class->O_TFR[R115]
		.assign class_op_num = cardinality class_op_set
		.print "Class ${class.Name} has operations ${class_op_num}"
		.assign op_num = op_num+class_op_num
	.end for
	.assign attr_result = attr_result + op_num
.end function
.//
.//-------------------------------------------------------------------------------
.// Graphical state machine cyclomatic complexity for all state machines in component 
.// Does not include 6 that include infrastructure
.//-------------------------------------------------------------------------------
.function calculate_cc_comp_sm_graphical
.param inst_ref comp_inst
	.assign attr_result = 0;
	.//defined in in q.general.utils.arc
	.invoke cic = find_classes_in_component(comp_inst)
	.assign class_set = cic.result
	.for each class in class_set
		.invoke csc = calculate_cc_sm(class, false)
		.assign cc_sm_class = csc.result
		.print "Class ${class.Name} has graphical state machine cyclomatic complexity: ${cc_sm_class}"
		.assign attr_result = attr_result + cc_sm_class
	.end for
.end function
.//-------------------------------------------------------------------------------
.// Complete state machine cyclomatic complexity for all state machines in component 
.// Includes cyclomatic complexity of infrastructure (+5 = 6-1) for each state machine
.//-------------------------------------------------------------------------------
.function calculate_cc_comp_sm_complete
.param inst_ref comp_inst
	.assign attr_result = 0;
	.//defined in in q.general.utils.arc
	.invoke cic = find_classes_in_component(comp_inst)
	.assign class_set = cic.result
	.for each class in class_set
		.invoke csc = calculate_cc_sm(class, true)
		.assign cc_sm_class = csc.result
		.print "Class ${class.Name} has COMPLETE state machine cyclomatic complexity: ${cc_sm_class}"
		.assign attr_result = attr_result + cc_sm_class
	.end for
.end function
.//-------------------------------------------------------------------------------
.// Processing code cyclomatic complexity:  = D + Ncall - p + 2
.//-------------------------------------------------------------------------------
.function calculate_cc_comp_body
.param inst_ref comp_inst
	.assign attr_result = 0;
	.//defined in in q.general.utils.arc
	.invoke bs = find_all_bodies_within_component(comp_inst)
	.assign body_set = bs.result
	.assign body_num = cardinality body_set
	.//print "Component ${comp_inst.Name} has ${body_num} bodies."
	.select many non_empty_body_set related by body_set->ACT_BLK[R601]->ACT_SMT[R602]->ACT_BLK[R602]->ACT_ACT[R601]
	.assign non_empty_body_num = cardinality non_empty_body_set
	.print "Component ${comp_inst.Name} has ${non_empty_body_num} non-empty bodies."
	.for each body in non_empty_body_set
		.invoke cdc = count_body_decisons_and_calls(body)	
		.assign decisions_and_calls= cdc.result	
		.print "Body ${body.Label} has decisions and calls:${decisions_and_calls}"
		.assign attr_result = attr_result+decisions_and_calls
	.end for
	.assign attr_result = attr_result-non_empty_body_num
	.assign attr_result = attr_result+2
.end function
.//----------------------------------------------------------------------------------------------------
.// END OF VERTICAL CYCLOMATIC COMPLEXITY HELPER FUNCTIONs
.//----------------------------------------------------------------------------------------------------
.//
.//----------------------------------------------------------------------------------------------------
.//----------------------------------------------------------------------------------------------------
.// !!! ACTUAL EXECUTION START POINT !!!
.//----------------------------------------------------------------------------------------------------
.//----------------------------------------------------------------------------------------------------
.//
.invoke pl = GET_ENV_VAR("PROJECT_LOC")
.invoke c = GET_ENV_VAR("COMP")
.if(pl.success and c.success)
	.print "======================================================================================"
	.assign project_location = pl.result	
	.print "Using project location ${project_location}."
	.assign comp_name = c.result
	.print "Calculating metrics for component ${comp_name}"
	.print "======================================================================================"
	.//--------------------------------------------------------------------------------
	.//INCLUSIONS
	.//include "${project_location}/utils/q.general.utils.arc"
	.//include "${project_location}/utils/q.cc.utils.arc"
	.//include "${project_location}/utils/q.cc.horizontal.arc"
	.//include "${project_location}/utils/q.cc.vertical.arc"
	.//include "${project_location}/utils/q.loc.horizontal.arc"
	.//include "${project_location}/utils/q.printing.arc"
	.//--------------------------------------------------------------------------------
	.select any comp_inst from instances of C_C where selected.Name==comp_name
	.if(not_empty comp_inst)
		.//--------------------------------------------------------------------------------
		.//Vertical distribution (takes into account only graphical)
		.invoke cpc = calculate_cc_comp_port(comp_inst)
		.assign cc_comp_port= cpc.result
		.invoke csmg = calculate_cc_comp_sm_graphical(comp_inst)
		.assign cc_comp_sm_graph = csmg.result
		.invoke ccc = calculate_cc_comp_class(comp_inst)
		.assign cc_comp_class = ccc.result	
		.invoke cbc = calculate_cc_comp_body(comp_inst)
		.assign cc_comp_body = cbc.result	
		.//Total cyclomatic complexity: cc_comp_body - num_body + cc_comp_sm - num_sm + cc_comp_port
		.invoke nb = count_comp_bodies(comp_inst)
		.assign num_bodies = nb.result
		.invoke csmc = calculate_cc_comp_sm_complete(comp_inst)
		.assign cc_comp_sm = csmc.result
		.invoke nsm = count_comp_sms(comp_inst)
		.assign num_sms = nsm.result
		.assign cc_total = (cc_comp_body + cc_comp_sm) + cc_comp_port
		.//Graphical/textual complexity
		.assign cc_graphical = (cc_comp_port + cc_comp_class) + cc_comp_sm_graph
		.assign cc_ratio = ((cc_graphical/1.0)/(cc_total/1.0))*100
		.//Relative values
		.assign cc_comp_port_rel =((cc_comp_port/1.0)/(cc_total/1.0))*100 
		.assign cc_comp_class_rel = ((cc_comp_class/1.0)/(cc_total/1.0))*100
		.assign cc_comp_sm_graph_rel =((cc_comp_sm_graph/1.0)/(cc_total/1.0))*100
		.assign cc_comp_sm_rel =((cc_comp_sm/1.0)/(cc_total/1.0))*100
		.assign cc_comp_body_rel = ((cc_comp_body/1.0)/(cc_total/1.0))*100
		.print "======================================================================================"
		.print "Component ${comp_inst.Name} has bodies: ${num_bodies} (empty and non-empty)"
		.print "Component ${comp_inst.Name} has state machines: ${num_sms}"
		.print "Component ${comp_inst.Name} has COMPONENT MODEL cyclomatic complexity: ${cc_comp_port} (${cc_comp_port_rel} %)"
		.print "Component ${comp_inst.Name} has CLASS MODEL cyclomatic complexity: ${cc_comp_class} (${cc_comp_class_rel} %)"
		.print "Component ${comp_inst.Name} has SM MODEL (graphical) cyclomatic complexity: ${cc_comp_sm_graph} (${cc_comp_sm_graph_rel} %)"
		.print "Component ${comp_inst.Name} has SM MODEL (complete) cyclomatic complexity: ${cc_comp_sm} (${cc_comp_sm_rel} %)"
		.print "Component ${comp_inst.Name} has PROCESSING MODEL cyclomatic complexity: ${cc_comp_body} (${cc_comp_body_rel} %)"
		.print "--------------------------------------------------------------------------------------"
		.print "Component ${comp_inst.Name} has graphical-to-total cyclomatic complexity ratio: ${cc_graphical}/${cc_total} = ${cc_ratio} %"
		.print "--------------------------------------------------------------------------------------"	
		.print "Component ${comp_inst.Name} has TOTAL cyclomatic complexity: CC_total = ${cc_total}"
		.print "======================================================================================"
	.else
		.print "ERROR: Component ${comp_name} does not exist."
	.end if
.else
	.print "Could not find project location. Giving up."
.end if	
