.//
.function calculate_cc_class_complete
.param inst_ref cls_inst
	.assign attr_result = 0
	.invoke csc = calculate_cc_sm_complete(cls_inst)
	.assign cc_class_sm = csc.result
	.invoke cdc = count_class_decisons_and_calls(cls_inst)
	.assign class_decisions_and_calls = cdc.result
	.invoke cbc = count_class_bodies(cls_inst)
	.assign class_bodies = cbc.result
	.assign attr_result = class_decisions_and_calls - class_bodies + cc_class_sm
	.print "Class ${cls_inst.Name} (with state machine and bodies) contributes to cyclomatic complexity: ${attr_result}"
.end function
.//
.//Includes only graphical part of SM complexity. This means 6 of the infrastructure is not included.
.// = \sum(s)[Nevt-Nei(s)] + Nstei + 2Nt
.function calculate_cc_sm_graphical
.param inst_ref class
	.assign attr_result = 0
	.select one ism related by class->SM_ISM[R518]->SM_SM[R517]
	.if(not_empty ism)
		.select many state_set related by ism->SM_STATE[R501]
		.//Contribution of each state
		.for each state in state_set
			.assign state_contrib = 0
			.select many state_entry_set related by state->SM_SEME[R503]
			.assign state_entry_num = (cardinality state_entry_set) 
			.//print "State ${state.Numb} in class ${class.Name} has ${state_entry_num} entries in state-event matrix."
			.select many state_ei_entry_set related by state->SM_SEME[R503]->SM_EIGN[R504]
			.assign state_ei_entry_num = (cardinality state_ei_entry_set) 
			.print "State ${state.Numb} in class ${class.Name} has ${state_ei_entry_num} Event-Ignored entries in state-event matrix."
			.assign state_non_ei_entry_num = (state_entry_num - state_ei_entry_num)
			.print "State ${state.Numb} in class ${class.Name} has ${state_non_ei_entry_num} non-Event-Ignored entries in state-event matrix."
			.//If state has at most one event ignored entry, state contribution to cyclomatic 
			.//complexity is equal to number of entries ni the row (which is equal to number 
			.//of received events). Otherwise, state contributes with one more than there is 
			.//non-event-ignored entries
			.if(state_ei_entry_num<=1)
				.assign state_contrib = state_entry_num
			.else
				.assign state_contrib = state_non_ei_entry_num + 1 
			.end if
			.//print "State ${state.Numb} in class ${class.Name} contributes with ${state_contrib} to SM cyclomatic complexity."
			.assign attr_result = attr_result + state_contrib
		.end for
		.//Contribution of transitions: State machine has CFG that for each RTC has 2 calls: 
		.//transition effect call and state entry body call. This means there are 2 calls per RTC.
		.//Since there are as many RTCs and there is transitions. state machine, without its bodies, 
		.//implies 2Nt invocations
		.select many transition_set related by ism->SM_TXN[R505]
		.assign num_txn =  cardinality transition_set
		.print "Class ${class.Name} has state machine with ${num_txn} transitions (RTC steps)"
		.assign attr_result = (attr_result + (2*num_txn))
		.print "Class ${class.Name} has state machine cyclomatic complexity: ${attr_result}"
	.else
		.//print "Class ${class.Name} has not instance state machine."
	.end if
.end function
.//
.//Includes complete SM cylomatic complexity: infrastructure and graphical
.// = 6 + \sum(s)[Nevt-Nei(s)] + Nstei + 2Nt
.function calculate_cc_sm_complete
.param inst_ref class
	.assign attr_result = 6
	.select one ism related by class->SM_ISM[R518]->SM_SM[R517]
	.if(not_empty ism)
		.select many state_set related by ism->SM_STATE[R501]
		.//Contribution of each state
		.for each state in state_set
			.assign state_contrib = 0
			.select many state_entry_set related by state->SM_SEME[R503]
			.assign state_entry_num = (cardinality state_entry_set) 
			.//print "State ${state.Numb} in class ${class.Name} has ${state_entry_num} entries in state-event matrix."
			.select many state_ei_entry_set related by state->SM_SEME[R503]->SM_EIGN[R504]
			.assign state_ei_entry_num = (cardinality state_ei_entry_set) 
			.//print "State ${state.Numb} in class ${class.Name} has ${state_ei_entry_num} Event-Ignored entries in state-event matrix."
			.assign state_non_ei_entry_num = (state_entry_num - state_ei_entry_num)
			.//print "State ${state.Numb} in class ${class.Name} has ${state_non_ei_entry_num} non-Event-Ignored entries in state-event matrix."
			.//If state has at most one event ignored entry, state contribution to cyclomatic 
			.//complexity is equal to number of entries ni the row (which is equal to number 
			.//of received events). Otherwise, state contributes with one more than there is 
			.//non-event-ignored entries
			.if(state_ei_entry_num<=1)
				.assign state_contrib = state_entry_num
			.else
				.assign state_contrib = state_non_ei_entry_num + 1 
			.end if
			.//print "State ${state.Numb} in class ${class.Name} contributes with ${state_contrib} to SM cyclomatic complexity."
			.assign attr_result = attr_result + state_contrib
		.end for
		.//Contribution of transitions: State machine has CFG that for each RTC has 2 calls: 
		.//transition effect call and state entry body call. This means there are 2 calls per RTC.
		.//Since there are as many RTCs and there is transitions. state machine, without its bodies, 
		.//implies 2Nt invocations
		.select many transition_set related by ism->SM_TXN[R505]
		.assign num_txn =  cardinality transition_set
		.//print "Class ${class.Name} has state machine with ${num_txn} transitions (RTC steps)"
		.assign attr_result = attr_result + (2*num_txn)
		.print "Class ${class.Name} has state machine cyclomatic complexity: ${attr_result}"
	.else
		.//print "Class ${class.Name} has not instance state machine."
	.end if
.end function
.//
.function count_class_decisons_and_calls
.param inst_ref cls_inst
	.assign attr_result = 0
	.assign body_num = 0
	.assign allDecisionsAndCalls = 0
	.select many body_set from instances of ACT_ACT where false
	.//Class operation bodies
	.select many op_body_set related by cls_inst->O_TFR[R115]->ACT_OPB[R696]->ACT_ACT[R698]
	.assign ob_num =  cardinality op_body_set
	.//print "Class ${cls_inst.Name} has class operation bodies: ${ob_num}"
	.assign body_set = body_set|op_body_set
	.//Derr attr bodies
	.select many derattr_body_set related by cls_inst->O_ATTR[R102]->O_BATTR[R106]->O_DBATTR[R107]->ACT_DAB[R693]->ACT_ACT[R698]
	.assign dab_num =  cardinality derattr_body_set
	.//print "Class ${cls_inst.Name} has derived attributes bodies: ${dab_num}"
	.assign body_set = body_set|derattr_body_set
	.//State bodies
	.select many state_body_set related by cls_inst->SM_ISM[R518]->SM_SM[R517]->SM_ACT[R515]->ACT_SAB[R691]->ACT_ACT[R698]
	.assign sb_num =  cardinality state_body_set
	.//print "Class ${cls_inst.Name} has state bodies: ${sb_num}"
	.assign body_set = body_set|state_body_set
	.//Transition bodies
	.select many txn_body_set related by cls_inst->SM_ISM[R518]->SM_SM[R517]->SM_ACT[R515]->ACT_TAB[R688]->ACT_ACT[R698]
	.assign tb_num =  cardinality txn_body_set
	.//print "Class ${cls_inst.Name} has transition bodies: ${tb_num}"
	.assign body_set = body_set|txn_body_set
	.assign body_num =  cardinality body_set
	.//
	.for each body in body_set
		.invoke dac = calculate_cc_single_body(body)
		.assign allDecisionsAndCalls = allDecisionsAndCalls + dac.result
	.end for
	.//Notice the unlikely but legal case when a class has more bodies than decisions and calls in those bodies
	.//This results that class has negative contribution to cyclomatic complexity.
	.assign attr_result = allDecisionsAndCalls - body_num
	.//print "Class ${cls_inst.Name} has processing model complexity of: ${attr_result}"
.end function
.//
.function count_class_bodies
.param inst_ref cls_inst
	.assign attr_result = 0
	.assign body_num = 0
	.select many body_set from instances of ACT_ACT where false
	.//Class operation bodies
	.select many op_body_set related by cls_inst->O_TFR[R115]->ACT_OPB[R696]->ACT_ACT[R698]
	.assign ob_num =  cardinality op_body_set
	.//print "Class ${cls_inst.Name} has class operation bodies: ${ob_num}"
	.assign body_set = body_set|op_body_set
	.//Derr attr bodies
	.select many derattr_body_set related by cls_inst->O_ATTR[R102]->O_BATTR[R106]->O_DBATTR[R107]->ACT_DAB[R693]->ACT_ACT[R698]
	.assign dab_num =  cardinality derattr_body_set
	.//print "Class ${cls_inst.Name} has derived attributes bodies: ${dab_num}"
	.assign body_set = body_set|derattr_body_set
	.//State bodies
	.select many state_body_set related by cls_inst->SM_ISM[R518]->SM_SM[R517]->SM_ACT[R515]->ACT_SAB[R691]->ACT_ACT[R698]
	.assign sb_num =  cardinality state_body_set
	.//print "Class ${cls_inst.Name} has state bodies: ${sb_num}"
	.assign body_set = body_set|state_body_set
	.//Transition bodies
	.select many txn_body_set related by cls_inst->SM_ISM[R518]->SM_SM[R517]->SM_ACT[R515]->ACT_TAB[R688]->ACT_ACT[R698]
	.assign tb_num =  cardinality txn_body_set
	.//print "Class ${cls_inst.Name} has transition bodies: ${tb_num}"
	.assign body_set = body_set|txn_body_set
	.assign body_num =  cardinality body_set
	.//
	.assign attr_result = body_num
	.print "Class ${cls_inst.Name} has ${attr_result} bodies!"
.end function
.//
.function count_body_decisons_and_calls
.param inst_ref body
	.assign attr_result = 0
	.assign decisons = 0
	.assign calls = 0
	.select many stmt_set related by body->ACT_BLK[R601]->ACT_SMT[R602]
	.assign stmt_num =  cardinality stmt_set
	.//print "Body of ${body.Type} ${body.label} has total of ${stmt_num} statements."
	.//Decision statements
	.select many whl_set related by body->ACT_BLK[R601]->ACT_SMT[R602]->ACT_WHL[R603]
	.assign whl_num =  cardinality whl_set
	.assign decisons = decisons + whl_num
	.//print "Body of ${body.Type} ${body.label} has ${whl_num} while statements."
	.select many if_set related by body->ACT_BLK[R601]->ACT_SMT[R602]->ACT_IF[R603]
	.assign if_num =  cardinality if_set
	.assign decisons = decisons + if_num
	.//print "Body of ${body.Type} ${body.label} has ${if_num} if statements."
	.select many elif_set related by body->ACT_BLK[R601]->ACT_SMT[R602]->ACT_EL[R603]
	.assign elif_num =  cardinality elif_set
	.assign decisons = decisons + elif_num
	.//print "Body of ${body.Type} ${body.label} has ${elif_num} elif statements."
	.select many for_set related by body->ACT_BLK[R601]->ACT_SMT[R602]->ACT_FOR[R603]
	.assign for_num =  cardinality for_set
	.assign decisons = decisons + for_num	
	.//print "Body of ${body.Type} ${body.label} has ${for_num} for statements."
	.//Sync commuincations (calls) 
	.select many brg_invk_set related by body->ACT_BLK[R601]->ACT_SMT[R602]->ACT_BRG[R603]
	.assign brg_invk_num =  cardinality brg_invk_set
	.assign calls = calls + brg_invk_num	
	.//print "Body of ${body.Type} ${body.label} has ${brg_invk_num} bridge invocation statements."
	.select many fnc_invk_set related by body->ACT_BLK[R601]->ACT_SMT[R602]->ACT_FNC[R603]
	.assign fnc_invk_num =  cardinality fnc_invk_set
	.assign calls = calls + fnc_invk_num
	.//print "Body of ${body.Type} ${body.label} has ${fnc_invk_num} function invocation statements."	
	.select many opr_invk_set related by body->ACT_BLK[R601]->ACT_SMT[R602]->ACT_TFM[R603]
	.assign opr_invk_num =  cardinality opr_invk_set
	.assign calls = calls + opr_invk_num	
	.//print "Body of ${body.Type} ${body.label} has ${opr_invk_num} operation invocation statements."
	.select many iop_invk_set related by body->ACT_BLK[R601]->ACT_SMT[R602]->ACT_IOP[R603]
	.assign iop_invk_num =  cardinality iop_invk_set
	.assign calls = calls + iop_invk_num	
	.//print "Body of ${body.Type} ${body.label} has ${iop_invk_num} port operation invocation statements."
	.//Async communcations (calls)
	.select many sgn_invk_set related by body->ACT_BLK[R601]->ACT_SMT[R602]->ACT_SGN[R603]
	.assign sgn_invk_num =  cardinality sgn_invk_set
	.assign calls = calls + sgn_invk_num	
	.//print "Body of ${body.Type} ${body.label} has ${sgn_invk_num} port signal invocation statements."	
	.select many ges_set related by body->ACT_BLK[R601]->ACT_SMT[R602]->E_ESS[R603]->E_GES[R701]
	.assign ges_num =  cardinality ges_set
	.assign calls = calls + ges_num	
	.//print "Body of ${body.Type} ${body.label} has ${ges_num} generate event statements."
	.select many gpe_set related by body->ACT_BLK[R601]->ACT_SMT[R602]->E_GPR[R603]
	.assign gpe_num =  cardinality gpe_set
	.assign calls = calls + gpe_num
	.//print "Body of ${body.Type} ${body.label} has ${gpe_num} generate preexisting event statements."
	.//Invocations in expressions
	.select many oper_inv_expr_set related by body->ACT_BLK[R601]->V_VAL[R826]->V_TRV[R801]
	.assign oie_num =  cardinality oper_inv_expr_set 
	.assign calls = calls + oie_num
	.//print "Body of ${body.Type} ${body.label} has ${oie_num} statements with class operation invocation expressions."
	.select many attr_access_expr_set related by body->ACT_BLK[R601]->V_VAL[R826]->V_AVL[R801]
	.assign dbaie_num =  0
	.for each attr_access_expr in attr_access_expr_set
		.select one dbattr related by attr_access_expr->O_ATTR[R806]->O_BATTR[R106]->O_DBATTR[R107]	
		.if(not_empty dbattr)
			.select one dbattr_body related by dbattr->ACT_DAB[R693]->ACT_ACT[R698]
			.if(body.Action_ID!=dbattr_body.Action_ID)
				.assign dbaie_num =  dbaie_num + 1
			.end if
		.end if
	.end for
	.assign calls = calls + dbaie_num
	.//print "Body of ${body.Type} ${body.label} has ${dbaie_num} statements with derrived attribute access expressions."
	.select many fun_inv_expr_set related by body->ACT_BLK[R601]->V_VAL[R826]->V_FNV[R801]
	.assign fie_num =  cardinality fun_inv_expr_set
	.assign calls = calls + fie_num
	.//print "Body of ${body.Type} ${body.label} has ${fie_num} statements with function invocation expressions."
	.select many brg_inv_expr_set related by body->ACT_BLK[R601]->V_VAL[R826]->V_BRV[R801]
	.assign bie_num =  cardinality brg_inv_expr_set
	.assign calls = calls + bie_num
	.//print "Body of ${body.Type} ${body.label} has ${bie_num} statements with bridge invocation expressions."
	.select many portop_inv_expr_set related by body->ACT_BLK[R601]->V_VAL[R826]->V_MSV[R801]
	.assign popie_num =  cardinality portop_inv_expr_set
	.assign calls = calls + popie_num
	.//print "Body of ${body.Type} ${body.label} has ${popie_num} statements with port operation invocation expressions."
	.//
	.//Calculate all for this body
	.//print "Body of ${body.Type} ${body.label} has decisions: + ${decisons}"	
	.//print "Body of ${body.Type} ${body.label} has calls: + ${calls}"	
	.assign attr_result = decisons + calls
	.print "Body of ${body.Type} ${body.label} has decisions and calls: + ${attr_result}"	
.end function
.//
.function count_comp_bodies
.param inst_ref comp_inst
	.assign attr_result = 0;
	.//defined in in q.general.utils.arc
	.invoke bs = find_all_bodies_within_component(comp_inst)
	.assign attr_result = bs.result
.end function
.//
.function count_comp_sms
.param inst_ref comp_inst
	.assign attr_result = 0;
	.//defined in in q.general.utils.arc
	.invoke cls = find_classes_in_component(comp_inst)
	.assign class_set = cls.result
	.for each class in class_set
		.select one sm related by class->SM_ISM[R518]->SM_SM[R517]
		.if(not_empty ism)
			.assign attr_result = attr_result + 1;
		.end if
	.end for
.end function
.//