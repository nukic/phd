.//------------------------------------------------------------------------------------
.//------------------------------------------------------------------------------------
.// GENERATE FUNCTIONS
.//------------------------------------------------------------------------------------
.//------------------------------------------------------------------------------------
.function generate_loc_comp_class_bodies
.param inst_ref comp_inst
.param string project_loc
	.invoke cic = find_classes_in_component(comp_inst)
	.assign class_set = cic.result
	.assign loc_all_classes = 0
	.for each class in class_set
		.invoke ccc = calculate_loc_single_class(class)
		.assign loc_class = ccc.result
		.assign key = "${class.Name}"
		.assign value = "${loc_class}"
${key},${value}
		.assign loc_all_classes = loc_all_classes + loc_class
	.end for
.end function
.//
.function generate_loc_comp_bodies
.param inst_ref comp_inst
.param string project_loc
	.assign attr_result = 0;
	.invoke bs = find_all_bodies_within_component(comp_inst)
	.assign body_set = bs.result
	.assign body_num = cardinality body_set
	.print "Component ${comp_inst.Name} has ${body_num} bodies."
	.select many non_empty_body_set related by body_set->ACT_BLK[R601]->ACT_SMT[R602]->ACT_BLK[R602]->ACT_ACT[R601]
	.assign non_empty_body_num = cardinality non_empty_body_set
	.print "Component ${comp_inst.Name} has ${non_empty_body_num} non-empty bodies."
	.assign empty_body_set = body_set - non_empty_body_set
	.for each empty_body in empty_body_set
		.print "Empty body ${empty_body.Label}"
	.end for
	.for each body in non_empty_body_set
		.invoke cbc = calculate_loc_single_body(body)	
		.assign loc_current_body = cbc.result
		.assign key =  "${body.Type}_${body.label}"
		.assign value = "${loc_current_body}"
${key},${value}
		.assign attr_result = attr_result+loc_current_body
	.end for
	.assign key =  "TOTAL NUMBER OF NON_EMPTY BODIES: "
	.assign value = "${non_empty_body_num}"
${key},${value}
	.assign key =  "TOTAL LOC OF COMP BODIES: "
	.assign value = "${attr_result}"
${key},${value}
.end function
.//------------------------------------------------------------------------------------
.//------------------------------------------------------------------------------------
.// HELPER FUNCTIONS
.//------------------------------------------------------------------------------------
.//------------------------------------------------------------------------------------
.function calculate_loc_single_class
.param inst_ref cls_inst
	.assign attr_result = 0
	.invoke cbc = calculate_loc_class_bodies(cls_inst)
	.assign attr_result = cbc.result	
	.//print "Class ${cls_inst.Name} (with state machine and bodies) contributes to cyclomatic complexity: ${attr_result}"
.end function
.//
.function calculate_loc_comp_body
.param inst_ref comp_inst
	.assign attr_result = 0;
	.invoke bs = find_all_bodies_within_component(comp_inst)
	.assign body_set = bs.result
	.assign body_num = cardinality body_set
	.print "Component ${comp_inst.Name} has ${body_num} bodies."
	.select many non_empty_body_set related by body_set->ACT_BLK[R601]->ACT_SMT[R602]->ACT_BLK[R602]->ACT_ACT[R601]
	.assign non_empty_body_num = cardinality non_empty_body_set
	.print "Component ${comp_inst.Name} has ${non_empty_body_num} non-empty bodies."
	.for each body in non_empty_body_set
		.invoke cbc = calculate_loc_single_body(body)	
		.assign loc_current_body = cbc.result	
		.assign attr_result = attr_result+loc_current_body
	.end for
.end function
.//
.function calculate_loc_class_bodies
.param inst_ref cls_inst
	.assign attr_result = 0
	.assign locTotal = 0
	.select many body_set from instances of ACT_ACT where false
	.//Class operation bodies
	.select many op_body_set related by cls_inst->O_TFR[R115]->ACT_OPB[R696]->ACT_ACT[R698]
	.assign ob_num =  cardinality op_body_set
	.print "Class ${cls_inst.Name} has class operation bodies: ${ob_num}"
	.assign body_set = body_set|op_body_set
	.//Derr attr bodies
	.select many derattr_body_set related by cls_inst->O_ATTR[R102]->O_BATTR[R106]->O_DBATTR[R107]->ACT_DAB[R693]->ACT_ACT[R698]
	.assign dab_num =  cardinality derattr_body_set
	.print "Class ${cls_inst.Name} has derived attributes bodies: ${dab_num}"
	.assign body_set = body_set|derattr_body_set
	.//State bodies
	.select many state_body_set related by cls_inst->SM_ISM[R518]->SM_SM[R517]->SM_ACT[R515]->ACT_SAB[R691]->ACT_ACT[R698]
	.assign sb_num =  cardinality state_body_set
	.print "Class ${cls_inst.Name} has state bodies: ${sb_num}"
	.assign body_set = body_set|state_body_set
	.//Transition bodies
	.select many txn_body_set related by cls_inst->SM_ISM[R518]->SM_SM[R517]->SM_ACT[R515]->ACT_TAB[R688]->ACT_ACT[R698]
	.assign tb_num =  cardinality txn_body_set
	.print "Class ${cls_inst.Name} has transition bodies: ${tb_num}"
	.assign body_set = body_set|txn_body_set
	.//
	.for each body in body_set
		.invoke dac = calculate_loc_single_body(body)
		.assign locTotal = locTotal + dac.result
	.end for
	.assign attr_result = locTotal
	.print "Class ${cls_inst.Name} has processing model complexity of: ${attr_result}"
.end function
.//
.function calculate_loc_single_body
.param inst_ref body
	.assign attr_result = 0
	.select many stmt_set related by body->ACT_BLK[R601]->ACT_SMT[R602]
	.assign attr_result =  cardinality stmt_set
	.print "Body of ${body.Type} ${body.label} has total of ${attr_result} statements."
.end function
.//
.//------------------------------------------------------------------------------------
.//------------------------------------------------------------------------------------
.// GENERAL HELPER FUNCTIONS
.//------------------------------------------------------------------------------------
.//------------------------------------------------------------------------------------
.function find_classes_in_component
.param inst_ref comp_inst
	.select many attr_result from instances of O_OBJ where false
	.select many root_pkg_set related by comp_inst->PE_PE[R8003]->EP_PKG[R8001]
	.for each root_pkg in root_pkg_set
		.invoke cips = find_classes_recursively(root_pkg)
		.assign class_in_pkg_set = cips.result
		.assign attr_result = attr_result|class_in_pkg_set
	.end for
.end function
.//
.function find_classes_recursively
.param inst_ref pkg_inst
	.select many class_set related by pkg_inst->PE_PE[R8000]->O_OBJ[R8001]
	.select many internal_pkg_set related by pkg_inst->PE_PE[R8000]->EP_PKG[R8001]
	.for each internal_pkg in internal_pkg_set
		.invoke ns = find_classes_recursively(internal_pkg)
		.assign nested_classes = ns.result
		.assign class_set = class_set|nested_classes
	.end for
	.assign attr_result = class_set
.end function
.//
.function find_relations_in_component
.param inst_ref comp_inst
	.select many attr_result from instances of R_REL where false
	.select many root_pkg_set related by comp_inst->PE_PE[R8003]->EP_PKG[R8001]
	.for each root_pkg in root_pkg_set
		.invoke rips = find_relations_recursively(root_pkg)
		.assign relation_in_pkg_set = rips.result
		.assign attr_result = attr_result|relation_in_pkg_set
	.end for
.end function
.//
.function find_relations_recursively
.param inst_ref pkg_inst
	.select many relation_set related by pkg_inst->PE_PE[R8000]->R_REL[R8001]
	.select many internal_pkg_set related by pkg_inst->PE_PE[R8000]->EP_PKG[R8001]
	.for each internal_pkg in internal_pkg_set
		.invoke nr = find_relations_recursively(internal_pkg)
		.assign nested_relations = nr.result
		.assign relation_set = relation_set|nested_relations
	.end for
	.assign attr_result = relation_set
.end function
.//
.function find_functions_recursively
.param inst_ref pkg_inst
	.select many fun_set related by pkg_inst->PE_PE[R8000]->S_SYNC[R8001]
	.select many internal_pkg_set related by pkg_inst->PE_PE[R8000]->EP_PKG[R8001]
	.for each internal_pkg in internal_pkg_set
		.invoke nf = find_functions_recursively(internal_pkg)
		.assign nested_funs = nr.result
		.assign fun_set = fun_set|nested_funs
	.end for
	.assign attr_result = fun_set
.end function
.//
.function find_ees_recursively
.param inst_ref pkg_inst
	.select many ee_set related by pkg_inst->PE_PE[R8000]->S_EE[R8001]
	.select many internal_pkg_set related by pkg_inst->PE_PE[R8000]->EP_PKG[R8001]
	.for each internal_pkg in internal_pkg_set
		.invoke nee = find_ees_recursively(internal_pkg)
		.assign nested_ees = nee.result
		.assign ee_set = ee_set|nested_ees
	.end for
	.assign attr_result = ee_set
.end function
.//
.function find_all_bodies_within_component
.param inst_ref comp_inst
	.select many body_set from instances of ACT_ACT where false
	.//Provided port bodies
	.select many provided_port_signal_body_set related by comp_inst->C_PO[R4010]->C_IR[R4016]->C_P[R4009]->SPR_PEP[R4501]->SPR_PS[R4503]->ACT_PSB[R686]->ACT_ACT[R698]
	.assign psb_num =  cardinality provided_port_signal_body_set
	.//print "Component ${comp_inst.Name} has provided signal bodies: ${psb_num}"
	.assign body_set = body_set|provided_port_signal_body_set
	.select many provided_port_operation_body_set related by comp_inst->C_PO[R4010]->C_IR[R4016]->C_P[R4009]->SPR_PEP[R4501]->SPR_PO[R4503]->ACT_POB[R687]->ACT_ACT[R698]
	.assign pob_num =  cardinality provided_port_operation_body_set
	.//print "Component ${comp_inst.Name} has provided operation bodies: ${pob_num}"
	.assign body_set = body_set|provided_port_operation_body_set
	.//Required port bodies
	.select many required_port_signal_body_set related by comp_inst->C_PO[R4010]->C_IR[R4016]->C_R[R4009]->SPR_REP[R4500]->SPR_RS[R4502]->ACT_RSB[R684]->ACT_ACT[R698]
	.assign rsb_num =  cardinality required_port_signal_body_set
	.//print "Component ${comp_inst.Name} has required signal bodies: ${rsb_num}"
	.assign body_set = body_set|required_port_signal_body_set
	.select many required_port_operation_body_set related by comp_inst->C_PO[R4010]->C_IR[R4016]->C_R[R4009]->SPR_REP[R4500]->SPR_RO[R4502]->ACT_ROB[R685]->ACT_ACT[R698]
	.assign rob_num =  cardinality required_port_operation_body_set
	.//print "Component ${comp_inst.Name} has required operation bodies: ${rob_num}"
	.assign body_set = body_set|required_port_operation_body_set
	.//In function body
	.select many root_pkg_set related by comp_inst->PE_PE[R8003]->EP_PKG[R8001]
	.select many fun_set from instances of S_SYNC where false
	.for each root_pkg in root_pkg_set
		.invoke fs = find_functions_recursively(root_pkg)
		.assign fun_set = fun_set|fs.result
	.end for
	.select many fun_body_set related by fun_set->ACT_FNB[R695]->ACT_ACT[R698]
	.assign fb_num =  cardinality fun_body_set
	.//print "Component ${comp_inst.Name} has function bodies: ${fb_num}"
	.assign body_set = body_set|fun_body_set
	.//Class operation bodies
	.select many cls_set from instances of O_OBJ where false
	.for each root_pkg in root_pkg_set
		.invoke fcr = find_classes_recursively(root_pkg)
		.assign cls_set = cls_set|fcr.result
	.end for
	.select many op_body_set related by cls_set->O_TFR[R115]->ACT_OPB[R696]->ACT_ACT[R698]
	.assign ob_num =  cardinality op_body_set
	.//print "Component ${comp_inst.Name} has class operation bodies: ${ob_num}"
	.assign body_set = body_set|op_body_set
	.//Derr attr bodies
	.select many derattr_body_set related by cls_set->O_ATTR[R102]->O_BATTR[R106]->O_DBATTR[R107]->ACT_DAB[R693]->ACT_ACT[R698]
	.assign dab_num =  cardinality derattr_body_set
	.//print "Component ${comp_inst.Name} has derived attributes bodies: ${dab_num}"
	.assign body_set = body_set|derattr_body_set
	.//Bridge bodies
	.select many ee_set from instances of S_EE where false
	.for each root_pkg in root_pkg_set
		.invoke fees = find_ees_recursively(root_pkg)
		.assign ee_set = ee_set|fees.result
	.end for
	.select many bridge_body_set related by ee_set->S_BRG[R19]->ACT_BRB[R697]->ACT_ACT[R698]
	.assign bb_num =  cardinality bridge_body_set
	.//print "Component ${comp_inst.Name} has bridge bodies: ${bb_num}"
	.assign body_set = body_set|bridge_body_set
	.//State bodies
	.select many state_body_set related by cls_set->SM_ISM[R518]->SM_SM[R517]->SM_ACT[R515]->ACT_SAB[R691]->ACT_ACT[R698]
	.assign sb_num =  cardinality state_body_set
	.//print "Component ${comp_inst.Name} has state bodies: ${sb_num}"
	.assign body_set = body_set|state_body_set
	.//Transition bodies
	.select many txn_body_set related by cls_set->SM_ISM[R518]->SM_SM[R517]->SM_ACT[R515]->ACT_TAB[R688]->ACT_ACT[R698]
	.assign tb_num =  cardinality txn_body_set
	.//print "Component ${comp_inst.Name} has transition bodies: ${tb_num}"
	.assign body_set = body_set|txn_body_set
	.assign body_set_num =  cardinality body_set
	.print "Component ${comp_inst.Name} has TOTAL bodies: ${body_set_num}"
	.assign attr_result = body_set	
.end function
.//------------------------------------------------------------------------------------
.//------------------------------------------------------------------------------------
.// END HELPER FUNCTIONS
.//------------------------------------------------------------------------------------
.//------------------------------------------------------------------------------------
.//
.invoke pl = GET_ENV_VAR("PROJECT_LOC")
.invoke c = GET_ENV_VAR("COMP")
.if(pl.success and c.success)
	.print "======================================================================================"
	.assign project_location = pl.result	
	.print "Using project location ${project_location}."
	.assign comp_name = c.result
	.print "Calculating metrics for component ${comp_name}"
	.print "======================================================================================"
	.//--------------------------------------------------------------------------------
	.//INCLUSIONS
	.//include "${project_location}/utils/q.general.utils.arc"
	.//include "${project_location}/utils/q.cc.horizontal.arc"
	.//include "${project_location}/utils/q.cc.vertical.arc"
	.//include "${project_location}/utils/q.loc.horizontal.arc"
	.//include "${project_location}/utils/q.printing.arc"
	.//--------------------------------------------------------------------------------
	.select any comp_inst from instances of C_C where selected.Name==comp_name
	.if(not_empty comp_inst)
		.//--------------------------------------------------------------------------------
		.//LOC: horizontal distribution
		.print "======================================================================================"
		.print "Generating class LOC for horizontal distribution:"
		.invoke t1 = generate_loc_comp_class_bodies(comp_inst, project_location)
		.assign Lines = t1.body
		.include "${project_location}/utils/t.part.general"
		.emit to file "C:/temp/${comp_inst.Name}_class_loc.csv"
		.print "======================================================================================"
		.print "Generating component body LOC for horizontal distribution:"
		.invoke t2 = generate_loc_comp_bodies(comp_inst, project_location)
		.assign Lines = t2.body
		.include "${project_location}/utils/t.part.general"
		.emit to file "C:/temp/${comp_inst.Name}_bodies_loc.csv"
	.else
		.print "ERROR: Component ${comp_name} does not exist."
	.end if
.else
	.print "Could not find project location. Giving up."
.end if	
