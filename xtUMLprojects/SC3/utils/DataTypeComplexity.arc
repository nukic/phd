.//-------------------------------------------------------------------------------------------
.function count_structured_type_primitive_members
.param inst_ref sdt
	.select one sdt_dt related by sdt->S_DT[R17]
	.assign attr_result = 0
	.select many member_set related by sdt->S_MBR[R44]
	.for each member in member_set
		.select one core_member_type related by member->S_DT[R45]->S_CDT[R17]
		.if(not_empty core_member_type)
			.print "Structure data type ${sdt_dt.Name} has member ${member.Name} of core data type!"
			.assign attr_result = attr_result + 1
		.end if
		.//
		.select one enum_member_type related by member->S_DT[R45]->S_EDT[R17]
		.if(not_empty enum_member_type)
			.print "Structure data type ${sdt_dt.Name} has member ${member.Name} of enumerated data type!"
			.assign attr_result = attr_result + 1
		.end if
		.//
		.select one irdt_member_type related by member->S_DT[R45]->S_IRDT[R17]
		.if(not_empty irdt_member_type)
			.print "ERROR: Structure data type ${sdt_dt.Name} has member ${member.Name} of instance reference data type!"
		.end if
		.select one sdt_member_type related by member->S_DT[R45]->S_SDT[R17]
		.if(not_empty sdt_member_type)
			.print "ERROR: Structure data type ${sdt_dt.Name} has member ${member.Name} of structured data type!"
			.invoke sdtmc = count_structured_type_primitive_members(sdt_member_type)
			.assign attr_result = attr_result + sdtmc.result
		.end if
	.end for
	.print "Structure data type ${sdt_dt.Name} has ${attr_result} primitive members"
.end function
.//
.function find_classes_in_component
.param inst_ref comp_inst
	.select many attr_result from instances of O_OBJ where false
	.select many root_pkg_set related by comp_inst->PE_PE[R8003]->EP_PKG[R8001]
	.for each root_pkg in root_pkg_set
		.invoke cips = find_classes_recursively(root_pkg)
		.assign class_in_pkg_set = cips.result
		.assign attr_result = attr_result|class_in_pkg_set
	.end for
.end function
.//
.function find_classes_recursively
.param inst_ref pkg_inst
	.select many class_set related by pkg_inst->PE_PE[R8000]->O_OBJ[R8001]
	.select many internal_pkg_set related by pkg_inst->PE_PE[R8000]->EP_PKG[R8001]
	.for each internal_pkg in internal_pkg_set
		.invoke ns = find_classes_recursively(internal_pkg)
		.assign nested_classes = ns.result
		.assign class_set = class_set|nested_classes
	.end for
	.assign attr_result = class_set
.end function
.//-------------------------------------------------------------------------------------------
.invoke pl = GET_ENV_VAR("PROJECT_LOC")
.invoke c = GET_ENV_VAR("COMP")
.if(pl.success and c.success)
	.print "======================================================================================"
	.assign project_location = pl.result	
	.print "Using project location ${project_location}."
	.assign comp_name = c.result
	.print "Calculating metrics for component ${comp_name}"
	.print "======================================================================================"
	.//--------------------------------------------------------------------------------
	.//INCLUSIONS
	.//include "${project_location}/utils/q.general.utils.arc"
	.//include "${project_location}/utils/q.cc.horizontal.arc"
	.//include "${project_location}/utils/q.cc.vertical.arc"
	.//include "${project_location}/utils/q.loc.horizontal.arc"
	.//include "${project_location}/utils/q.printing.arc"
	.select any comp_inst from instances of C_C where selected.Name==comp_name
	.if(not_empty comp_inst)
		.print "Starting..."
		.invoke cic = find_classes_in_component(comp_inst)
		.assign class_set = cic.result
		.assign total_primitive_values = 0
		.for each class in class_set
			.assign cls_primitive_values = 0
			.select many attr_set related by class->O_ATTR[R102]
			.for each attr in attr_set
				.if(attr.Name!="current_state")
					.select one cdt_type related by attr->S_DT[R114]->S_CDT[R17]
					.if(not_empty cdt_type)
						.print "Class ${class.Name} has member ${attr.Name} of core data type!"
						.assign cls_primitive_values = cls_primitive_values + 1
					.end if
					.//
					.select one enum_attr_type related by attr->S_DT[R114]->S_EDT[R17]
					.if(not_empty enum_attr_type)
						.print "Class ${class.Name} has attr ${attr.Name} of enumerated data type!"
						.assign cls_primitive_values = cls_primitive_values + 1
					.end if
					.//
					.select one irdt_attr_type related by attr->S_DT[R114]->S_IRDT[R17]
					.if(not_empty irdt_attr_type)
						.print "ERROR: Class ${class.Name} has attr ${attr.Name} of instance reference data type!"
					.end if
					.//
					.select one sdt_attr_type related by attr->S_DT[R114]->S_SDT[R17]
					.if(not_empty sdt_attr_type)
						.print "ERROR: Class ${class.Name} has attr ${attr.Name} of structured data type!"
						.invoke sdtmc = count_structured_type_primitive_members(sdt_attr_type)
						.assign cls_primitive_values = cls_primitive_values + sdtmc.result
					.end if
				.end if
			.end for
			.print "${class.Key_Lett} ${cls_primitive_values}"
			.//Count the number of relations
			.select many rel_set related by class->R_OIR[R201]->R_REL[R201]
			.assign rel_num = cardinality rel_set
			.print "Class ${class.Name} is involved in ${rel_num} relations."
			.assign cls_primitive_values = cls_primitive_values + rel_num
${class.Key_Lett} ${cls_primitive_values}
			.assign total_primitive_values = total_primitive_values + cls_primitive_values
		.end for
		.print "TOTAL ${total_primitive_values}"
TOTAL ${total_primitive_values}
		.print "DONE!"
		.select any sys from instances of S_SYS
		.print "Sys.ID = ${sys.Sys_ID}"
		.emit to file "C:/Temp/${sys.Name}_DataTypeComplexityPerClasses.csv"
	.else
		.print "ERROR: Component ${comp_name} does not exist."
	.end if
.else
	.print "Could not find project location. Giving up."
.end if	