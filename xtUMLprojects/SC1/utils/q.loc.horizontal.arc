.function export_loc_comp_class_bodies
.param inst_ref comp_inst
.param string project_loc
	.invoke cic = find_classes_in_component(comp_inst)
	.assign class_set = cic.result
	.assign loc_all_classes = 0
	.for each class in class_set
		.invoke ccc = calculate_loc_single_class(class)
		.assign loc_class = ccc.result
		.assign key = "${class.Name}"
		.assign value = "${loc_class}"
		.include "${project_loc}/utils/t.key_value.csv"
		.assign loc_all_classes = loc_all_classes + loc_class
	.end for
.end function
.//
.function export_loc_comp_bodies
.param inst_ref comp_inst
.param string project_loc
	.assign attr_result = 0;
	.invoke bs = find_all_bodies_within_component(comp_inst)
	.assign body_set = bs.result
	.assign body_num = cardinality body_set
	.//print "Component ${comp_inst.Name} has ${body_num} bodies."
	.select many non_empty_body_set related by body_set->ACT_BLK[R601]->ACT_SMT[R602]->ACT_BLK[R602]->ACT_ACT[R601]
	.assign non_empty_body_num = cardinality non_empty_body_set
	.//print "Component ${comp_inst.Name} has ${non_empty_body_num} non-empty bodies."
	.for each body in non_empty_body_set
		.invoke cbc = calculate_loc_single_body(body)	
		.assign loc_current_body = cbc.result
		.assign key =  "${body.Type}_${body.label}"
		.assign value = "${loc_current_body}"
		.include "${project_loc}/utils/t.key_value.csv"	
		.assign attr_result = attr_result+loc_current_body
	.end for
	.assign key =  "TOTAL NUMBER OF NON_EMPTY BODIES: "
	.assign value = "${non_empty_body_num}"
	.include "${project_loc}/utils/t.key_value.csv"
	.assign key =  "TOTAL LOC OF COMP BODIES: "
	.assign value = "${attr_result}"
	.include "${project_loc}/utils/t.key_value.csv"
.end function
.//------------------------------------------------------------------------------------
.//------------------------------------------------------------------------------------
.// HELPER FUNCTIONS
.//------------------------------------------------------------------------------------
.//------------------------------------------------------------------------------------
.function calculate_loc_single_class
.param inst_ref cls_inst
	.assign attr_result = 0
	.invoke cbc = calculate_loc_class_bodies(cls_inst)
	.assign attr_result = cbc.result	
	.//print "Class ${cls_inst.Name} (with state machine and bodies) contributes to cyclomatic complexity: ${attr_result}"
.end function
.//
.function calculate_loc_comp_body
.param inst_ref comp_inst
	.assign attr_result = 0;
	.invoke bs = find_all_bodies_within_component(comp_inst)
	.assign body_set = bs.result
	.assign body_num = cardinality body_set
	.//print "Component ${comp_inst.Name} has ${body_num} bodies."
	.select many non_empty_body_set related by body_set->ACT_BLK[R601]->ACT_SMT[R602]->ACT_BLK[R602]->ACT_ACT[R601]
	.assign non_empty_body_num = cardinality non_empty_body_set
	.//print "Component ${comp_inst.Name} has ${non_empty_body_num} non-empty bodies."
	.for each body in non_empty_body_set
		.invoke cbc = calculate_loc_single_body(body)	
		.assign loc_current_body = cbc.result	
		.assign attr_result = attr_result+loc_current_body
	.end for
.end function
.//
.function calculate_loc_class_bodies
.param inst_ref cls_inst
	.assign attr_result = 0
	.assign locTotal = 0
	.select many body_set from instances of ACT_ACT where false
	.//Class operation bodies
	.select many op_body_set related by cls_inst->O_TFR[R115]->ACT_OPB[R696]->ACT_ACT[R698]
	.assign ob_num =  cardinality op_body_set
	.print "Class ${cls_inst.Name} has class operation bodies: ${ob_num}"
	.assign body_set = body_set|op_body_set
	.//Derr attr bodies
	.select many derattr_body_set related by cls_inst->O_ATTR[R102]->O_BATTR[R106]->O_DBATTR[R107]->ACT_DAB[R693]->ACT_ACT[R698]
	.assign dab_num =  cardinality derattr_body_set
	.print "Class ${cls_inst.Name} has derived attributes bodies: ${dab_num}"
	.assign body_set = body_set|derattr_body_set
	.//State bodies
	.select many state_body_set related by cls_inst->SM_ISM[R518]->SM_SM[R517]->SM_ACT[R515]->ACT_SAB[R691]->ACT_ACT[R698]
	.assign sb_num =  cardinality state_body_set
	.print "Class ${cls_inst.Name} has state bodies: ${sb_num}"
	.assign body_set = body_set|state_body_set
	.//Transition bodies
	.select many txn_body_set related by cls_inst->SM_ISM[R518]->SM_SM[R517]->SM_ACT[R515]->ACT_TAB[R688]->ACT_ACT[R698]
	.assign tb_num =  cardinality txn_body_set
	.print "Class ${cls_inst.Name} has transition bodies: ${tb_num}"
	.assign body_set = body_set|txn_body_set
	.//
	.for each body in body_set
		.invoke dac = calculate_loc_single_body(body)
		.assign locTotal = locTotal + dac.result
	.end for
	.assign attr_result = locTotal
	.print "Class ${cls_inst.Name} has processing model complexity of: ${attr_result}"
.end function
.//
.function calculate_loc_single_body
.param inst_ref body
	.assign attr_result = 0
	.select many stmt_set related by body->ACT_BLK[R601]->ACT_SMT[R602]
	.assign attr_result =  cardinality stmt_set
	.print "Body of ${body.Type} ${body.label} has total of ${attr_result} statements."
.end function
.//