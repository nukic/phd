.//
.function print_cc_vertical_summary
.param inst_ref comp_inst
.param integer cc_comp_port
.param integer cc_comp_cls
.param integer cc_comp_sm
.param integer cc_comp_body
	.assign cc_graphical = (cc_comp_port + cc_comp_sm) + cc_comp_cls
	.assign cc_textual = cc_comp_body
	.assign cc_ratio = ((cc_graphical/1.0)/(cc_textual/1.0))*100
	.assign cc_total = ((cc_comp_body + cc_comp_sm) + cc_comp_port)
	.assign cc_comp_port_rel =((cc_comp_port/1.0)/(cc_total/1.0))*100 
	.assign cc_comp_cls_rel = ((cc_comp_cls/1.0)/(cc_total/1.0))*100
	.assign cc_comp_sm_rel =((cc_comp_sm/1.0)/(cc_total/1.0))*100
	.assign cc_comp_body_rel = ((cc_comp_body/1.0)/(cc_total/1.0))*100
	.print "======================================================================================"
	.print "Component ${comp_inst.Name} has COMPONENT MODEL cyclomatic complexity: ${cc_comp_port} (${cc_comp_port_rel} %)"
	.print "Component ${comp_inst.Name} has CLASS MODELcyclomatic complexity: ${cc_comp_cls} (${cc_comp_cls_rel} %)"
	.print "Component ${comp_inst.Name} has SM MODELcyclomatic complexity: ${cc_comp_sm} (${cc_comp_sm_rel} %)"
	.print "Component ${comp_inst.Name} has PROCESSING MODEL cyclomatic complexity: ${cc_comp_body} (${cc_comp_body_rel} %)"
	.print "--------------------------------------------------------------------------------------"
	.print "Component ${comp_inst.Name} has graphical-to-textual cyclomatic complexity ratio: ${cc_graphical}/${cc_textual} = ${cc_ratio} %"
	.print "--------------------------------------------------------------------------------------"	
	.print "Component ${comp_inst.Name} has TOTAL cyclomatic complexity: CC_total = ${cc_total}"
	.print "======================================================================================"
.end function
.//