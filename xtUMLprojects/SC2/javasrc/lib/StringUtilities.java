package lib;

public class StringUtilities {

	public static String intToString(int i){
		return ""+i;
	}

	
	public static String realToString(float r, int decimalPoints){
		//System.out.println("Rounding number " + r + " to " + decimalPoints + " decimal places!");
		return String.format("%."+decimalPoints+"f", r).replace(",", ".");
	}
	
	public static int stringToInt(String s){
		return Integer.parseInt(s);
	}
	
	public static float stringToReal(String s){
		return Float.parseFloat(s);
	}
}
