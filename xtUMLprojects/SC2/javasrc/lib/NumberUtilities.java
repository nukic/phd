package lib;

import com.mentor.nucleus.bp.core.CorePlugin;

public class NumberUtilities {
	

	public static float getBase10PowerAmount(int power) {
		return (float) Math.pow(10, power);
	}

	public static double round(float value, int decimalPlaces) {

		int powerAmount = (int) Math.pow(10, decimalPlaces);
		return Math.round(value * powerAmount) / powerAmount;
	}

	public static int roundToInt(float value) {
		return Math.round(value);
	}

	public static int floor(float value) {
		return (int) Math.floor(value);
	}

	
	public static int ceiling(float value) {
		return (int) Math.ceil(value);
	}

	public static int getPrecision(float value) {
		return (int) Math.ceil(value);
	}
	
	public static int getNumberOfDecimalPlaces(float num) {
		String numStr = "" + num;
		return numStr.split("\\.")[1].length();
		
	}
	
	public static void printInteger(int value){
			CorePlugin.out.print(value + " ");
	}

	
}
