.invoke pl = GET_ENV_VAR("PROJECT_LOC")
.invoke c = GET_ENV_VAR("COMP")
.if(pl.success and c.success)
	.print "======================================================================================"
	.assign project_location = pl.result	
	.print "Using project location ${project_location}."
	.assign comp_name = c.result
	.print "Calculating metrics for component ${comp_name}"
	.print "======================================================================================"
	.//--------------------------------------------------------------------------------
	.//INCLUSIONS
	.include "${project_location}/utils/q.general.utils.arc"
	.include "${project_location}/utils/q.cc.horizontal.arc"
	.include "${project_location}/utils/q.cc.vertical.arc"
	.include "${project_location}/utils/q.loc.horizontal.arc"
	.include "${project_location}/utils/q.printing.arc"
	.//--------------------------------------------------------------------------------
	.select any comp_inst from instances of C_C where selected.Name==comp_name
	.if(not_empty comp_inst)
		.//--------------------------------------------------------------------------------
		.//Cyclomatic complexity: vertical distribution
		.invoke cpc = calculate_cc_comp_port(comp_inst)
		.assign cc_comp_port= cpc.result
		.invoke ccc = calculate_cc_comp_class(comp_inst)
		.assign cc_comp_cls = ccc.result
		.invoke csmc = calculate_cc_comp_sm(comp_inst)
		.assign cc_comp_sm = csmc.result
		.invoke cbc = calculate_cc_comp_body(comp_inst)
		.assign cc_comp_body = cbc.result	
		.invoke print_cc_vertical_summary(comp_inst, cc_comp_port, cc_comp_cls, cc_comp_sm, cc_comp_body)
		.//--------------------------------------------------------------------------------
		.//Cyclomatic complexity: horizontal distribution
		.print "======================================================================================"
		.print "Exporting class cyclomatic complexity for horizontal distribution:"
		.invoke t1 = export_cc_comp_class(comp_inst, project_location)
		.assign Lines = t1.body
		.include "${project_location}/utils/t.part.general"
		.emit to file "C:/temp/${comp_inst.Name}_class.csv"
		.print "======================================================================================"
		.print "Component body cyclomatic complexity for horizontal distribution:"
		.invoke t2 = export_cc_comp_body_decisions_and_calls(comp_inst, project_location)
		.assign Lines = t2.body
		.include "${project_location}/utils/t.part.general"
		.emit to file "C:/temp/${comp_inst.Name}_bodies.csv"
		.//--------------------------------------------------------------------------------
		.//LOC: horizontal distribution
		.print "======================================================================================"
		.print "Exporting class LOC for horizontal distribution:"
		.invoke t1 = export_loc_comp_class_bodies(comp_inst, project_location)
		.assign Lines = t1.body
		.include "${project_location}/utils/t.part.general"
		.emit to file "C:/temp/${comp_inst.Name}_class_loc.csv"
		.print "======================================================================================"
		.print "Exporting component body LOC for horizontal distribution:"
		.invoke t2 = export_loc_comp_bodies(comp_inst, project_location)
		.assign Lines = t2.body
		.include "${project_location}/utils/t.part.general"
		.emit to file "C:/temp/${comp_inst.Name}_bodies_loc.csv"
	.else
		.print "ERROR: Component ${comp_name} does not exist."
	.end if
.else
	.print "Could not find project location. Giving up."
.end if	
