.//--------------------------------------------------------------------------------
.// Component complexity = 2*Nop + Nsig
.//--------------------------------------------------------------------------------
.function calculate_cc_comp_port
.param inst_ref comp_inst
	.assign attr_result = 0;
	.select many signal_set related by comp_inst->C_PO[R4010]->C_IR[R4016]->C_I[R4012]->C_EP[R4003]->C_AS[R4004]
	.select many operation_set related by comp_inst->C_PO[R4010]->C_IR[R4016]->C_I[R4012]->C_EP[R4003]->C_IO[R4004]
	.assign op_num = cardinality operation_set
	.assign sig_num = cardinality signal_set
	.//print "Component ${comp_inst.Name} has ${op_num} operations and ${sig_num} signals."
	.assign attr_result = ((2 * op_num) + (sig_num))
	.//print "Component ${comp_inst.Name} has (port) cyclomatic complexity: ${attr_result}"
.end function
.//
.//-------------------------------------------------------------------------------
.// State machine cyclomatic complexity = \sum(s)[Nevt-Nei(s)] + Nstei
.//-------------------------------------------------------------------------------
.function calculate_cc_comp_sm
.param inst_ref comp_inst
	.assign attr_result = 0;
	.invoke cic = find_classes_in_component(comp_inst)
	.assign class_set = cic.result
	.for each class in class_set
		.invoke csc = calculate_cc_class_sm(class)
		.assign cc_sm_class = csc.result
		.//print "Class ${class.Name} has state machine cyclomatic complexity: ${cc_sm_class} = ${state_num} * ${evt_num} - ${ei_num}"
		.assign attr_result = attr_result + cc_sm_class
	.end for
.end function
.//
.//-------------------------------------------------------------------------------
.// Class relation cyclomatic complexity
.//-------------------------------------------------------------------------------
.function calculate_cc_comp_class
.param inst_ref comp_inst
	.assign attr_result = 0;
	.invoke ric = find_relations_in_component(comp_inst)
	.assign relation_set = ric.result
	.assign rel_num = cardinality relation_set
	.for each rel in relation_set
		.assign cc_rel = 0
		.select one simple_rel related by rel->R_SIMP[R206]
		.select one assoc_rel related by rel->R_ASSOC[R206]
		.select one subsup_rel related by rel->R_SUBSUP[R206]
		.if(not_empty simple_rel)
			.select many participant_set related by simple_rel->R_PART[R207]
			.select one formalizer related by simple_rel->R_FORM[R208]
			.for each participant in participant_set
				.if(participant.Cond==1)
					.//print "Simple relation ${rel.Numb} is conditional on participant side."
					.assign cc_rel = cc_rel+1
				.end if
				.if(participant.Mult==1)
					.//print "Simple relation ${rel.Numb} is multiple on participant side."
					.assign cc_rel = cc_rel+1
				.end if
			.end for
			.if(not_empty formalizer)
				.if(formalizer.Cond==1)
					.//print "Simple relation ${rel.Numb} is conditional on formalizer side."
					.assign cc_rel = cc_rel+1
				.end if
				.if(formalizer.Mult==1)
					.//print "Simple relation ${rel.Numb} is multiple on formalizer side."
					.assign cc_rel = cc_rel+1
				.end if
			.end if
		.elif(not_empty assoc_rel)
			.select one one_side related by assoc_rel->R_AONE[R209]
			.select one other_side related by assoc_rel->R_AOTH[R210]
			.select one assoc_cls_side related by assoc_rel->R_ASSR[R211]
			.if(one_side.Cond==1)
				.//print "One side of associtive relation ${rel.Numb} is conditional."
				.assign cc_rel = cc_rel+1
			.end if
			.if(one_side.Mult==1)
				.//print "One side of associtive relation ${rel.Numb} is multiple."
				.assign cc_rel = cc_rel+1
			.end if
			.if(other_side.Cond==1)
				.//print "Other side of associtive relation ${rel.Numb} is conditional."
				.assign cc_rel = cc_rel+1
			.end if
			.if(other_side.Mult==1)
				.//print "Other side of associtive relation ${rel.Numb} is multiple."
				.assign cc_rel = cc_rel+1
			.end if
			.if(assoc_cls_side.Mult==1)
				.//print "Associative side of associtive relation ${rel.Numb} is multiple."
				.assign cc_rel = cc_rel+2
			.end if
		.elif(not_empty subsup_rel)
			.select many subclass_set related by subsup_rel->R_SUB[R213]
			.assign subcls_num =  (cardinality subclass_set)
			.//print "Generalization relation ${rel.Numb} defines ${subcls_num} subclasses."
			.assign cc_rel = cc_rel+subcls_num
		.else
			.print "ERROR: Relation ${rel.Numb} has unknown type! Ignoring it."
		.end if
		.//print "Relation ${rel.Numb} contributes to class cyclomtic complexity with: + ${cc_rel}"
		.assign attr_result = attr_result + cc_rel
	.end for
.end function
.//
.//-------------------------------------------------------------------------------
.// Processing code cyclomatic complexity: Nd + Ncall - Nbody + 2
.//-------------------------------------------------------------------------------
.//This should work but it does not:
.//select many body_set related by comp_inst->ACT_BIC[R694]->ACT_ACT[R694]
.//R694 not populated at all? There is complicated workarund to find all bodies manually
.//Workaround: select bodies manually
.function calculate_cc_comp_body
.param inst_ref comp_inst
	.assign attr_result = 0;
	.invoke bs = find_all_bodies_within_component(comp_inst)
	.assign body_set = bs.result
	.assign body_num = cardinality body_set
	.//print "Component ${comp_inst.Name} has ${body_num} bodies."
	.select many non_empty_body_set related by body_set->ACT_BLK[R601]->ACT_SMT[R602]->ACT_BLK[R602]->ACT_ACT[R601]
	.assign non_empty_body_num = cardinality non_empty_body_set
	.//print "Component ${comp_inst.Name} has ${non_empty_body_num} non-empty bodies."
	.for each body in non_empty_body_set
		.invoke cbc = calculate_cc_single_body(body)	
		.assign cc_current_body = cbc.result	
		.assign attr_result = attr_result+cc_current_body
	.end for
	.assign totalDecisionsAndCalls = attr_result
	.assign attr_result = ((attr_result-non_empty_body_num)+2)
.end function
.//