.//------------------------------------------------------------------------------------
.//------------------------------------------------------------------------------------
.// FUNCTIONS
.//------------------------------------------------------------------------------------
.//------------------------------------------------------------------------------------
.function find_classes_in_component
.param inst_ref comp_inst
	.select many attr_result from instances of O_OBJ where false
	.select many root_pkg_set related by comp_inst->PE_PE[R8003]->EP_PKG[R8001]
	.for each root_pkg in root_pkg_set
		.invoke cips = find_classes_recursively(root_pkg)
		.assign class_in_pkg_set = cips.result
		.assign attr_result = attr_result|class_in_pkg_set
	.end for
.end function
.//
.function find_classes_recursively
.param inst_ref pkg_inst
	.select many class_set related by pkg_inst->PE_PE[R8000]->O_OBJ[R8001]
	.select many internal_pkg_set related by pkg_inst->PE_PE[R8000]->EP_PKG[R8001]
	.for each internal_pkg in internal_pkg_set
		.invoke ns = find_classes_recursively(internal_pkg)
		.assign nested_classes = ns.result
		.assign class_set = class_set|nested_classes
	.end for
	.assign attr_result = class_set
.end function
.//
.function find_relations_in_component
.param inst_ref comp_inst
	.select many attr_result from instances of R_REL where false
	.select many root_pkg_set related by comp_inst->PE_PE[R8003]->EP_PKG[R8001]
	.for each root_pkg in root_pkg_set
		.invoke rips = find_relations_recursively(root_pkg)
		.assign relation_in_pkg_set = rips.result
		.assign attr_result = attr_result|relation_in_pkg_set
	.end for
.end function
.//
.function find_relations_recursively
.param inst_ref pkg_inst
	.select many relation_set related by pkg_inst->PE_PE[R8000]->R_REL[R8001]
	.select many internal_pkg_set related by pkg_inst->PE_PE[R8000]->EP_PKG[R8001]
	.for each internal_pkg in internal_pkg_set
		.invoke nr = find_relations_recursively(internal_pkg)
		.assign nested_relations = nr.result
		.assign relation_set = relation_set|nested_relations
	.end for
	.assign attr_result = relation_set
.end function
.//
.function find_functions_recursively
.param inst_ref pkg_inst
	.select many fun_set related by pkg_inst->PE_PE[R8000]->S_SYNC[R8001]
	.select many internal_pkg_set related by pkg_inst->PE_PE[R8000]->EP_PKG[R8001]
	.for each internal_pkg in internal_pkg_set
		.invoke nf = find_functions_recursively(internal_pkg)
		.assign nested_funs = nr.result
		.assign fun_set = fun_set|nested_funs
	.end for
	.assign attr_result = fun_set
.end function
.//
.function find_ees_recursively
.param inst_ref pkg_inst
	.select many ee_set related by pkg_inst->PE_PE[R8000]->S_EE[R8001]
	.select many internal_pkg_set related by pkg_inst->PE_PE[R8000]->EP_PKG[R8001]
	.for each internal_pkg in internal_pkg_set
		.invoke nee = find_ees_recursively(internal_pkg)
		.assign nested_ees = nee.result
		.assign ee_set = ee_set|nested_ees
	.end for
	.assign attr_result = ee_set
.end function
.//
.function find_all_bodies_within_component
.param inst_ref comp_inst
	.select many body_set from instances of ACT_ACT where false
	.//Provided port bodies
	.select many provided_port_signal_body_set related by comp_inst->C_PO[R4010]->C_IR[R4016]->C_P[R4009]->SPR_PEP[R4501]->SPR_PS[R4503]->ACT_PSB[R686]->ACT_ACT[R698]
	.assign psb_num =  cardinality provided_port_signal_body_set
	.//print "Component ${comp_inst.Name} has provided signal bodies: ${psb_num}"
	.assign body_set = body_set|provided_port_signal_body_set
	.select many provided_port_operation_body_set related by comp_inst->C_PO[R4010]->C_IR[R4016]->C_P[R4009]->SPR_PEP[R4501]->SPR_PO[R4503]->ACT_POB[R687]->ACT_ACT[R698]
	.assign pob_num =  cardinality provided_port_operation_body_set
	.//print "Component ${comp_inst.Name} has provided operation bodies: ${pob_num}"
	.assign body_set = body_set|provided_port_operation_body_set
	.//Required port bodies
	.select many required_port_signal_body_set related by comp_inst->C_PO[R4010]->C_IR[R4016]->C_R[R4009]->SPR_REP[R4500]->SPR_RS[R4502]->ACT_RSB[R684]->ACT_ACT[R698]
	.assign rsb_num =  cardinality required_port_signal_body_set
	.//print "Component ${comp_inst.Name} has required signal bodies: ${rsb_num}"
	.assign body_set = body_set|required_port_signal_body_set
	.select many required_port_operation_body_set related by comp_inst->C_PO[R4010]->C_IR[R4016]->C_R[R4009]->SPR_REP[R4500]->SPR_RO[R4502]->ACT_ROB[R685]->ACT_ACT[R698]
	.assign rob_num =  cardinality required_port_operation_body_set
	.//print "Component ${comp_inst.Name} has required operation bodies: ${rob_num}"
	.assign body_set = body_set|required_port_operation_body_set
	.//In function body
	.select many root_pkg_set related by comp_inst->PE_PE[R8003]->EP_PKG[R8001]
	.select many fun_set from instances of S_SYNC where false
	.for each root_pkg in root_pkg_set
		.invoke fs = find_functions_recursively(root_pkg)
		.assign fun_set = fun_set|fs.result
	.end for
	.select many fun_body_set related by fun_set->ACT_FNB[R695]->ACT_ACT[R698]
	.assign fb_num =  cardinality fun_body_set
	.//print "Component ${comp_inst.Name} has function bodies: ${fb_num}"
	.assign body_set = body_set|fun_body_set
	.//Class operation bodies
	.select many cls_set from instances of O_OBJ where false
	.for each root_pkg in root_pkg_set
		.invoke fcr = find_classes_recursively(root_pkg)
		.assign cls_set = cls_set|fcr.result
	.end for
	.select many op_body_set related by cls_set->O_TFR[R115]->ACT_OPB[R696]->ACT_ACT[R698]
	.assign ob_num =  cardinality op_body_set
	.//print "Component ${comp_inst.Name} has class operation bodies: ${ob_num}"
	.assign body_set = body_set|op_body_set
	.//Derr attr bodies
	.select many derattr_body_set related by cls_set->O_ATTR[R102]->O_BATTR[R106]->O_DBATTR[R107]->ACT_DAB[R693]->ACT_ACT[R698]
	.assign dab_num =  cardinality derattr_body_set
	.//print "Component ${comp_inst.Name} has derived attributes bodies: ${dab_num}"
	.assign body_set = body_set|derattr_body_set
	.//Bridge bodies
	.select many ee_set from instances of S_EE where false
	.for each root_pkg in root_pkg_set
		.invoke fees = find_ees_recursively(root_pkg)
		.assign ee_set = ee_set|fees.result
	.end for
	.select many bridge_body_set related by ee_set->S_BRG[R19]->ACT_BRB[R697]->ACT_ACT[R698]
	.assign bb_num =  cardinality bridge_body_set
	.//print "Component ${comp_inst.Name} has bridge bodies: ${bb_num}"
	.assign body_set = body_set|bridge_body_set
	.//State bodies
	.select many state_body_set related by cls_set->SM_ISM[R518]->SM_SM[R517]->SM_ACT[R515]->ACT_SAB[R691]->ACT_ACT[R698]
	.assign sb_num =  cardinality state_body_set
	.//print "Component ${comp_inst.Name} has state bodies: ${sb_num}"
	.assign body_set = body_set|state_body_set
	.//Transition bodies
	.select many txn_body_set related by cls_set->SM_ISM[R518]->SM_SM[R517]->SM_ACT[R515]->ACT_TAB[R688]->ACT_ACT[R698]
	.assign tb_num =  cardinality txn_body_set
	.//print "Component ${comp_inst.Name} has transition bodies: ${tb_num}"
	.assign body_set = body_set|txn_body_set
	.assign body_set_num =  cardinality body_set
	.assign attr_result = body_set	
.end function
.//------------------------------------------------------------------------------------
.//------------------------------------------------------------------------------------
.// END OF FUNCTIONS
.//------------------------------------------------------------------------------------
.//------------------------------------------------------------------------------------