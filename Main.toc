\contentsline {section}{Abstract}{iv}{section*.3}
\contentsline {section}{Sa\v {z}etak}{v}{section*.4}
\contentsline {section}{List of Tables}{xiv}{section*.6}
\contentsline {section}{List of Figures}{xviii}{section*.7}
\contentsline {section}{List of Acronyms}{xix}{section*.8}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Background and related work}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}xtUML}{3}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Components}{3}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Classes}{5}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}State machines}{7}{subsection.2.1.3}
\contentsline {subsection}{\numberline {2.1.4}Processing code}{9}{subsection.2.1.4}
\contentsline {section}{\numberline {2.2}Other executable software methodologies}{13}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Real-time Object-Oriented Modeling (ROOM) methodology}{14}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Foundational Subset for Executable UML Models (fUML)}{14}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Action Language for fUML (ALF)}{14}{subsection.2.2.3}
\contentsline {section}{\numberline {2.3}Software understandability}{15}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Software understandability factors}{15}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Software understanding process and theories}{16}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}A relation between software metrics and understandability}{17}{subsection.2.3.3}
\contentsline {section}{\numberline {2.4}Cyclomatic complexity}{18}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Cyclomatic complexity and modularization}{20}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}Cyclomatic complexity for modules with multiple entry and/or exit nodes}{22}{subsection.2.4.2}
\contentsline {subsection}{\numberline {2.4.3}A critique of cyclomatic complexity as a software metric}{23}{subsection.2.4.3}
\contentsline {section}{\numberline {2.5}Entropy-based complexity metrics}{25}{section.2.5}
\contentsline {section}{\numberline {2.6}Data and information flow complexity metrics}{26}{section.2.6}
\contentsline {chapter}{\numberline {3}Measuring complexity of xtUML models}{29}{chapter.3}
\contentsline {section}{\numberline {3.1}Cyclomatic complexity of xtUML models}{29}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Cyclomatic complexity of components}{29}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Cyclomatic complexity of classes}{32}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}Cyclomatic complexity of state machines}{35}{subsection.3.1.3}
\contentsline {subsection}{\numberline {3.1.4}Cyclomatic complexity of processing code}{40}{subsection.3.1.4}
\contentsline {subsubsection}{Choosing the basic approach}{41}{subsection.3.1.4}
\contentsline {subsubsection}{Handling multiple synchronous calls of the same subroutine}{42}{figure.caption.33}
\contentsline {subsubsection}{Handling asynchronous calls}{44}{figure.caption.34}
\contentsline {subsubsection}{Handling compound branching conditions}{44}{figure.caption.35}
\contentsline {subsection}{\numberline {3.1.5}Calculating the overall cyclomatic complexity}{45}{subsection.3.1.5}
\contentsline {subsection}{\numberline {3.1.6}Calculating the distribution of cyclomatic complexity}{47}{subsection.3.1.6}
\contentsline {section}{\numberline {3.2}Entropy as a measure of xtUML component model complexity}{49}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Model elements}{50}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Vertical distribution of entropy}{51}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Horizontal distribution of entropy across classes}{52}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3.2.4}Horizontal distribution of entropy across bodies}{53}{subsection.3.2.4}
\contentsline {subsection}{\numberline {3.2.5}Entropy as a complexity metric: conclusion}{53}{subsection.3.2.5}
\contentsline {section}{\numberline {3.3}Data complexity of xtUML models}{54}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Introduction to data types in xtUML}{55}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Data type complexity}{55}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}Data flow complexity}{56}{subsection.3.3.3}
\contentsline {chapter}{\numberline {4}Calculating procedure of xtUML complexity metrics}{61}{chapter.4}
\contentsline {section}{\numberline {4.1}BridgePoint translation process}{61}{section.4.1}
\contentsline {section}{\numberline {4.2}Implementation of xtUML cyclomatic complexity}{63}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Vertical distribution of cyclomatic complexity}{64}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Horizontal distribution of cyclomatic complexity}{73}{subsection.4.2.2}
\contentsline {section}{\numberline {4.3}Implementation of entropy complexity metric}{76}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Vertical distribution of entropy complexity metric}{77}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Horizontal distribution of entropy complexity}{78}{subsection.4.3.2}
\contentsline {section}{\numberline {4.4}Implementation of data complexity metric}{80}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Distribution of data type complexity metric}{81}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}Vertical distribution of data flow complexity metric}{85}{subsection.4.4.2}
\contentsline {subsection}{\numberline {4.4.3}Horizontal distribution of data flow complexity metric}{89}{subsection.4.4.3}
\contentsline {chapter}{\numberline {5}Hypothesis and experiment setup}{91}{chapter.5}
\contentsline {section}{\numberline {5.1}Study objects}{91}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Comparing the naming conventions used by models}{92}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Comparing the models in terms of LOC}{93}{subsection.5.1.2}
\contentsline {subsection}{\numberline {5.1.3}Comparing the models in terms of cyclomatic complexity}{95}{subsection.5.1.3}
\contentsline {subsection}{\numberline {5.1.4}Comparing model entropies}{97}{subsection.5.1.4}
\contentsline {subsection}{\numberline {5.1.5}Comparing the data type complexity of models}{102}{subsection.5.1.5}
\contentsline {subsection}{\numberline {5.1.6}Comparing the data flow complexity of models}{105}{subsection.5.1.6}
\contentsline {subsection}{\numberline {5.1.7}Conclusion on model comparison}{108}{subsection.5.1.7}
\contentsline {section}{\numberline {5.2}Study subjects}{109}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Preparation}{109}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Data collection}{111}{subsection.5.2.2}
\contentsline {chapter}{\numberline {6}Experiment results}{113}{chapter.6}
\contentsline {section}{\numberline {6.1}The relation between experiment results and complexity distribution}{114}{section.6.1}
\contentsline {section}{\numberline {6.2}Threats to validity}{115}{section.6.2}
\contentsline {subsection}{\numberline {6.2.1}Internal validity}{115}{subsection.6.2.1}
\contentsline {subsection}{\numberline {6.2.2}External validity}{116}{subsection.6.2.2}
\contentsline {subsection}{\numberline {6.2.3}Construct validity}{117}{subsection.6.2.3}
\contentsline {chapter}{\numberline {7}Conclusion}{119}{chapter.7}
\contentsline {chapter}{Bibliography}{121}{chapter*.112}
